 <div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link active" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="fas fa-shopping-bag"></i>Quản lý bán hàng <span class="badge badge-success">6</span></a>
                        <div id="submenu-1" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                <a class="nav-link" href="{{ route('loai_hd') }}">Loại hóa đơn</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('hoa_don') }}">Quản lý hóa đơn</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:;">Sales</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class="far fa-newspaper"></i>Quản lý thực đơn</a>
                        <div id="submenu-2" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.html" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2-1" aria-controls="submenu-2-1">Thực đơn</a>
                                    <div id="submenu-2-1" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('loai_sp') }}">Nhóm món ăn</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('san_pham') }}">Danh sách món ăn</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="index.html" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2-2" aria-controls="submenu-2-2">Topping</a>
                                    <div id="submenu-2-2" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('loai_topping') }}">Nhóm Topping</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('topping') }}">Danh sách Topping</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-users"></i>Quản lý nhân viên</a>
                        <div id="submenu-3" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.html" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3-1" aria-controls="submenu-3-1">Tài khoản nhân viên</a>
                                    <div id="submenu-3-1" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:;">Chức vụ</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('phan_quyen') }}">Phân quyền</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('user') }}">Danh sách nhân viên</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:;">Chấm công</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:;" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3-2" aria-controls="submenu-3-2">Quản lý ca làm việc</a>
                                    <div id="submenu-3-2" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:;">Ca làm việc</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:;">Phân ca làm việc</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:;">Đổi ca làm việc</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:;">Nhân viên nghỉ phép</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="javascript:;">Lịch sử phiên làm việc</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4" aria-controls="submenu-4"><i class=" fas fa-chart-line"></i>Báo cáo</a>
                        <div id="submenu-4" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('dashboard') }}">Báo cáo chung</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('doanh_thu') }}">Báo cáo doanh thu</a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a class="nav-link" href="">Báo cáo sản phẩm</a>
                                </li> --}}
                               {{--  <li class="nav-item">
                                    <a class="nav-link" href="">Báo cáo Marketing</a>
                                </li> --}}
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5"><i class="fas fa-fw fa-table"></i>Marketing</a>
                        <div id="submenu-5" class="collapse submenu" style="">
                            <ul class="nav flex-column">
                                <a class="nav-link" href="index.html" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5-1" aria-controls="submenu-5-1">Chương trình khuyến mại</a>
                                    <div id="submenu-5-1" class="collapse submenu" style="">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('loai_promotion') }}">Loại khuyến mại</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('promotion') }}">Danh sách khuyến mại</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('promotion_detail') }}">Chi tiết khuyến mại</a>
                                            </li>
                                        </ul>
                                    </div>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('setting') }}" ><i class="fas fa-cog"></i>Tùy chỉnh thông tin cửa hàng</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
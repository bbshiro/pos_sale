<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login</title>
    <!-- Bootstrap CSS -->
    <base href="{{asset('')}}"> 
    <link rel="stylesheet" href="source/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="source/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="source/assets/libs/css/style.css">
    <link rel="stylesheet" href="source/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center"><a href="../index.html"><img class="logo-img" src="source/assets/images/small_logo.jpg" alt="logo"></a><span class="splash-description">Đăng nhập vào hệ thống</span></div>
            <div class="card-body">
                <form action="{{ route('check_login') }}" method="post">
                    {{csrf_field()}}
                    @if(count($errors) > 0 )
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $arr)
                                {{$arr}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(isset($thongbao))
                        <div class="alert alert-danger">
                            {{$thongbao}}
                        </div>
                    @endif
                    <div class="formce-group">
                        <input class="form-control form-control-lg" id="email" type="email" placeholder="Email" name="email" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="password" type="password" placeholder="Password" name="password">
                    </div>
                    <div class="form-group">
                        <label class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox"><span class="custom-control-label">Remember Me</span>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
                </form>
            </div>
        </div>
    </div>
  
    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="source/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="source/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
</body>
 
</html>
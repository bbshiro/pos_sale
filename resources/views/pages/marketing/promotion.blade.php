@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Thông tin về promotion</h5>

            <div class="card-body">
                <br>
                <button id="add_btn" name="add" type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#add_modal">Thêm</button>
                <button id="add_detail" name="add_detail" type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#add_detail_promotion">Thêm chi tiết promotion</button>
                <br>
                <br>
                <form method = "POST" id="filter_form">
                <div class="form-group row">
                    <label for="filter_loai_pr" class="col-sm-2 col-form-label">Loại Sản Phẩm:</label>
                    <select class="form-control col-4" name = "filter_loai_pr" id="filter_loai_pr">
                        <option value="" disabled selected hidden>Please choose...</option>
                        <option value="">Tất cả</option>
                        @forelse($ListLoaiPromotion as $LoaiPromotion)
                            <option value = "{{ $LoaiPromotion->ma_loai_promotion }}">{{$LoaiPromotion->ten_loai_promotion}}</option>
                        @empty
                            <option value="" disabled selected hidden>Please choose...</option>
                        @endforelse
                    </select>
                </div>
                <div class="form-group row">
                    <label for="filter_start_date" class="col-sm-2 col-form-label">Ngày bắt đầu:</label>
                    <input type="date" class="form-control col-4" name="filter_start_date" id="filter_start_date" placeholder="Nhập ngày bắt đầu">
                    <label for="filter_end_date" class="col-sm-2 col-form-label">Ngày kết thúc:</label>
                    <input type="date" class="form-control col-4" name="filter_end_date" id="filter_end_date" placeholder="Nhập ngày Kết thúc">
                    <button class="btn btn-primary" type="submit">Lọc</button>
                </div>
                </form>
                <br>
                <br>
                <table class="table table-bordered" id="list-table">
                    <thead>
                        <tr>
                            <th>Tên promotion</th>
                            <th>Loại Promotion</th>
                            <th>Phần trăm giảm giá</th>
                            <th>Thời gian bắt đầu</th>
                            <th>Thời gian kết thúc</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="add_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="post_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &#10006;
                    </button>
                    <h4 class="modal-title">Add Data</h4>
                </div>

                <div class="modal-body">
                    {{csrf_field()}}
                    <span id="form_output"></span>
                    <div class="form-group">
                        <label>Nhập Tên Promotion</label>
                        <input type="text" name="ten_promotion" id="input_ten_promotion" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Loại Promotion</label>
                        <select class="form-control" name = "loai_promotion" id="input_loai_promotion">
                            @forelse($ListLoaiPromotion as $LoaiPromotion)
                                <option value = "{{ $LoaiPromotion->ma_loai_promotion }}">{{$LoaiPromotion->ten_loai_promotion}}</option>
                            @empty
                                <option value = "empty">Không có loại khuyến mại nào</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nhập phần trăm giảm giá</label>
                        <input type="number" name="percent_discount" id="input_percent_discount" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập thời gian bắt đầu</label>
                        <input type="date" name="time_start" id="input_time_start" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập thời gian kết thúc</label>
                        <input type="date" name="time_end" id="input_time_end" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="promotion_id" id="promotion_id" value="">
                    <input type="hidden" name="button_action" id="button_action" value="insert">
                    <input type="submit" name="submit" id="action" value="Add" class="btn btn-info">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="add_detail_promotion" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="post_form_promotion">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &#10006;
                    </button>
                    <h4 class="modal-title">Add Data</h4>
                </div>

                <div class="modal-body">
                    {{-- {{csrf_field()}} --}}
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <span id="form_output_promotion"></span>
                    <div class="form-group">
                        <label>Chọn Promotion</label>
                        <select class="form-control" name = "available_promotion" id="input_available_promotion">
                            @forelse($ListAvailablePromotion as $AvailablePromotion)
                                <option value = "{{ $AvailablePromotion->id }}">{{$AvailablePromotion->ten_promotion}}</option>
                            @empty
                                <option value = "empty">Không có chương trình khuyến mại nào</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <a id="check_all" class="form-control btn btn-default">Chọn tất cả sản phẩm</a>
                    </div>
                    <div class="form-group">
                        @forelse($ListSanPham as $SanPham)
                            <div class="form-check">
                              <label class="form-check-label" for="sp_{{$SanPham->id}}" >
                                <input type="checkbox" class="form-check-input" id="sp_{{$SanPham->id}}" name="{{ $SanPham->ten_san_pham}}" value={{$SanPham->id}}>{{ $SanPham->ten_san_pham}}
                              </label>
                            </div>
                        @empty
                            <div class="form-check">
                              Không có data để hiển thị
                              </label>
                            </div>
                        @endforelse
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="a_promotion_id" id="a_promotion_id" value="">
                    {{-- <input type="hidden" name="button_action" id="button_action" value="insert"> --}}
                    <input type="submit" name="submit" id="action_promotion" value="Add" class="btn btn-info">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="toast mt-8 fixed-top">
  <div class="toast-header">
    Thông báo
  </div>
  <div id="toast_content" class="toast-body alert alert-danger">
    Bị lỗi
  </div>
</div>
@endsection

@section('script')
<script>
$(function() {
    createTable();
});

function createTable(){
    var filter_loai_pr = $("#filter_loai_pr").val();
    var filter_start_date = $("#filter_start_date").val();
    var filter_end_date = $("#filter_end_date").val();

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url:'{!! route('promotionData') !!}',
            type: 'POST',
            data: {filter_loai_pr:filter_loai_pr, filter_start_date:filter_start_date, filter_end_date:filter_end_date}
        },
        columns: [
            { data: 'ten_promotion', name: 'ten_promotion' },
            { data: 'loai_promotion', name: 'loai_promotion' },
            { data: 'percent_discount', name: 'percent_discount' },
            { data: 'time_start', name: 'time_start' },
            { data: 'time_end', name: 'time_start' },
            { data: 'action', name: 'action', orderable: false, searchalbe: false}
        ]
    });
}

$('#filter_form').on('submit', function(event){
    event.preventDefault();
    $('#list-table').dataTable().fnDestroy();
    createTable();
});

$('#add_btn').click(function(){
    // $('#add_modal').show('show');
    $('#form_output').html("");
    $('#post_form')[0].reset();
    $('#form_output').html('');
    $('#button_action').val('insert');
    $('#action').val('Add');
});

$('#post_form_promotion').on('submit', function(event){
    event.preventDefault();
    var san_pham_id = [];
    $.each($("input[type='checkbox']:checked"), function(){            
                san_pham_id.push($(this).val());
            });

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
        url:'{{ route('ajax.add_promotion_detail') }}',
        method: "POST",
        data: {promotion_id: jQuery('#input_available_promotion').val(),
               san_pham_id: san_pham_id},
        dataType: "json",
        success:function(data)
        {
            if(data.error.length > 0)
            {
                var error_html = '';
                for(var count = 0; count < data.error.length;
                    count++)
                {
                    error_html += '<div class ="alert alert-danger">'+data.error[count]+'</div>';
                }
                $('#form_output_promotion').html(error_html);
            }
            else
            {
                $('#form_output_promotion').html(data.success);
                $('#post_form_promotion')[0].reset();
                $('#action_promotion').val('Add');
                $('.modal-title').text('Add Data');
                // $('#button_action').val('insert');
                // $('#list-table').DataTable().ajax.reload();
                $('#add_detail_promotion').modal('toggle');
                show_toast("Cập nhật dữ liệu thành công!!", "success");
            }
        },
        error:function(_a, _b, _c){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");
        }
    })
});

$('#post_form').on('submit', function(event){
    event.preventDefault();
    var form_data = $(this).serialize();
    console.log(form_data);
    $.ajax({
        url:'{{ route('ajax.add_promotion') }}',
        method: "POST",
        data: form_data,
        dataType: "json",
        success:function(data)
        {
            if(data.error.length > 0)
            {
                var error_html = '';
                for(var count = 0; count < data.error.length;
                    count++)
                {
                    error_html += '<div class ="alert alert-danger">'+data.error[count]+'</div>';
                }
                $('#form_output').html(error_html);
            }
            else
            {
                $('#form_output').html(data.success);
                $('#post_form')[0].reset();
                $('#action').val('Add');
                $('.modal-title').text('Add Data');
                $('#button_action').val('insert');
                $('#list-table').DataTable().ajax.reload();
                $('#add_modal').modal('toggle');
                show_toast("Cập nhật dữ liệu thành công!!", "success");
            }
        },
        error:function(_a, _b, _c){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");
        }
    })
});

$(document).on('click', '.delete', function(){
    var id = $(this).attr('id');
    if(confirm("Bạn có thực sự muốn xóa Khuyến Mại này")){
        $.ajax({
            url: "{{ route('ajax.remove_promotion') }}",
            method: "get",
            data:{id:id},
            success:function(data){
                $('#list-table').DataTable().ajax.reload();
                show_toast("Xóa dữ liệu thành công!!", "success");
            },
            error:function(_a, _b, _c){
                show_toast("Xóa dữ liệu không thành công!!", "warning");
            }            
        })
    }
});

$(document).on('click', '.edit', function(){
    var id = $(this).attr("id");
    // $('#form_output').html(data.success);
    $.ajax({
        url:"{{ route('ajax.get_promotion') }}",
        method:'get',
        data:{id:id},
        dataType:'json',
        success:function(data){
            $('#input_ten_promotion').val(data.ten_promotion);
            $('#input_percent_discount').val(data.percent_discount);
            $('#input_time_start').val(data.time_start);
            $('#input_time_end').val(data.time_end);
            $('#loai_promotion').val('loai_promotion');
            $('#promotion_id').val(id);
            $('#action').val('Edit');
            $('.modal-title').text('Edit Data');
            $('#button_action').val('update');
            show_toast("Cập nhật dữ liệu thành công!!", "success");
        },
        error:function(_a, _b, _c){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");
        }
    });
});

$(document).on('click', '#check_all', function(){
    $.each($("input[type='checkbox']"), function(){            
        if($(this).is(':checked')){
            $(this).prop('checked', false);
        } else
            $(this).prop('checked', true);
    });
});
</script>
@endsection
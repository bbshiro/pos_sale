@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Thông tin chi tiết promotion</h5>

            <div class="card-body">
                {{-- <br>
                <button id="add_btn" name="add" type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#add_modal">Thêm</button>
                <br> --}}
                <br>
                <table class="table table-bordered" id="list-table">
                    <thead>
                        <tr>
                            <th>Tên promotion</th>
                            <th>Tên sản phẩm</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="toast mt-8 fixed-top">
  <div class="toast-header">
    Thông báo
  </div>
  <div id="toast_content" class="toast-body alert alert-danger">
    Bị lỗi
  </div>
</div>
@endsection

@section('script')
<script>
$(function() {
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('promotionDetailData') !!}',
        columns: [
            { data: 'ten_promotion', name: 'ten_promotion' },
            { data: 'ten_san_pham', name: 'ten_san_pham' },
            { data: 'action', name: 'action', orderable: false, searchalbe: false}
        ]
    });
});

$(document).on('click', '.delete', function(){
    var promotion_id = $(this).attr('id');
    var san_pham_id = $(this).children("input[type='hidden']").val();
    console.log(san_pham_id);
    if(confirm("Bạn có thực sự muốn xóa")){
        $.ajax({
            url: "{{ route('ajax.remove_promotion_detail') }}",
            method: "get",
            data:{promotion_id:promotion_id, san_pham_id:san_pham_id},
            success:function(data){
                $('#list-table').DataTable().ajax.reload();
                show_toast("Xóa dữ liệu thành công!!", "success");
            },
            error:function(_a, _b, _c){
                show_toast("Xóa dữ liệu không thành công!!", "warning");
            }            
        })
    }
});

</script>
@endsection
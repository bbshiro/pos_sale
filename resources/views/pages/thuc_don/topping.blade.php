@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Thông tin về các sản phẩm</h5>

            <div class="card-body">
                <br>
                <button id="add_btn" name="add" type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#add_modal">Thêm</button>
                <br>
                <br>
                 <div class="form-group row">
                    <label for="filter_loai_tp" class="col-sm-2 col-form-label">Loại Sản Phẩm:</label>
                    <select class="form-control col-4" name = "filter_loai_tp" id="filter_loai_tp">
                        <option value="" disabled selected hidden>Please choose...</option>
                        <option value="">Tất cả</option>
                        @forelse($ListLoaiTopping as $LoaiTopping)
                            <option value = "{{ $LoaiTopping->ma_loai_topping }}">{{$LoaiTopping->ten_loai_topping}}</option>
                        @empty
                            <option value="" disabled selected hidden>Please choose...</option>
                        @endforelse
                    </select>
                    <label for="filter_price" class="col-sm-2 col-form-label">Giá tiền:</label>
                    <input type="number" class="form-control col-4" name="filter_price" id="filter_price" placeholder="Nhập giá tiền">
                </div>
                <br>
                <br>
                <table class="table table-bordered" id="list-table">
                    <thead>
                        <tr>
                            <th>Tên Topping</th>
                            <th>Loại Topping</th>
                            <th>Giá</th>
                            <th>Ghi chú</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="add_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="post_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &#10006;
                    </button>
                    <h4 class="modal-title">Add Data</h4>
                </div>

                <div class="modal-body">
                    {{csrf_field()}}
                    <span id="form_output"></span>
                    <div class="form-group">
                        <label>Nhập Tên Topping</label>
                        <input type="text" name="name_topping" id="input_name_topping" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập Giá Topping</label>
                        <input type="number" name="price_topping" id="input_price_topping" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Loại Topping</label>
                        <select class="form-control" name = "loai_topping" id="input_loai_topping">
                            @forelse($ListLoaiTopping as $LoaiTopping)
                                <option value = "{{ $LoaiTopping->ma_loai_topping }}">{{$LoaiTopping->ten_loai_topping}}</option>
                            @empty
                                <option valie = "empty">Không có loại topping nào</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nhập Ghi Chú</label>
                        <textarea class="form-control" name = "ghi_chu" id="input_ghi_chu" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="topping_id" id="topping_id" value="">
                    <input type="hidden" name="button_action" id="button_action" value="insert">
                    <input type="submit" name="submit" id="action" value="Add" class="btn btn-info">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="toast mt-8 fixed-top">
  <div class="toast-header">
    Thông báo
  </div>
  <div id="toast_content" class="toast-body alert alert-danger">
    Bị lỗi
  </div>
</div>
@endsection

@section('script')
<script>
$(function() {
    createTable();
});

$("#filter_loai_tp").on('change', function(e){
    $('#list-table').dataTable().fnDestroy();
    createTable();
})

$("#filter_price").on('input', function(e){
    $('#list-table').dataTable().fnDestroy();
    createTable();
})

function createTable(){
    var filter_loai_tp = $("#filter_loai_tp").val();
    var filter_price = $("#filter_price").val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url:'{!! route('toppingData') !!}',
            type:'POST',
            data:{filter_loai_tp:filter_loai_tp, filter_price:filter_price}
        },
        columns: [
            { data: 'ten_topping', name: 'ten_topping' },
            { data: 'loai_topping', name: 'loai_topping' },
            { data: 'price', name: 'price' },
            { data: 'ghi_chu', name: 'ghi_chu' },
            { data: 'action', name: 'action', orderable: false, searchalbe: false}
        ]
    });
}

$('#add_btn').click(function(){
    // $('#add_modal').show('show');
    $('#form_output').html("");
    $('#post_form')[0].reset();
    $('#form_output').html('');
    $('#button_action').val('insert');
    $('#action').val('Add');
});

$('#post_form').on('submit', function(event){
    event.preventDefault();
    var form_data = $(this).serialize();
    console.log(form_data);
    $.ajax({
        url:'{{ route('ajax.add_topping') }}',
        method: "POST",
        data: form_data,
        dataType: "json",
        success:function(data)
        {
            if(data.error.length > 0)
            {
                var error_html = '';
                for(var count = 0; count < data.error.length;
                    count++)
                {
                    error_html += '<div class ="alert alert-danger">'+data.error[count]+'</div>';
                }
                $('#form_output').html(error_html);
            }
            else
            {
                $('#form_output').html(data.success);
                $('#post_form')[0].reset();
                $('#action').val('Add');
                $('.modal-title').text('Add Data');
                $('#button_action').val('insert');
                $('#list-table').DataTable().ajax.reload();
                $('#add_modal').modal('toggle');
                show_toast("Cập nhật dữ liệu thành công!!", "success");
            }
        },
        error:function(_a, _b, _C){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");   
        }
    })
});

$(document).on('click', '.delete', function(){
    var id = $(this).attr('id');
    if(confirm("Bạn có thực sự muốn xóa topping này")){
        $.ajax({
            url: "{{ route('ajax.remove_topping') }}",
            method: "get",
            data:{id:id},
            success:function(data){
                $('#list-table').DataTable().ajax.reload();
                show_toast("Xóa dữ liệu thành công!!", "success");
            },
            error:function(_a, _b, _C){
                show_toast("Xóa dữ liệu không thành công!!", "warning");
            }            
        })
    }
});

$(document).on('click', '.edit', function(){
    var id = $(this).attr("id");
    // $('#form_output').html(data.success);
    $.ajax({
        url:"{{ route('ajax.get_topping') }}",
        method:'get',
        data:{id:id},
        dataType:'json',
        success:function(data){
            $('#input_name_topping').val(data.ten_topping);
            $('#input_price_topping').val(data.price);
            $('#input_ghi_chu').val(data.ghi_chu);
            $('#input_loai_topping').val(data.loai_topping);
            $('#topping_id').val(id);
            $('#action').val('Edit');
            $('.modal-title').text('Edit Data');
            $('#button_action').val('update');
            show_toast("Cập nhật dữ liệu thành công!!", "success");
        },
        error:function(_a, _b, _c){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");
        }
    });
});
</script>
@endsection
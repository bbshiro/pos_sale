@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Thông tin về các sản phẩm</h5>

            <div class="card-body">
                <br>
                <button id="add_btn" name="add" type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#add_modal">Thêm</button>
                <br>
                <br>
               
                <div class="form-group row">
                    <label for="filter_loai_sp" class="col-sm-2 col-form-label">Loại Sản Phẩm:</label>
                    <select class="form-control col-4" name = "filter_loai_sp" id="filter_loai_sp">
                        <option value="" disabled selected hidden>Please choose...</option>
                        <option value="">Tất cả</option>
                        @forelse($ListLoaiSanPham as $LoaiSanPham)
                            <option value = "{{ $LoaiSanPham->ma_loai_san_pham }}">{{$LoaiSanPham->ten_loai_san_pham}}</option>
                        @empty
                            <option value="" disabled selected hidden>Please choose...</option>
                        @endforelse
                    </select>
                    <label for="filter_price" class="col-sm-2 col-form-label">Giá tiền:</label>
                    <input type="number" class="form-control col-4" name="filter_price" id="filter_price" placeholder="Nhập giá tiền">
                </div>
                <br>
                <br>
                <table class="table table-bordered" id="list-table">
                    <thead>
                        <tr>
                            <th>Tên Sản Phẩm</th>
                            <th>Loại sản phẩm</th>
                            <th>Giá</th>
                            <th>Ghi chú</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="add_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="post_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &#10006;
                    </button>
                    <h4 class="modal-title">Add Data</h4>
                </div>

                <div class="modal-body">
                    {{csrf_field()}}
                    <span id="form_output"></span>
                    <div class="form-group">
                        <label>Nhập Tên Sản Phẩm</label>
                        <input type="text" name="name_sp" id="input_name_sp" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập Giá Sản Phẩm</label>
                        <input type="number" name="price_sp" id="input_price_sp" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Loại Sản Phẩm</label>
                        <select class="form-control" name = "loai_sp" id="input_loai_sp">
                            @forelse($ListLoaiSanPham as $LoaiSanPham)
                                <option value = "{{ $LoaiSanPham->ma_loai_san_pham }}">{{$LoaiSanPham->ten_loai_san_pham}}</option>
                            @empty
                                <option value = "empty">Không có loại sản phẩm nào</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nhập Ghi Chú</label>
                        <textarea class="form-control" name = "ghi_chu" id="input_ghi_chu" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="sp_id" id="sp_id" value="">
                    <input type="hidden" name="button_action" id="button_action" value="insert">
                    <input type="submit" name="submit" id="action" value="Add" class="btn btn-info">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
$(function() {
    createTable();
});

$("#filter_loai_sp").on('change', function(e){
    $('#list-table').dataTable().fnDestroy();
    createTable();
})

$("#filter_price").on('input', function(e){
$('#list-table').dataTable().fnDestroy();
    createTable();
})

function createTable(){
    var filter_loai_sp = $("#filter_loai_sp").val();
    var filter_price = $("#filter_price").val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('sanPhamData') !!}',
            type: 'POST',
            data: {filter_loai_sp:filter_loai_sp, filter_price:filter_price}
        },
        columns: [
            { data: 'ten_san_pham', name: 'ten_san_pham' },
            { data: 'loai_san_pham', name: 'loai_san_pham' },
            { data: 'price', name: 'price' },
            { data: 'ghi_chu', name: 'ghi_chu' },
            { data: 'action', name: 'action', orderable: false, searchalbe: false}
        ]
    });
}

$('#add_btn').click(function(){
    // $('#add_modal').show('show');
    $('#form_output').html("");
    $('#post_form')[0].reset();
    $('#form_output').html('');
    $('#button_action').val('insert');
    $('#action').val('Add');
});

$('#post_form').on('submit', function(event){
    event.preventDefault();
    var form_data = $(this).serialize();
    console.log(form_data);
    $.ajax({
        url:'{{ route('ajax.add_sp') }}',
        method: "POST",
        data: form_data,
        dataType: "json",
        success:function(data)
        {
            if(data.error.length > 0)
            {
                var error_html = '';
                for(var count = 0; count < data.error.length;
                    count++)
                {
                    error_html += '<div class ="alert alert-danger">'+data.error[count]+'</div>';
                }
                $('#form_output').html(error_html);
            }
            else
            {
                $('#form_output').html(data.success);
                $('#post_form')[0].reset();
                $('#action').val('Add');
                $('.modal-title').text('Add Data');
                $('#button_action').val('insert');
                $('#list-table').DataTable().ajax.reload();
                $('#add_modal').modal('toggle');
                show_toast("Cập nhật dữ liệu thành công!!", "success");
            }
        },
        error:function(_a, _b, _c){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");
        }
    })
});

$(document).on('click', '.delete', function(){
    var id = $(this).attr('id');
    if(confirm("Bạn có thực sự muốn xóa sản phẩm này")){
        $.ajax({
            url: "{{ route('ajax.remove_sp') }}",
            method: "get",
            data:{id:id},
            success:function(data){
                $('#list-table').DataTable().ajax.reload();
                show_toast("Xóa dữ liệu thành công", "success");
            },
            error:function(_a, _b, _c){
                show_toast("Xóa dữ liệu không thành công!!", "warning");
            }            
        })
    }
});

$(document).on('click', '.edit', function(){
    var id = $(this).attr("id");
    // $('#form_output').html(data.success);
    $.ajax({
        url:"{{ route('ajax.get_sp') }}",
        method:'get',
        data:{id:id},
        dataType:'json',
        success:function(data){
            $('#input_name_sp').val(data.ten_san_pham);
            $('#input_price_sp').val(data.price);
            $('#input_ghi_chu').val(data.ghi_chu);
            $('#input_loai_sp').val(data.loai_san_pham);
            $('#sp_id').val(id);
            $('#action').val('Edit');
            $('.modal-title').text('Edit Data');
            $('#button_action').val('update');
            show_toast("Cập nhật thông tin thành công!!", "success");
        },
        error:function(_a, _b, _c){
            show_toast("Cập nhật thông tin không thành công!!", "warning");
        }
    });
});

</script>
@endsection
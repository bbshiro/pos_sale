@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Thông tin về Hóa đơn</h5>

            <div class="card-body">
                <br>
                <form method = "POST" id="filter_form">
                    <div class="form-group row">
                        <label for="filter_loai_hd" class="col-sm-2 col-form-label">Loại Hóa đơn:</label>
                        <select class="form-control col-4" name = "filter_loai_hd" id="filter_loai_hd">
                            <option value="" disabled selected hidden>Please choose...</option>
                            <option value="">Tất cả</option>
                            @forelse($ListLoaiHoaDon as $LoaiHoaDon)
                                <option value = "{{ $LoaiHoaDon->ma_loai_hoa_don }}">{{$LoaiHoaDon->ten_loai_hoa_don}}</option>
                            @empty
                                <option value="" disabled selected hidden>Please choose...</option>
                            @endforelse
                        </select>
                        <label for="filter_user" class="col-sm-2 col-form-label">Người bán hàng:</label>
                        <select class="form-control col-4" name = "filter_user" id="filter_user">
                            <option value="" disabled selected hidden>Please choose...</option>
                            <option value="">Tất cả</option>
                            @forelse($ListUser as $User)
                                <option value = "{{ $User->id }}">{{$User->name}}</option>
                            @empty
                                <option value="" disabled selected hidden>Please choose...</option>
                            @endforelse
                        </select>
                        <br>
                        <br>
                        <label for="filter_start_date" class="col-sm-2 col-form-label">Ngày tạo:</label>
                        <input type="date" class="form-control col-4" name="filter_start_date" id="filter_start_date" placeholder="Nhập giá tiền">
                        <span class="col-sm-2 col-form-label"> to </span>
                        <input type="date" class="form-control col-4" name="filter_end_date" id="filter_end_date" placeholder="Nhập giá tiền">
                        <button type="submit" class="btn btn-primary">Lọc</button>
                    </div>
                </form>
                <br>
                <br>
                <table class="table table-bordered" id="list-table">
                    <thead>
                        <tr>
                            <th>Mã hóa đơn</th>
                            <th>Loại Hóa Đơn</th>
                            <th>Người Bán</th>
                            <th>Ngày tạo</th>
                            <th>Khuyến mại</th>
                            <th>Tổng tiền</th>
                            <th>Tổng tiền khuyến mại</th>
                            <th>Tiền khách trả</th>
                            <th>Tiền thừa</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="detail_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &#10006;
                    </button>
                    <h4 class="modal-title">Chi tiết hóa đơn</h4>
                </div>

                <div class="modal-body">
                    <button id="print" class="btn btn-info">Xuất Hóa Đơn</button>
                    <br>
                    <table class="table table-bordered" id="detail-table">
                    <thead>
                        <tr>
                            <th>Mã hóa đơn</th>
                            <th>Sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Tổng tiền</th>
                            <th>Tổng tiền khuyến mại</th>
                        </tr>
                    </thead>
                </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
        </div>
        <input type="hidden" name="ma_hoa_don" id="ma_hoa_don">
    </div>
</div>

@endsection

@section('script')
<script>
$(function() {
    createTable();
});

function createTable(){
    var filter_loai_hd = $("#filter_loai_hd").val();
    var filter_user = $("#filter_user").val();
    var filter_start_date = $("#filter_start_date").val();
    var filter_end_date = $("#filter_end_date").val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('hoaDonData') !!}',
            type: 'POST',
            data: {filter_loai_hd:filter_loai_hd, filter_user:filter_user, filter_start_date:filter_start_date, filter_end_date:filter_end_date}
        },
        columns: [
            { data: 'ma_hoa_don', name: 'ma_hoa_don' },
            { data: 'loai_hoa_don', name: 'loai_hoa_don' },
            { data: 'user_id', name: 'user_id' },
            { data: 'created_at', name: 'created_at' },
            { data: 'promotion_id', name: 'promotion_id' },
            { data: 'tong_tien', name: 'tong_tien' },
            { data: 'tong_tien_discount', name: 'tong_tien_discount' },
            { data: 'khach_tra', name: 'khach_tra' },
            { data: 'tien_thua', name: 'tien_thua' },
            { data: 'action', name: 'action', orderable: false, searchalbe: false}
        ]
    });
}


$('#filter_form').on('submit', function(event){
    event.preventDefault();
    $('#list-table').dataTable().fnDestroy();
    createTable();
});

$(document).on('click', '.detail', function(){
    var id = $(this).attr("id");
    $('#ma_hoa_don').val(id);
    $('#detail-table').dataTable().fnDestroy();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#detail-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('hoaDonDataDetail') !!}',
            type: 'POST',
            data: {hoa_don_id:id}
        },
        columns: [
            { data: 'ma_hoa_don', name: 'ma_hoa_don' },
            { data: 'id_san_pham', name: 'id_san_pham' },
            { data: 'so_luong', name: 'so_luong' },
            { data: 'tong_tien', name: 'tong_tien' },
            { data: 'tong_tien_discount', name: 'tong_tien_discount' }
        ]
    });
    $('#detail_modal').modal('toggle');
    $('#detail_modal').modal('handleUpdate');
});

$('#print').click(function(){
    var id = $('#ma_hoa_don').val();
    $.ajax({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ route('ajax.print_bill') }}",
        method: "post",
        data:{id:id},
        dataType: "json",
        success:function(data){
            window.open(data.url, '_blank');
        },

        error: function (jqXHR, textStatus, errorThrown)
         {
            alert(errorThrown);
         }           
    });
});
</script>
@endsection
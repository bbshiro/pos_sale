@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Thông tin về các loại sản phẩm</h5>
            <div class="card-body">
                <table class="table table-bordered" id="users-table">
                    <thead>
                        <tr>
                            <th>Mã Loại Sản Phẩm</th>
                            <th>Tên Loại Sản Phẩm</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('loaiSpData') !!}',
        columns: [
            { data: 'ma_loai_san_pham', name: 'ma_loai_san_pham' },
            { data: 'ten_loai_san_pham', name: 'ten_loai_san_pham' }
        ]
    });
});
</script>
@endsection
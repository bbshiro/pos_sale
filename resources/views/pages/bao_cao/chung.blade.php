@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Báo cáo doanh thu</h5>

            <div class="card-body">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Doanh thu hôm nay</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1 text-primary">{!! $tong_doanh_thu !!} VND</h1>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Số hóa đơn hôm nay</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1 text-primary">{!! $so_hoa_don !!}</h1>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Doanh thu trung bình trên hóa đơn</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 class="mb-1 text-primary">{!! $trung_binh !!} VND</h1>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                
                <form method = "POST" id="form">
                    <div class="form-group row">
                        <label for="type" class="col-form-label">Loại Báo cáo:  </label>
                        <select id="type" class="custom-select col-3">
                            <option value="doanh_thu_theo_gio">Báo cáo doanh thu theo giờ</option>
                            <option value="doanh_thu_theo_ngay">Báo cáo doanh thu theo ngày</option>
                            <option value="doanh_thu_theo_sp">Báo cáo doanh thu theo sản phẩm</option>
                            {{-- <option value="promotion">Báo cáo doanh thu theo chương trình KM</option> --}}
                        </select>
                        <label for="time" class="col-form-label">Ngày:  </label>
                        <input type="text" name="time" id="time" class="form-control col-4">
                        <button type="submit" class="btn btn-info" style="margin-left: 10px;">Xem báo cáo</button>
                    </div>
                </form>
                <br>
                <br>
                <div class="col-md-8 col-lg-8 col-8 offset-md-2">
                    <canvas id="tong_tien"></canvas>
                </div>
                <br>
                <br>
                <div class="col-md-8 col-lg-8 col-8 offset-md-2">
                    <canvas id="trung_binh"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    var chart_tong_tien;
    var chart_trung_binh;
$(function(){
    $('#time').daterangepicker({
        "autoApply": true,
        "timePicker": true,
        "timePicker24Hour": true,
        "startDate": set_start_of_day(moment().startOf('month')),
        "endDate": set_end_of_day(moment().endOf('month')),
        locale: {
          format: 'DD/MM/YY HH:mm:ss'
        },
        ranges: {
           'Hiện Tại': [set_start_of_day(moment()), set_end_of_day(moment())],
           'Hôm Qua': [set_start_of_day(moment().subtract(1, 'days')), set_end_of_day(moment().subtract(1, 'days'))],
           '7 ngày gần nhất': [set_start_of_day(moment().subtract(6, 'days')), set_end_of_day(moment())],
           '30 Ngày gần nhất': [set_start_of_day(moment().subtract(29, 'days')), set_end_of_day(moment())],
           'Tháng Hiện Tại': [set_start_of_day(moment().startOf('month')), set_end_of_day(moment().endOf('month'))],
           'Tháng trước': [set_start_of_day(moment().subtract(1, 'month').startOf('month')), set_end_of_day(moment().subtract(1, 'month').endOf('month'))]
        }
    }, function(start, end, label) {

    });
});

function set_start_of_day(time){
    return time.set({hour:0,minute:0,second:0});
}

function set_end_of_day(time){
    return time.set({hour:23,minute:59,second:59});
}


$('#form').on('submit', function(event){
    event.preventDefault();
    var start_time = $('#time').data('daterangepicker').startDate.format("YYYY-MM-DD HH:mm");
    var end_time = $('#time').data('daterangepicker').endDate.format("YYYY-MM-DD HH:mm");
    var type = $("#type").val();

    if(type == "doanh_thu_theo_gio"){
        update_chart('{{ route('tongTienTheoGio') }}', "Tổng doanh thu", "Tổng doanh thu", start_time, end_time, chart_tong_tien, 'gio', 'tong_tien_hd');
        update_chart('{{ route('trungBinhTheoGio') }}', "Trung bình trên 1 hóa đơn", "Doanh thu trung bình trên một hóa đơn", start_time, end_time, chart_trung_binh, 'gio', 'trung_binh');
    }
    else if(type == "doanh_thu_theo_ngay"){
        update_chart('{{ route('tongTienTheoNgay') }}', "Tổng doanh thu", "Tổng doanh thu", start_time, end_time, chart_tong_tien, 'ngay', 'tong_tien_hd');
        update_chart('{{ route('trungBinhTheoNgay') }}', "Trung bình trên 1 hóa đơn", "Doanh thu trung bình trên một hóa đơn", start_time, end_time, chart_trung_binh, 'ngay', 'trung_binh');
    } 
    else if(type == "doanh_thu_theo_sp"){
        update_chart('{{ route('tongTienSanPham') }}', "Doanh thu theo sản phẩm", "Doanh thu theo sản phẩm", start_time, end_time, chart_tong_tien, 'sp', 'tong_tien_sp');
        update_chart('{{ route('soLuongSanPham') }}', "Số sản phẩm bán ra", "Số sản phẩm bán ra", start_time, end_time, chart_trung_binh, 'sp', 'so_luong_sp');
    }
});

function update_chart(Route, LabelText, Title, start_time, end_time, chart, type_label, type_data){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
        url:Route,
        method: "POST",
        data: {start_time:start_time, end_time:end_time},
        dataType: "json",
        success:function(data)
        {   
            console.log(data);
            var Labels = new Array();
            var Totals = new Array();
            data.forEach(function(du_lieu){
                if(type_label == 'gio'){
                    Labels.push(du_lieu.Day+"-"+du_lieu.Month+"-"+du_lieu.Year+" "+du_lieu.Hour+"h");
                }
                else if(type_label == 'ngay'){
                    Labels.push(du_lieu.Day+"-"+du_lieu.Month+"-"+du_lieu.Year);
                } 
                else if(type_label == 'sp'){
                    Labels.push(du_lieu.ten_san_pham);
                }

                if(type_data == "tong_tien_hd"){
                    Totals.push(du_lieu.tong_tien_hd);
                } 
                else if(type_data == "trung_binh"){
                    Totals.push(du_lieu.trung_binh);
                } 
                else if(type_data == "tong_tien_sp"){
                    Totals.push(du_lieu.tong_tien_sp);
                }
                else if(type_data == "so_luong_sp"){
                    Totals.push(du_lieu.so_luong_sp);
                }
                
            });
            console.log(Labels);
            console.log(Totals);
            chart.data.labels = Labels;
            var datasets = [
                    {
                      label: LabelText,
                      backgroundColor: "#3e95cd",
                      data: Totals
                    }
                  ];
            chart.data.datasets = datasets;
            chart.options.title.text = Title;
            chart.update();
        },
        error:function(a,b,c){
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
}

$(function(){
    var start_time = $('#time').data('daterangepicker').startDate.format("YYYY-MM-DD HH:mm");
    var end_time = $('#time').data('daterangepicker').endDate.format("YYYY-MM-DD HH:mm");
    var Labels = new Array();
    var Totals = new Array();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
        url:'{{ route('tongTienTheoGio') }}',
        method: "POST",
        data: {start_time:start_time, end_time:end_time},
        dataType: "json",
        success:function(data)
        {   
            data.forEach(function(du_lieu){
                Labels.push(du_lieu.Day+"-"+du_lieu.Month+"-"+du_lieu.Year+" "+du_lieu.Hour+"h");
                Totals.push(du_lieu.tong_tien_hd);
            });

            chart_tong_tien = new Chart(document.getElementById("tong_tien"), {
                type: 'bar',
                data: {
                  labels: Labels,
                  datasets: [
                    {
                      label: "Tổng doanh thu",
                      backgroundColor: "#3e95cd",
                      data: Totals
                    }
                  ]
                },
                options: {
                  legend: { display: false },
                  title: {
                    display: true,
                    text: 'Tổng doanh thu'
                  },
                  scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

        },
        error:function(a,b,c){
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
});

$(function(){
    var start_time = $('#time').data('daterangepicker').startDate.format("YYYY-MM-DD HH:mm");
    var end_time = $('#time').data('daterangepicker').endDate.format("YYYY-MM-DD HH:mm");
    var Labels = new Array();
    var Totals = new Array();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
        url:'{{ route('trungBinhTheoGio') }}',
        method: "POST",
        data: {start_time:start_time, end_time:end_time},
        dataType: "json",
        success:function(data)
        {   
            data.forEach(function(du_lieu){
                Labels.push(du_lieu.Day+"-"+du_lieu.Month+"-"+du_lieu.Year+" "+du_lieu.Hour+"h");
                Totals.push(du_lieu.trung_binh);
            });

            chart_trung_binh = new Chart(document.getElementById("trung_binh"), {
                type: 'bar',
                data: {
                  labels: Labels,
                  datasets: [
                    {
                      label: "Trung bình trên 1 hóa đon",
                      backgroundColor: "#3e95cd",
                      data: Totals
                    }
                  ]
                },
                options: {
                  legend: { display: false },
                  title: {
                    display: true,
                    text: 'Doanh thu trung bình trên một hóa đơn'
                  },
                  scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

        },
        error:function(a,b,c){
            console.log(a);
            console.log(b);
            console.log(c);
        }
    });
});
</script>
@endsection
@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Báo cáo doanh thu</h5>

            <div class="card-body">
                <form method = "POST" id="form">
                    <div class="form-group row">
                        <label for="type" class="col-form-label">Loại Báo cáo:  </label>
                        <select id="type" class="custom-select col-3">
                            <option value="nhan_vien">Báo cáo doanh thu theo nhân viên</option>
                            <option value="loai_hoa_don">Báo cáo doanh thu theo loại hóa đơn</option>
                            <option value="san_pham">Báo cáo doanh thu theo sản phẩm</option>
                            <option value="theo_gio">Báo cáo doanh thu theo giờ</option>
                            <option value="theo_ngay">Báo cáo doanh thu theo ngày</option>
                            {{-- <option value="promotion">Báo cáo doanh thu theo chương trình KM</option> --}}
                        </select>
                        <label for="time" class="col-form-label">Ngày:  </label>
                        <input type="text" name="time" id="time" class="form-control col-4">
                        <button type="submit" class="btn btn-info" style="margin-left: 10px;">Xem báo cáo</button>
                    </div>
                </form>
                <br>
                <br>
                <table class="table table-bordered" id="list-table">
                    <thead>
                        <tr>
                            {{-- <th>STT</th> --}}
                            <th>Nhân Viên</th>
                            <th>Số Hóa Đơn</th>
                            <th>Tổng Tiền Hóa Đơn</th>
                            <th>Tổng tiền trung bình</th>
                            <th>Tổng Tiền Khuyến Mại</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
$(function() {
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('doanhThuData') !!}',
        columns: [
            { data: 'user_id', name: 'user_id' },
            { data: 'so_hoa_don', name: 'so_hoa_don' },
            { data: 'tong_tien_hd', name: 'tong_tien_hd' },
            { data: 'trung_binh', name: 'trung_binh' },
            { data: 'tong_tien_discount_hd', name: 'tong_tien_discount_hd' }
        ]
    });
});

$(function(){
    $('#time').daterangepicker({
        "autoApply": true,
        "timePicker": true,
        "timePicker24Hour": true,
        "startDate": set_start_of_day(moment()),
        "endDate": set_end_of_day(moment()),
        locale: {
          format: 'DD/MM/YY HH:mm:ss'
        },
        ranges: {
           'Hiện Tại': [set_start_of_day(moment()), set_end_of_day(moment())],
           'Hôm Qua': [set_start_of_day(moment().subtract(1, 'days')), set_end_of_day(moment().subtract(1, 'days'))],
           '7 ngày gần nhất': [set_start_of_day(moment().subtract(6, 'days')), set_end_of_day(moment())],
           '30 Ngày gần nhất': [set_start_of_day(moment().subtract(29, 'days')), set_end_of_day(moment())],
           'Tháng Hiện Tại': [set_start_of_day(moment().startOf('month')), set_end_of_day(moment().endOf('month'))],
           'Tháng trước': [set_start_of_day(moment().subtract(1, 'month').startOf('month')), set_end_of_day(moment().subtract(1, 'month').endOf('month'))]
        }
    }, function(start, end, label) {

    });
});

function set_start_of_day(time){
    return time.set({hour:0,minute:0,second:0});
}

function set_end_of_day(time){
    return time.set({hour:23,minute:59,second:59});
}

$('#form').on('submit', function(event){
    event.preventDefault();
    var start_time = $('#time').data('daterangepicker').startDate.format("YYYY-MM-DD HH:mm");
    var end_time = $('#time').data('daterangepicker').endDate.format("YYYY-MM-DD HH:mm");
    var type = $("#type").val();

    if(type == "nhan_vien"){
        createNhanVienTable(start_time, end_time);
    }
    else if(type == "loai_hoa_don"){
        createLoaiHdTable(start_time, end_time);
    }
    else if(type == "san_pham"){
        createSanPhamTable(start_time, end_time);
    } 
    else if(type == "theo_ngay"){
        createTheoNgayTable(start_time, end_time);
    } 
    else if(type == "theo_gio"){
        createTheoGioTable(start_time, end_time);
    } else if(type == "promotion"){
        createPromotionTable(start_time, end_time);
    }
});

function createNhanVienTable(start_time, end_time){
    $('#list-table').dataTable().fnDestroy();
    $('#list-table').html("<thead><th>Id</th><th>Nhân Viên</th><th>Số Hóa Đơn</th><th>Tổng Tiền Hóa Đơn</th><th>Tổng tiền trung bình</th><th>Tổng Tiền Khuyến Mại</th></thead>");
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('doanhThuNhanVien') !!}',
            type: 'POST',
            data: {start_time:start_time, end_time:end_time}
        },
        columns: [
            { data: 'user_id', name: 'user_id' },
            { data: 'ten_nv', name: 'ten_nv' },
            { data: 'so_hoa_don', name: 'so_hoa_don' },
            { data: 'tong_tien_hd', name: 'tong_tien_hd' },
            { data: 'trung_binh', name: 'trung_binh' },
            { data: 'tong_tien_discount_hd', name: 'tong_tien_discount_hd' }
        ]
    });
}

function createLoaiHdTable(start_time, end_time){
    $('#list-table').dataTable().fnDestroy();
    $('#list-table').html("<thead><th>Loại Hóa Đơn</th><th>Số Hóa Đơn</th><th>Tổng Tiền Hóa Đơn</th><th>Tổng tiền trung bình/HD</th><th>Tổng Tiền Khuyến Mại</th></thead>");
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('doanhThuLoaiHd') !!}',
            type: 'POST',
            data: {start_time:start_time, end_time:end_time}
        },
        columns: [
            { data: 'loai_hoa_don', name: 'loai_hoa_don' },
            { data: 'so_hoa_don', name: 'so_hoa_don' },
            { data: 'tong_tien_hd', name: 'tong_tien_hd' },
            { data: 'trung_binh', name: 'trung_binh' },
            { data: 'tong_tien_discount_hd', name: 'tong_tien_discount_hd' }
        ]
    });
}

function createSanPhamTable(start_time, end_time){
    $('#list-table').dataTable().fnDestroy();
    $('#list-table').html("<thead><th>Sản Phẩm</th><th>Số Lượng Hóa Đơn</th><th>Số Lượng Bán Ra</th><th>Tổng tiền bán(kèm topping)</th><th>Tổng tiền trung bình</th><th>Tổng Tiền Khuyến Mại</th></thead>");
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('doanhThuSanPham') !!}',
            type: 'POST',
            data: {start_time:start_time, end_time:end_time}
        },
        columns: [
            { data: 'id_san_pham', name: 'id_san_pham' },
            { data: 'so_luong_hd', name: 'so_luong_hd' },
            { data: 'so_luong_sp', name: 'so_luong_sp' },
            { data: 'tong_tien_sp', name: 'tong_tien_sp' },
            { data: 'trung_binh', name: 'trung_binh' },
            { data: 'tong_tien_discount_sp', name: 'tong_tien_discount_sp' }
        ]
    });
}

function createTheoNgayTable(start_time, end_time) {
    $('#list-table').dataTable().fnDestroy();
    $('#list-table').html("<thead><th>Ngày</th><th>Số Hóa Đơn</th><th>Tổng Tiền Hóa Đơn</th><th>Tổng tiền trung bình/HD</th><th>Tổng Tiền Khuyến Mại</th></thead>");
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('doanhThuTheoNgay') !!}',
            type: 'POST',
            data: {start_time:start_time, end_time:end_time}
        },
        columns: [
            { data: 'ngay', name: 'ngay' },
            { data: 'so_hoa_don', name: 'so_hoa_don' },
            { data: 'tong_tien_hd', name: 'tong_tien_hd' },
            { data: 'trung_binh', name: 'trung_binh' },
            { data: 'tong_tien_discount_hd', name: 'tong_tien_discount_hd' }
        ]
    });
}

function createTheoGioTable(start_time, end_time){
    $('#list-table').dataTable().fnDestroy();
    $('#list-table').html("<thead><th>Giờ</th><th>Số Hóa Đơn</th><th>Tổng Tiền Hóa Đơn</th><th>Tổng tiền trung bình/HD</th><th>Tổng Tiền Khuyến Mại</th></thead>");
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('doanhThuTheoGio') !!}',
            type: 'POST',
            data: {start_time:start_time, end_time:end_time}
        },
        columns: [
            { data: 'gio', name: 'gio' },
            { data: 'so_hoa_don', name: 'so_hoa_don' },
            { data: 'tong_tien_hd', name: 'tong_tien_hd' },
            { data: 'trung_binh', name: 'trung_binh' },
            { data: 'tong_tien_discount_hd', name: 'tong_tien_discount_hd' }
        ]
    });
}

function createPromotionTable(start_time, end_time){
    $('#list-table').dataTable().fnDestroy();
    $('#list-table').html("<thead><th>Tên Promotinon</th><th>Số Hóa Đơn</th><th>Tổng Tiền Hóa Đơn</th><th>Tổng tiền trung bình/HD</th><th>Tổng Tiền Khuyến Mại</th></thead>");
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '{!! route('doanhThuPromotion') !!}',
            type: 'POST',
            data: {start_time:start_time, end_time:end_time}
        },
        columns: [
            { data: 'promotion', name: 'promotion' },
            { data: 'so_hoa_don', name: 'so_hoa_don' },
            { data: 'tong_tien_hd', name: 'tong_tien_hd' },
            { data: 'trung_binh', name: 'trung_binh' },
            { data: 'tong_tien_discount_hd', name: 'tong_tien_discount_hd' }
        ]
    });
}
</script>
@endsection
@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Tùy chỉnh thông tin cửa hàng</h5>

            <div class="card-body">
                <div class="alert" id="message" style="display: none;"></div>
                <br>
                <form enctype="multipart/form-data" method="POST" id="upload_form">
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="col-3">
                            <label for="shop_logo" class="col-form-label">Logo cửa hàng: </label>
                            <br>
                            <span id="uploaded_image"></span>
                        </div> 
                        <div class="col-9">
                            <input type="file" name="shop_logo" id="shop_logo">
                        </div>
                    </div>
                    <div class="form-row">
                        <label for="shop_name" class="col-3 col-form-label">Tên cửa hàng: </label>
                        <div class="col-9">
                            <input type="text" name="shop_name" id="shop_name" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <label for="shop_address" class="col-3 col-form-label">Địa chỉ cửa hàng: </label>
                        <div class="col-9">
                            <input type="text" name="shop_address" id="shop_address" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <label for="shop_number" class="col-3 col-form-label">Số điện thoại cửa hàng: </label>
                        <div class="col-9">
                            <input type="tel" name="shop_number" id="shop_number" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <label for="shop_password" class="col-3 col-form-label">Mật khẩu wifi: </label>
                        <div class="col-9">
                            <input type="tel" name="shop_password" id="shop_password" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <input type="submit" name="submit" value="Cập nhật" class="btn btn-info ">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function(){
        // $('#upload_form').on('submit', function(event){
        //     evetnt.preventDefault();
        //     $.ajax({
        //         url:"{{ route('ajax.upload') }}",
        //         method: "post",
        //         data:new FormData(this),
        //         dataType:"JSON",
        //         contentType: false,
        //         cache: false,
        //         processData: false,
        //         success:function(data){
        //             $('#message').css('display', 'block');
        //             $('#message').html(data.message);
        //             $('#message').addClass(data.class_name);
        //             $('#uploaded_image').html(data.uploaded_image);
        //         }
        //     })
        // });

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $('#upload_form').on('submit', function(event){
            event.preventDefault();
            var form_data = $(this).serialize();
            console.log(form_data);
            $.ajax({
                url:'{{ route('ajax.upload') }}',
                method: "POST",
                data:new FormData(this),
                dataType: "json",
                contentType: false,
                cache: false,
                processData: false,
                success:function(data)
                {   
                    console.log(data);
                    if(data.errors.length > 0)
                    {
                        var error_html = '';
                        for(var count = 0; count < data.errors.length;
                            count++)
                        {
                            error_html += '<div class ="alert alert-danger">'+data.errors[count]+'</div>';
                        }
                        $('#message').css('display', 'block');
                        $('#message').html(error_html);
                    }
                    else {
                        $('#message').css('display', 'block');
                        var message = '<div class ="alert alert-success">'+data.message+'</div>';
                        $('#message').html(message);
                        // $('#message').addClass(data.class_name);
                        var uploaded_image = '<img src="images/'+data.uploaded_image+'" class="img-thumbnail" width="300" />';
                        $('#uploaded_image').html(uploaded_image);
                    }
                    // console.log(data);
                },
                error:function(a, b, c){
                    console.log(a);
                    console.log(b);
                    console.log(c);
                    show_toast("Cập nhật dữ liệu không thành công!!", "warning");
                }
            })
        });
    });  
</script>
@endsection
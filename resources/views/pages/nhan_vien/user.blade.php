@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Thông tin về danh sách nhân viên</h5>

            <div class="card-body">
                <br>
                <button id="add_btn" name="add" type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#add_modal">Thêm</button>
                <br>
                <br>
                <div class="form-group row">
                    <label for="filter_phan_quyen" class="col-sm-2 col-form-label">Phân Quyền:</label>
                    <select class="form-control col-4" name = "filter_phan_quyen" id="filter_phan_quyen">
                        <option value="" disabled selected hidden>Please choose...</option>
                        <option value="">Tất cả</option>
                         @forelse($phanquyen as $quyen)
                                <option value = "{{ $quyen->ma_phan_quyen }}">{{$quyen->ten_quyen}}</option>
                        @empty
                            <option value="" disabled selected hidden>Please choose...</option>
                        @endforelse
                    </select>
                </div>
                <br>
                <br>
                <table class="table table-bordered" id="list-table">
                    <thead>
                        <tr>
                            <th>Username</th>
                            {{-- <th>Password</th> --}}
                            <th>Email</th>
                            <th>Chứng Minh Thư</th>
                            <th>Địa Chỉ</th>
                            <th>Quyền hạn</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="add_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="post_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &#10006;
                    </button>
                    <h4 class="modal-title">Add Data</h4>
                </div>

                <div class="modal-body">
                    {{csrf_field()}}
                    <span id="form_output"></span>
                    <div class="form-group">
                        <label>Nhập Username</label>
                        <input type="text" name="username" id="input_username" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập Password</label>
                        <input type="password" name="password" id="input_password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập Email</label>
                        <input type="email" name="email" id="input_email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập Số Điện Thoại</label>
                        <input type="tel" name="sdt" id="input_sdt" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập Số Chứng Minh</label>
                        <input type="text" name="cmnd" id="input_cmnd" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Phân Quyền</label>
                        <select class="form-control" name = "phan_quyen" id="input_phan_quyen">
                            @forelse($phanquyen as $quyen)
                                <option value = "{{ $quyen->ma_phan_quyen }}">{{$quyen->ten_quyen}}</option>
                            @empty
                                <option value = "empty">Không có loại quyền nào</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nhập địa chỉ</label>
                        <textarea class="form-control" name = "dia_chi" id="input_dia_chi" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="user_id" id="user_id" value="">
                    <input type="hidden" name="button_action" id="button_action" value="insert">
                    <input type="submit" name="submit" id="action" value="Add" class="btn btn-info">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="toast mt-8 fixed-top">
  <div class="toast-header">
    Thông báo
  </div>
  <div id="toast_content" class="toast-body alert alert-danger">
    Bị lỗi
  </div>
</div>
@endsection

@section('script')
<script>
$(function() {
    createTable();
});

$("#filter_phan_quyen").on('change', function(){
    $('#list-table').dataTable().fnDestroy();
    createTable();
});

function createTable(){
    var filter_phan_quyen = $("#filter_phan_quyen").val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url:'{!! route('userData') !!}',
            type: 'POST',
            data: {filter_phan_quyen:filter_phan_quyen}
        },
        columns: [
            { data: 'name', name: 'name' },
            // { data: 'password', name: 'password' },
            { data: 'email', name: 'email' },
            { data: 'cmnd', name: 'cmnd' },
            { data: 'dia_chi', name: 'dia_chi' },
            { data: 'phan_quyen', name: 'phan_quyen' },
            { data: 'action', name: 'action', orderable: false, searchalbe: false}
        ]
    });
}

$('#add_btn').click(function(){
    // $('#add_modal').show('show');
    $('#form_output').html("");
    $('#post_form')[0].reset();
    $('#form_output').html('');
    $('#button_action').val('insert');
    $('#action').val('Add');
});

$('#post_form').on('submit', function(event){
    event.preventDefault();
    var form_data = $(this).serialize();
    console.log(form_data);
    $.ajax({
        url:'{{ route('ajax.add_user') }}',
        method: "POST",
        data: form_data,
        dataType: "json",
        success:function(data)
        {
            if(data.error.length > 0)
            {
                var error_html = '';
                for(var count = 0; count < data.error.length;
                    count++)
                {
                    error_html += '<div class ="alert alert-danger">'+data.error[count]+'</div>';
                }
                $('#form_output').html(error_html);
            }
            else
            {
                $('#form_output').html(data.success);
                $('#post_form')[0].reset();
                $('#action').val('Add');
                $('.modal-title').text('Add Data');
                $('#button_action').val('insert');
                $('#list-table').DataTable().ajax.reload();
                $('#add_modal').modal('toggle');
                show_toast("Cập nhật dữ liệu thành công!!", "success");
            }
        },
        error:function(_a, _b, _C){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");
        }
    })
});

$(document).on('click', '.delete', function(){
    var id = $(this).attr('id');
    if(confirm("Bạn có thực sự muốn xóa Nhân Viên này")){
        $.ajax({
            url: "{{ route('ajax.remove_user') }}",
            method: "get",
            data:{id:id},
            success:function(data){
                $('#list-table').DataTable().ajax.reload();
                show_toast("Xóa dữ liệu thành công!!", "success");
            },
            error:function(_a, _b, _C){
                show_toast("Xóa dữ liệu không thành công!!", "warning");
            }            
        })
    }
});

$(document).on('click', '.edit', function(){
    var id = $(this).attr("id");
    // $('#form_output').html(data.success);
    $.ajax({
        url:"{{ route('ajax.get_user') }}",
        method:'get',
        data:{id:id},
        dataType:'json',
        success:function(data){
            $('#input_username').val(data.username);
            $('#input_password').val(data.password);
            $('#input_email').val(data.email);
            $('#input_sdt').val(data.sdt);
            $('#input_cmnd').val(data.cmnd);
            $('#input_dia_chi').val(data.dia_chi);
            $('#input_phan_quyen').val(data.phan_quyen);
            $('#user_id').val(id);
            $('#action').val('Edit');
            $('.modal-title').text('Edit Data');
            $('#button_action').val('update');
            show_toast("Cập nhật dữ liệu thành công!!", "success");
        },
        error:function(_a, _b, _C){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");
        }
    });
});
</script>
@endsection
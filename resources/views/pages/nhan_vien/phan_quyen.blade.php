@extends('layouts.master')

@section('content')
<div class="container-fluid  dashboard-content">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Thông tin về loại quyền nhân viên</h5>

            <div class="card-body">
                <br>
                <button id="add_btn" name="add" type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#add_modal">Thêm</button>
                <br>
                <br>
                <table class="table table-bordered" id="list-table">
                    <thead>
                        <tr>
                            <th>Mã Phân Quyền</th>
                            <th>Tên Phân Quyền</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="add_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="post_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &#10006;
                    </button>
                    <h4 class="modal-title">Add Data</h4>
                </div>

                <div class="modal-body">
                    {{csrf_field()}}
                    <span id="form_output"></span>
                    <div class="form-group">
                        <label>Nhập Mã Phân Quyền</label>
                        <input type="text" name="ma_pq" id="input_ma_pq" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nhập Tên Phân Quyền</label>
                        <input type="text" name="name_pq" id="input_name_pq" class="form-control">
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="phan_quyen_id" id="phan_quyen_id" value="">
                    <input type="hidden" name="button_action" id="button_action" value="insert">
                    <input type="submit" name="submit" id="action" value="Add" class="btn btn-info">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="toast mt-8 fixed-top">
  <div class="toast-header">
    Thông báo
  </div>
  <div id="toast_content" class="toast-body alert alert-danger">
    Bị lỗi
  </div>
</div>
@endsection

@section('script')
<script>
$(function() {
    $('#list-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('phanQuyenData') !!}',
        columns: [
            { data: 'ma_phan_quyen', name: 'ma_phan_quyen' },
            { data: 'ten_quyen', name: 'ten_quyen' },
            { data: 'action', name: 'action', orderable: false, searchalbe: false}
        ]
    });
});

$('#add_btn').click(function(){
    // $('#add_modal').show('show');
    $('#form_output').html("");
    $('#post_form')[0].reset();
    $('#form_output').html('');
    $('#button_action').val('insert');
    $('#action').val('Add');
});

$('#post_form').on('submit', function(event){
    event.preventDefault();
    var form_data = $(this).serialize();
    console.log(form_data);
    $.ajax({
        url:'{{ route('ajax.add_phan_quyen') }}',
        method: "POST",
        data: form_data,
        dataType: "json",
        success:function(data)
        {
            if(data.error.length > 0)
            {
                var error_html = '';
                for(var count = 0; count < data.error.length;
                    count++)
                {
                    error_html += '<div class ="alert alert-danger">'+data.error[count]+'</div>';
                }
                $('#form_output').html(error_html);
            }
            else
            {
                $('#form_output').html(data.success);
                $('#post_form')[0].reset();
                $('#action').val('Add');
                $('.modal-title').text('Add Data');
                $('#button_action').val('insert');
                $('#list-table').DataTable().ajax.reload();
                $('#add_modal').modal('toggle');
                show_toast("Cập nhật dữ liệu thành công!!", "success");
            }
        },
        error:function(_a, _b, _c){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");
        }
    })
});

$(document).on('click', '.delete', function(){
    var id = $(this).attr('id');
    if(confirm("Bạn có thực sự muốn xóa Quyền này")){
        $.ajax({
            url: "{{ route('ajax.remove_phan_quyen') }}",
            method: "get",
            data:{id:id},
            success:function(data){
                $('#list-table').DataTable().ajax.reload();
                show_toast("Xóa dữ liệu thành công!!", "success");
            },
            error:function(_a, _b, _C){
                show_toast("Xóa dữ liệu không thành công!!", "warning");
            }            
        })
    }
});

$(document).on('click', '.edit', function(){
    var id = $(this).attr("id");
    // $('#form_output').html(data.success);
    $.ajax({
        url:"{{ route('ajax.get_phan_quyen') }}",
        method:'get',
        data:{id:id},
        dataType:'json',
        success:function(data){
            $('#input_name_pq').val(data.ten_quyen);
            $('#input_ma_pq').val(data.ma_phan_quyen);
            $('#phan_quyen_id').val(id);
            $('#action').val('Edit');
            $('.modal-title').text('Edit Data');
            $('#button_action').val('update');
            show_toast("Cập nhật dữ liệu thành công!!", "success");
        },
        error:function(_a, _b, _c){
            show_toast("Cập nhật dữ liệu không thành công!!", "warning");
        }
    });
});
</script>
@endsection
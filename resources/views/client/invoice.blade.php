<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
    <title>Admin</title>
    <!-- Bootstrap CSS -->
    <!-- <base href="{{asset('')}}"> -->
    <!-- <link rel="stylesheet" href="source/assets/vendor/bootstrap/css/bootstrap.min.css"> -->
    <style type="text/css">
        * {
            font-family: "DejaVu Sans";
            font-size: 12px;
        }
        h3 {
           font-family: "DejaVu Sans";
         }

        hr{
            border: 1px solid black;
        }
        .money{
            font-size: 14px;
        }
        .topping{
            font-size: 10px;
            margin-left: 5px;
        }
        .text-center{
            text-align: center;
        }
        .text-right{
            float: right;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-12" style="text-align: center;">
                <img src="images/{{ $thong_tin->logo}}" style="height: 70px; width: 70px;">
            </div>
        </div>
        <div class="row text-center">
            <div class="col-12">
                <span><b>{{ $thong_tin->name }}</b></span>
                <br>
                <span>{{ $thong_tin->address}}</span>
                <br>
                <span>Số điện thoại: {{ $thong_tin->phone_number }}</span>
                <br>
                <h3><b>Phiếu Thanh Toán</b></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <span>Thời gian tạo: {!! date('d-m-y H:i:s') !!}</span>
                <br>
                <span>Loại Hóa Đơn: <b>Mang Về</b></span>
            </div>
        </div>
        <hr>
        <div class="row">
            <table class="table table-sm" style="width: 100%;">
              <thead>
                <tr>
                  <th scope="col" style="width: 50%;">Tên</th>
                  <th scope="col" style="width: 10%;">SL</th>
                  <th scope="col" style="width: 20%;">Đ.Giá</th>
                  <th scope="col" style="width: 20%;">T.Tiền(VND)</th>
                </tr>
              </thead>
              <tbody>
                @forelse($bill_data['items'] as $item)
                    <tr>
                      <td><span>{{$item['ten_san_pham']}}</span>
                        @forelse($item['options'] as $option)
                            <br>
                            <span class="topping">{{ $option['ten_topping'] }}</span>
                        @empty
                        @endforelse
                      </td>
                      <td><span>{{$item['so_luong']}}</span></td>
                      <td><span>{{$item['price']}}</span>
                        @forelse($item['options'] as $option)
                            <br>
                            <span class="topping">{{ $option['topping_price'] }}</span>
                        @empty
                        @endforelse
                      </td>
                      <td>
                        @if($item['tong_tien'] != $item['tong_cong'])
                            <strike>{{ $item['tong_cong'] }} VND</strike>
                            <br>
                            <span>{{ $item['tong_tien'] }} VND</span>
                        @else
                            <span>{{$item['tong_tien']}} VND</span>
                        @endif
                      </td>
                    </tr>
                @empty
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                @endforelse
                <tr>
                    <td>Tổng cộng:</td>
                    <td>{{ $bill_data['tong_sl']}}</td>
                    <td></td>
                    <td>{{$bill_data['tong_cong']}} VND</td>
                </tr>
                <tr>
                    <td><b>Tổng tiền:</b></td>
                    <td></td>
                    <td></td>
                    <td><b>{{$bill_data['tong_tien']}} VND</b></td>
                </tr>
              </tbody>
            </table>    
        </div>
        <hr>
        <div class="row">
            <div class="col-12">
                <b class="money">KHÁCH THANH TOÁN</b>
                <b class= "money text-right">{{$bill_data['khach_tra']}} VND</b>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div class="row">
            <div class="col-12">
                <b>Nhận tiền mặt: </b>
                <b class="text-right">{{$bill_data['khach_tra']}} VND</b>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div class="row">
            <div class="col-12">
                <b>Tiền trả: </b>
                <b class="text-right">{{$bill_data['tien_thua']}} VND</b>
            </div>
        </div>
        <hr>
        <div class="row text-center">
            <div class="col-12">
                <b>Wifi: CiteaFun1, 2, 3</b>
                <br>
                <b>Pass: {{ $thong_tin->pass_wifi }}</b>  
            </div>
        </div>
        <hr>
        <div class="row text-center">
            Citea Fun xin chào quý khách!!!
        </div>
    </div>
</body>
 
</html>
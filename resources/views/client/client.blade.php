<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Order sản phẩm</title>
    <!-- Bootstrap CSS -->
    <base href="{{asset('')}}">
    <link rel="stylesheet" href="source/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="source/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="source/assets/libs/css/style.css">
    <link rel="stylesheet" href="source/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="css/client.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
         <!-- ============================================================== -->
        <!-- navbar -->
        @include('layouts.admin_header')

    </div>

    <div class="container-fluid">
            <div class="row">
                <div class="col-md-5 left-side">
                    <div class="row">
                        <div class="col-5 table-header">
                            <h3>Sản phẩm</h3>
                        </div>
                        <div class="col-2 table-header">
                            <h3>Giá</h3>
                        </div>
                        <div class="col-3 table-header">
                            <h3 class="text-left">Số Lượng</h3>
                        </div>
                        <div class="col-2 table-header">
                            <h3>Tổng Tiền</h3>
                        </div>
                    </div>

                    <div id="productList">
                            <div class="col-12">
                                
                            </div>
                        </div>

                    <div class="footer-section">
                        <div class="table-responsive col-sm-12 totalTab">
                           <table class="table">
                              <tr>
                                 <td class="active" width="40%">SubTotal</td>
                                 <td class="whiteBg" width="60%"><span id="Subtot"></span> VND                        <span class="float-right"><b id="ItemsNum"><span></span> </b>  items</span>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="active">Discount</td>
                                 <td class="whiteBg">

                                    @if(count($ListPromotion) == 0)
                                        <h5>Không có chương trình khuyến mại</h5>
                                    @else
                                        <select class="form-control" name = "promotion" id="input_promotion" onchange="update_promotion()">
                                            @forelse($ListPromotion as $Promotion)
                                                <option value = "{{ $Promotion->id }}">{{$Promotion->ten_promotion}}</option>
                                            @empty
                                                <option value = "empty">Không có khuyến mãi</option>
                                            @endforelse
                                        </select>
                                    @endif
                                 </td>
                              </tr>
                              <tr>
                                 <td class="active">Tổng Tiền</td>
                                 <td class="whiteBg light-blue text-bold"><span id="total">0</span> VND</td>
                              </tr>
                           </table>
                        </div>
                        <div class="row">
                          <div class="col-md-6 payment-div">
                            <button type="button" onclick="cancelPOS()" class="shadow btn btn-red col-12 flat-box-btn rounded">Cancel</button>
                          </div>
                          
                          <div class="col-md-6 payment-div">
                            <button type="button" class="shadow btn btn-green col-12 flat-box-btn rounded" data-toggle="modal" data-target="#caculate">Thanh Toán</button>
                          </div>
                        </div>
                     </div>
                </div>

                <div class="col-md-7 right-side">
                    <div class="row">
                        <ul class="nav">
                            @forelse($ListLoaiSanPham as $LoaiSanPham)
                                <li class="nav-item">
                                    <span class="categories" id="{{$LoaiSanPham->ma_loai_san_pham}}">{{ $LoaiSanPham->ten_loai_san_pham}}</span>
                                </li>
                            @empty
                                <li class="nav-item">
                                    EMPTY
                                </li>
                            @endforelse
                        </ul>
                        <div class="col-sm-12">
                            <div class="input-group custom-search-form">
                              <input type="text" class="form-control" id="searchPro">
                              <span class="input-group-btn">
                              <button class="btn btn-default" type="button">
                              <span class="fas fa-search"></span>
                             </button>
                             </span>
                            </div>
                        </div>
                        <div id="productList2" class="container row">
                            {{-- create dynamic list --}}

                        </div>
                      </div>
                  </div>
                </div>
            </div>

            <div class="modal fade" id="options" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
             <div class="modal-dialog" role="document" id="ticketModal">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ticket">Add Options</h4>
                  </div>
                  <div class="modal-body" id="modal-body">
                     <div id="optionsSection">
                       @forelse($Topping as $Topping_item)
                            @if($Topping_item->loai_topping == "TFT")
                                <label class="checkbox-inline"><input type="checkbox" value="{{$Topping_item->id}}" id="topping_{{$Topping_item->id}}" name="topping_{{$Topping_item->id}}">{!! $Topping_item->ten_topping !!}</label><br>
                            @elseif($Topping_item->loai_topping == "OTH")
                                <h4>{!! $Topping_item->ten_topping !!}</h4>
                                <div class="custom-control custom-radio">
                                  <input type="radio" class="custom-control-input" id="topping_{{$Topping_item->id}}_1" name="topping_{{$Topping_item->id}}" value="0%">
                                  <label class="custom-control-label" for="topping_{{$Topping_item->id}}_1">0%</label>
                                </div>
                                <div class="custom-control custom-radio">
                                  <input type="radio" class="custom-control-input" id="topping_{{$Topping_item->id}}_2" name="topping_{{$Topping_item->id}}" value="30%">
                                  <label class="custom-control-label" for="topping_{{$Topping_item->id}}_2">30%</label>
                                </div>
                                <div class="custom-control custom-radio">
                                  <input type="radio" class="custom-control-input" id="topping_{{$Topping_item->id}}_3" name="topping_{{$Topping_item->id}}" value="50%">
                                  <label class="custom-control-label" for="topping_{{$Topping_item->id}}_3">50%</label>
                                </div>
                                <div class="custom-control custom-radio">
                                  <input type="radio" class="custom-control-input" id="topping_{{$Topping_item->id}}_4" name="topping_{{$Topping_item->id}}" value="70%">
                                  <label class="custom-control-label" for="topping_{{$Topping_item->id}}_4">70%</label>
                                </div>
                                <div class="custom-control custom-radio">
                                  <input type="radio" class="custom-control-input" id="topping_{{$Topping_item->id}}_5" name="topping_{{$Topping_item->id}}" value="100%">
                                  <label class="custom-control-label" for="topping_{{$Topping_item->id}}_5">100%</label>
                                </div>
                            @endif
                       @empty
                        <h3>No Options</h3>
                       @endforelse
                     </div>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="product_id_topping" id="product_id_topping">
                    <button type="button" class="btn btn-default hiddenpr" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-add" onclick="addPoptions()">OK</button>
                  </div>
                </div>
             </div>
            </div>

    <div class="modal fade" id="caculate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog" role="document" id="caculateModal">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="caculate">Thanh toán</h4>
          </div>
          <div class="modal-body" id="modal-body">
             <div class="row">
                <div class="col-12">
                    <label>Hình thức thanh toán</label>
                    @forelse($LoaiHoaDons as $LoaiHoaDon)
                      <div class="custom-control custom-radio">
                      <input type="radio" class="custom-control-input" id="payment_type_{{$LoaiHoaDon->ma_loai_hoa_don}}" name="payment_type" value="{{$LoaiHoaDon->ma_loai_hoa_don}}" required>
                      <label class="custom-control-label" for="payment_type_{{$LoaiHoaDon->ma_loai_hoa_don}}">{{$LoaiHoaDon->ten_loai_hoa_don}}</label>
                    </div>
                    @empty
                    <h5>Không có hình thức thanh toán nào</h5>
                    @endforelse
                    
                </div>
                <div class="col-12">
                    <label>Khách thanh toán</label>
                    <input type="number" id="client_money" name="client_money" class="form-control" onkeyup="changeMoney()" value="0">
                    <label>Tiền thừa</label>
                    <input type="number" id="excess_cash" name="excess_cash" class="form-control" readonly="true" value="0">
                </div>
             </div>
          </div>
          <div class="row">
            <div class="col-sm-1"></div>
              <div class="col-10">
                <div class="row">
                    <div class="col-4 caculate">
                      <button type="button" class="btn btn-block btn-default" onclick="addvalue('7')">7</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-default" onclick="addvalue('8')">8</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-default" onclick="addvalue('9')">9</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-default" onclick="addvalue('4')">4</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-default" onclick="addvalue('5')">5</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-default" onclick="addvalue('6')">6</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-default" onclick="addvalue('1')">1</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-default" onclick="addvalue('2')">2</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-default" onclick="addvalue('3')">3</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-danger" onclick="allClear()">AC</button>
                      </div>
                      <div class="col-4 caculate">
                          <button type="button" class="btn btn-block btn-default" onclick="addvalue('0')">0</button>
                      </div>
                </div>
              </div>
            <div class="col-sm-1"></div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="product_id_topping" id="product_id_topping">
            <button type="button" class="btn btn-default hiddenpr" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success" onclick="submitCart()">OK</button>
          </div>
        </div>
     </div>
    </div>
    

    <div class="toast mt-8 fixed-top">
      <div class="toast-header">
        Thông báo
      </div>
      <div id="toast_content" class="toast-body alert alert-danger">
        Bị lỗi
      </div>
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="source/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="source/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="source/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="source/assets/libs/js/main-js.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        var ListProducts = {!! json_encode($ListSanPham->toArray()) !!};
        var CartBase = {!! json_encode($cart) !!};
        var ListTopping = {!! json_encode($Topping->toArray()) !!};
       
        $(function() {
            createProductList2("MIT");
            if(jQuery.isEmptyObject(CartBase)){
                $("#productList").children('div').html("<h3>No Items In Cart</h3>");
            } else{
                updateProductList(CartBase);
            }
        });

        $(".categories").on("click", function (){
            $(".categories").css('border-bottom', '3px solid #212C32');
            var Category = $(this).attr('id');
            createProductList2(Category);
            $(this).css('border-bottom', '3px solid red'); 
        });

        function createProductList2(Id){
            var productsDiv = "";
            for(let i = 0; i < ListProducts.length; i++){
                product = ListProducts[i];
                if(product.loai_san_pham == Id){
                    productsDiv = productsDiv + '<div class="col-sm-2 col-4 product_contain"><a href="javascript:void(0)" class="addPct" id="product-'+product.id+'" onclick="add_posale(\'product-'+product.id+'\')"><div class="product"><h3 id="proname">'+product.ten_san_pham+'</h3><h3 id="proprice">'+product.price+' VND</h3><input type="hidden" id="product_id" name="product_id" value="'+product.id+'" /><input type="hidden" id="name" name="name" value="'+product.ten_san_pham+'" /><input type="hidden" id="price" name="price" value="'+product.price+'" /><input type="hidden" id="category" name="category" value="'+product.loai_san_pham+'" /></div></a></div>';
                }
            }
            $("#productList2").html(productsDiv);
        };

        function add_posale(Id){
            var ProductObj = $("#"+Id).children("div");
            var product_id = ProductObj.children("#product_id").val();
            var name = ProductObj.children("#name").val(); 
            var price = ProductObj.children("#price").val();
            // var loai_san_pham = ProductObj.children("#category").val();
            var promotion_id = getPromotionId();
            
            $.ajax({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('ajax.add_posale') }}",
                method: "post",
                data:{product_id:product_id, name:name, price:price, promotion_id: promotion_id},
                dataType: "json",
                success:function(data){
                    updateProductList(data);
                    $("#ItemsNum").html(data.items.length);
                    $("#Subtot").html(data.total_cart_price);
                    $("#total").html(data.final_total_price);
                },

                error: function (jqXHR, textStatus, errorThrown)
                 {
                    alert(errorThrown);
                 }           
            });

        }

        function updateProductList(data){
            $("#productList").children('div').html("");
            var items = data.items;
            if(items == undefined){
                $("#productList").children('div').html("<h3>No Items In Cart</h3>");
                $("#ItemsNum").html("0");
                $("#Subtot").html("0");
                $("#total").html("0");
            }
            else if( items.length == 0 ){
                $("#productList").children('div').html("<h3>No Items In Cart</h3>");
                $("#ItemsNum").html("0");
                $("#Subtot").html("0");
                $("#total").html("0");
            }
            else{
                for(var i = 0; i < items.length; i++){
                    var item = items[i];
                    var optionItemName = createItemOptionName(item);
                    var optionItemPrice = createItemOptionPrice(item);
                    var totalItemPrice = createTotalItemPrice(item);
                    var Div = '<div class="panel panel-default product-details">'+
                                '<div class="panel-body">'+
                                    '<div class="row">'+
                                        '<div class="col-5 nopadding">'+
                                            '<div class="row">'+
                                                '<div class="col-2 nopadding" >'+
                                                    '<a href="javascript:void(0)" onclick="delete_posale(\''+item.cart_id+'\')">'+
                                                        '<span class="fa-stack fa-sm productD">'+
                                                        '<i class="fa fa-circle fa-stack-2x delete-product"></i>'+
                                                        '<i class="fa fa-times fa-stack-1x fa-fw fa-inverse"></i>'+
                                                        '</span>'+
                                                    '</a>'+
                                                '</div>'+
                                                '<div class="col-10 nopadding" >'+
                                                    '<span class="textPD">'+item.name+'</span>'+
                                                    optionItemName+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-2 nopadding">'+
                                            '<span class="textPD">'+item.price+'</span>'+
                                            optionItemPrice+
                                        '</div>'+
                                        '<div class="col-3 nopadding productNum">'+
                                            '<a href="javascript:void(0)" onclick="minusQuantity(\''+item.cart_id+'\')">'+
                                                '<span class="fa-stack fa-sm decbutton">'+
                                                    '<i class="fa fa-square fa-stack-2x light-grey"></i>'+
                                                    '<i class="fa fa-minus fa-stack-1x fa-inverse white"></i>'+
                                                '</span>'+
                                            '</a>'+
                                            '<input type="text" id="qt-'+item.cart_id+'" onchange="edit_posale(2202)" class="form-control" value="'+item.quantity+'" placeholder="0" maxlength="3" disabled>'+
                                            '<a href="javascript:void(0)" onclick="addQuantity(\''+item.cart_id+'\')">'+
                                                '<span class="fa-stack fa-sm incbutton">'+
                                                    '<i class="fa fa-square fa-stack-2x light-grey"></i>'+
                                                    '<i class="fa fa-plus fa-stack-1x fa-inverse white"></i>'+
                                                '</span>'+
                                            '</a>'+
                                        '</div>'+
                                        '<div class="col-2 nopadding">'+totalItemPrice+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<button type="button" onclick="addoptions(\''+item.cart_id+'\')" class="btn btn-success btn-xs">Options</button> <span id="pooptions-2202"></span>'+
                            '</div>';
                $("#productList").children('div').append(Div);
             }
                $("#ItemsNum").html(data.items.length);
                $("#Subtot").html(data.total_cart_price);
                $("#total").html(data.final_total_price);
            }
            
        }

        function cancelPOS(){
            if(confirm("Xóa bỏ đơn hàng này")){
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('ajax.remove_cart') }}",
                    method: "get",
                    data:{action:"remove"},
                    dataType: "json",
                    success:function(data){
                       updateProductList(data);
                       show_toast("Đã xóa đơn hàng thành công!", "success"); 
                    },
                    error:function(_a, _b, _c){
                        show_toast("Xóa đơn hàng không thành công!", "warning");
                    }            
                })
            }
        }

        function addoptions(CartId){
            for(var i = 0; i < ListTopping.length; i++){
                var Topping = ListTopping[i];
                var Topping_name = "topping_" + Topping.id;

                if($("input[name='"+Topping_name+"']:checked").val()){
                    $("input[name='"+Topping_name+"']:checked").prop('checked', false);
                }
            }
            $("#product_id_topping").val(CartId);
            $('#options').modal('show');
        }

        function addPoptions(){
            var CartId = $("#product_id_topping").val();
            var promotion_id = getPromotionId();
            var ItemOptions = [];
            for(var i = 0; i < ListTopping.length; i++){
                var Topping = ListTopping[i];
                var Topping_name = "topping_" + Topping.id;

                if($("input[name='"+Topping_name+"']:checked").val()){
                    var Value = $("input[name='"+Topping_name+"']:checked").val();
                    var Option;
                    
                    if(getToppingType(Topping.id) != "OTH"){
                        Option = {id: Topping.id};
                        ItemOptions.push(Option);
                    }else{
                        Option = {id: Topping.id, ghi_chu: Value};
                        ItemOptions.push(Option);
                    }
                }
                $('#options').modal('hide');
            }
        
            $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('ajax.add_product_option') }}",
                    method: "post",
                    data:{cart_id: CartId, option: ItemOptions, promotion_id:promotion_id},
                    dataType: "json",
                    success:function(data){
                        updateProductList(data);
                    }            
                })
        }

        function getToppingType(id){
            for(var i =0; i < ListTopping.length; i++){
                if(id == ListTopping[i].id){
                    return ListTopping[i].loai_topping;
                }
            }
        }

        function createItemOptionName(Item){
            var htmlOption = '';
            if(Item.option != undefined){
                for(var i = 0; i < Item.option.length; i++){
                    option = Item.option[i];
                    var ghi_chu;
                    if(option.ghi_chu != undefined){
                        ghi_chu = ' ('+option.ghi_chu+')';
                    }
                    else{
                        ghi_chu = '';
                    }
                    htmlOption = htmlOption + '<br><span>'+option.name+ghi_chu+'</span>'
                }
            }
            return htmlOption;
        }

        function createItemOptionPrice(Item){
            var htmlOption = '';
            if(Item.option != undefined){
                for(var i = 0; i < Item.option.length; i++){
                    option = Item.option[i];
                    var price;
                    if(option.price != undefined){
                        price = ' ('+option.price+')';
                    }
                    else{
                        price = '';
                    }
                    htmlOption = htmlOption + '<br><span>'+option.price+'</span>'
                }
            }
            return htmlOption;
        }

        function createTotalItemPrice(Item){
            var htmlPrice = '';
            if(Item.discount_price != undefined){
                htmlPrice = '<span class="subtotal textPD"><strike>'+Item.total_price+' VND</strike></span>'+
                '<br><span class="subtotal textPD">'+Item.discount_price+' VND</span>';
            } else {
                htmlPrice = '<span class="subtotal textPD">'+Item.total_price+' VND</span>';
            }
            return htmlPrice;
        }

        function delete_posale(CartId){
            $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('ajax.remove_production') }}",
                method: "GET",
                data:{cart_id: CartId},
                dataType: "json",
                success:function(data){
                    updateProductList(data);
                }            
            })
        }

        function update_promotion(){
            var promotion_id = getPromotionId();
            $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('ajax.update_promotion') }}",
                method: "GET",
                data:{promotion_id: promotion_id},
                dataType: "json",
                success:function(data){
                    updateProductList(data);
                }            
            })
        }

        function getPromotionId(){
            var promotion_id = -1;
            if($('#input_promotion').length > 0){
                promotion_id = $('#input_promotion').val();
            }
            return promotion_id;
        }

        function minusQuantity(CartId){
            $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('ajax.minus_quantity') }}",
                method: "GET",
                data:{cart_id: CartId},
                dataType: "json",
                success:function(data){
                    updateProductList(data);
                }            
            })
        }

        function addQuantity(CartId){
            $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('ajax.add_quantity') }}",
                method: "GET",
                data:{cart_id: CartId},
                dataType: "json",
                success:function(data){
                    updateProductList(data);
                }            
            })
        }

        $("#searchPro").keyup(function(){
            $("#productList2").html("");
            var filter = $(this).val();

            for(var i = 0; i < ListProducts.length; i++){
                if(ListProducts[i].ten_san_pham.search(new RegExp(filter, "i")) >= 0){
                    var product = ListProducts[i];
                    var Div = '<div class="col-sm-2 col-4 product_contain"><a href="javascript:void(0)" class="addPct" id="product-'+product.id+'" onclick="add_posale(\'product-'+product.id+'\')"><div class="product"><h3 id="proname">'+product.ten_san_pham+'</h3><h3 id="proprice">'+product.price+' VND</h3><input type="hidden" id="product_id" name="product_id" value="'+product.id+'" /><input type="hidden" id="name" name="name" value="'+product.ten_san_pham+'" /><input type="hidden" id="price" name="price" value="'+product.price+'" /><input type="hidden" id="category" name="category" value="'+product.loai_san_pham+'" /></div></a></div>';
                    $("#productList2").append(Div);
                }   
            }
        });

        function addvalue(Value){
            Money = $("#client_money").val();
            TotalMoney = $("#total").text();
            if(Money == 0){
                $("#client_money").val(Value);
            } else{
                $("#client_money").val(Money + Value);
            } 

            if(parseInt(Money + Value) > parseInt(TotalMoney)){
                $("#excess_cash").val(parseInt(Money + Value) - parseInt(TotalMoney));
            } 
        };

        function allClear(){
            $("#client_money").val("0");
            $("#excess_cash").val("0");
        }

        function changeMoney(){
            Money = $("#client_money").val();
            TotalMoney = $("#total").text();
            if(parseInt(Money) > parseInt(TotalMoney)){
                $("#excess_cash").val(parseInt(Money) - parseInt(TotalMoney));
            } else{
                $("#excess_cash").val("0");
            }
        }

        function submitCart(){
          PaymentType = $("input[name='payment_type']:checked").val();
          Money = $("#client_money").val();
          TotalMoney = $("#total").text();
          ExcessCash = $("#excess_cash").val();
          if(PaymentType == undefined){
            show_toast("Chưa chọn hình thức mua hàng!!", "warning");
          } else if(parseInt(Money) < parseInt(TotalMoney)){
            show_toast("Tiền Khách trả chưa đủ!!", "warning");
          }
          else {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "{{ route('ajax.submit_cart') }}",
              method: "POST",
              data:{payment_type: PaymentType, client_money:Money, excess_cash:ExcessCash},
              dataType: "json",
              success:function(data){
                  $('#caculate').modal('hide');
                  updateProductList(data.items);
                  allClear();
                  $("input[name=payment_type]:checked").prop('checked', false);
                  window.open(data.url, '_blank');
              }            
            })
          }
        }

        function show_toast(Text, Type){
          if(Type == "warning"){
            $('#toast_content').removeClass("alert-success");
            $('#toast_content').addClass("alert-danger");
            $('.toast').toast({delay: 2000});
            $('#toast_content').text(Text);
            $('.toast').toast('show');
          } else if(Type == "success"){
            $('#toast_content').removeClass("alert-danger");
            $('#toast_content').addClass("alert-success");
            $('.toast').toast({delay: 2000});
            $('#toast_content').text(Text);
            $('.toast').toast('show');
          }
        }
    </script>
</body>
 
</html>
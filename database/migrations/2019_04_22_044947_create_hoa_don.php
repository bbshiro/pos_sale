<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoaDon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoa_don', function (Blueprint $table) {
            $table->string('ma_hoa_don', 12)->primary();
            $table->dateTime('created_at');
            $table->float('tong_tien', 12, 2);
            $table->bigInteger('user_id')->unsigned();
            $table->string('loai_hoa_don', 3);
            $table->float('tong_tien_discount')->nullable();
            $table->bigInteger('promotion_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoa_don');
    }
}

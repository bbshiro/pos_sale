<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeChiTietPromotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chi_tiet_promotion', function(Blueprint $table)
        {   
            $table->bigInteger('promotion_id')->unsigned();
            $table->bigInteger('san_pham_id')->unsigned();
            $table->primary(['promotion_id', 'san_pham_id']);
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chi_tiet_promotion');
    }
}

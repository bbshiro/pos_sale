<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hoa_don', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('loai_hoa_don')->references('ma_loai_hoa_don')->on('loai_hoa_don')->onDelete('cascade');
            $table->foreign('promotion_id')->references('id')->on('promotion')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hoa_don', function(Blueprint $table)
        {
            $table->dropForeign('promotion_id');
            $table->dropForeign('user_id');
            $table->dropForeign('loai_hoa_don');
        });
    }
}

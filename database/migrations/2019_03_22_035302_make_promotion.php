<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePromotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion', function(Blueprint $table)
        {   
            $table->bigIncrements('id');
            $table->string('ten_promotion', 120);
            $table->date('time_start');
            $table->date('time_end');
            $table->integer('percent_discount');
            $table->string('loai_promotion', 3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion');
    }
}

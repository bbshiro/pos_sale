<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyChiTietPromotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chi_tiet_promotion', function(Blueprint $table)
        {
            $table->foreign('promotion_id')->references('id')->on('promotion')->onDelete('cascade');
            $table->foreign('san_pham_id')->references('id')->on('san_pham')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chi_tiet_promotion', function(Blueprint $table)
        {
            $table->dropForeign('promotion_id');
            $table->dropForeign('san_pham_id');
        });
    }
}

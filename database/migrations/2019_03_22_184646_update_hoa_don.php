<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHoaDon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hoa_don', function(Blueprint $table)
        {
            $table->float('tong_tien_discount')->default(0);
            $table->bigInteger('promotion_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hoa_don', function(Blueprint $table)
        {
            $table->dropColumn('tong_tien_discount');
            $table->dropColumn('promotion_id');
        });
    }
}

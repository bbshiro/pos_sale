<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyTopping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topping', function(Blueprint $table)
        {
            $table->foreign('loai_topping')->references('ma_loai_topping')->on('loai_topping')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topping', function(Blueprint $table)
        {
            $table->dropForeign('loai_topping'); //
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyTuyChon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tuy_chon', function(Blueprint $table)
        {
            $table->foreign('id_chi_tiet_hoa_don')->references('id')->on('chi_tiet_hoa_don')->onDelete('cascade');
            $table->foreign('id_topping')->references('id')->on('topping')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tuy_chon', function(Blueprint $table)
        {
            $table->dropForeign('id_chi_tiet_hoa_don');
            $table->dropForeign('id_topping');
        });
    }
}

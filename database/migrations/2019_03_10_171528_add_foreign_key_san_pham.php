<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeySanPham extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('san_pham', function(Blueprint $table)
        {
            $table->foreign('loai_san_pham')->references('ma_loai_san_pham')->on('loai_san_pham')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('san_pham', function(Blueprint $table)
        {
            $table->dropForeign('loai_san_pham'); //
        });
    }
}

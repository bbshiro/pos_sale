<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeTuyChon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tuy_chon', function(Blueprint $table)
        {   
            $table->bigIncrements('id');
            $table->bigInteger('id_chi_tiet_hoa_don')->unsigned();
            $table->bigInteger('id_topping')->unsigned();
            $table->enum('percent', [0, 30, 50, 100]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tuy_chon');
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(loaiSanPhamSeeder::class);
         $this->call(loaiToppingSeeder::class);
    }
}

class loaiSanPhamSeeder extends Seeder
{
	public function run()
    {	
    	$data = $this->getLoaiSanPham();
        DB::table('loai_san_pham')->insert($data);
    }

    public function getLoaiSanPham(){
    	return [
	    	["ma_loai_san_pham" => "IBL", "ten_loai_san_pham" => "Ice Blend"],
	    	["ma_loai_san_pham" => "WDR", "ten_loai_san_pham" => "Winter Drinks"],
	    	["ma_loai_san_pham" => "SPT", "ten_loai_san_pham" => "Special Tea"],
	    	["ma_loai_san_pham" => "SMT", "ten_loai_san_pham" => "Smoothie"],
	    	["ma_loai_san_pham" => "COF", "ten_loai_san_pham" => "Coffee"],
	    	["ma_loai_san_pham" => "MIT", "ten_loai_san_pham" => "Milk Tea"],
	    	["ma_loai_san_pham" => "CPT", "ten_loai_san_pham" => "Classic Pure Tea"],
	    	["ma_loai_san_pham" => "FRM", "ten_loai_san_pham" => "Fresh Milk"],
	    	["ma_loai_san_pham" => "YOG", "ten_loai_san_pham" => "Sữa chua"],
	    	["ma_loai_san_pham" => "FRT", "ten_loai_san_pham" => "Fruit Tea"]
    	];
    }
}

class loaiToppingSeeder extends Seeder{
	public function run()
    {
        $data = $this->getLoaiTopping();
        DB::table('loai_topping')->insert($data);
    }

    public function getLoaiTopping(){
    	return ["ma_loai_topping" => "TFT", "ten_loai_topping" => "Topping for Tea"];
    }
}
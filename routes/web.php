<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', 'pageController@login')->name('login');
Route::get('/logout', 'pageController@logout')->name('logout');
Route::post('/check_login', 'pageController@checkLogin')->name('check_login');

Route::group(['prefix' => 'admin', 'middleware' => ['login', 'phanquyen']], function(){
	Route::get('table', 'pageController@getTable');
	Route::group(['prefix' => 'thuc_don'], function() {
		Route::get('/data/loai_sp', 'DatatablesController@loaiSanPhamData')->name('loaiSpData');
		Route::get('/loaisp', 'DatatablesController@getIndexLoaiSanPham')->name('loai_sp');

		Route::get('/data/loai_topping', 'DatatablesController@loaiToppingData')->name('loaiToppingData');
		Route::get('/loai_topping', 'DatatablesController@getIndexLoaiTopping')->name('loai_topping');

		Route::post('/data/san_pham', 'DatatablesController@sanPhamData')->name('sanPhamData');
		Route::get('/san_pham', 'DatatablesController@getIndexSanPham')->name('san_pham');

		Route::post('/data/topping', 'DatatablesController@toppingData')->name('toppingData');
		Route::get('/topping', 'DatatablesController@getIndexTopping')->name('topping');		
	});

	Route::group(['prefix' => 'nhan_vien'], function() {
	    Route::get('phan_quyen', 'DatatablesController@getIndexPhanQuyen')->name('phan_quyen');
	    Route::get('/data/phan_quyen', 'DatatablesController@phanQuyenData')->name('phanQuyenData');

	    // Nhân viên
	    Route::get('user', 'DatatablesController@getIndexUser')->name('user');
	    Route::post('/data/user', 'DatatablesController@userData')->name('userData');
	});

	Route::group(['prefix' => 'marketing'], function() {
	    Route::get('loai_promotion', 'DatatablesController@getIndexLoaiPromotion')->name('loai_promotion');
	    Route::get('/data/loai_promotion', 'DatatablesController@loaiPromotionData')->name('loaiPromotionData');

	    // Promotion
	    Route::get('promotion', 'DatatablesController@getIndexPromotion')->name('promotion');
	    Route::post('/data/promotion', 'DatatablesController@promotionData')->name('promotionData');

	    // Promotion Detail
	    Route::get('promotion_detail', 'DatatablesController@getIndexPromotionDetail')->name('promotion_detail');
	    Route::get('/data/promotion_detail', 'DatatablesController@promotionDetailData')->name('promotionDetailData');
	});

	Route::group(['prefix' => 'hoa_don'], function() {
	    //Loại hóa đơn
	    Route::get('loai_hd', 'DatatablesController@getIndexLoaiHoaDon')->name('loai_hd');
	    Route::get('/data/loai_hd', 'DatatablesController@loaiHoaDonData')->name('loaiHoaDonData');

	    // Hóa Đơn
	    Route::get('hoa_don', 'DatatablesController@getIndexHoaDon')->name('hoa_don');
	    Route::post('/data/hoa_don', 'DatatablesController@hoaDonData')->name('hoaDonData');

	    // Hóa Đơn detail
	    Route::post('/data/hoa_don_detail', 'DatatablesController@hoaDonDataDetail')->name('hoaDonDataDetail');
	});

	Route::group(['prefix' => 'bao_cao'], function() {
	    // Doanh thu
	    Route::get('doanh_thu', 'DatatablesController@getIndexDoanhThu')->name('doanh_thu');
	    Route::get('/data/doanh_thu', 'DatatablesController@doanhThuData')->name('doanhThuData');
	    Route::post('/data/doanh_thu_nhan_vien', 'DatatablesController@doanhThuNhanVien')->name('doanhThuNhanVien');
	    Route::post('/data/doanh_thu_loai_hd', 'DatatablesController@doanhThuLoaiHd')->name('doanhThuLoaiHd');
	    Route::post('/data/doanh_thu_san_pham', 'DatatablesController@doanhThuSanPham')->name('doanhThuSanPham');
	    Route::post('/data/doanh_thu_theo_ngay', 'DatatablesController@doanhThuTheoNgay')->name('doanhThuTheoNgay');
	    Route::post('/data/doanh_thu_theo_gio', 'DatatablesController@doanhThuTheoGio')->name('doanhThuTheoGio');
	    Route::post('/data/doanh_thu_promotion', 'DatatablesController@doanhThuPromotion')->name('doanhThuPromotion');

	    Route::get('chung', 'pageController@getDashboard')->name('dashboard');
	    Route::post('/data/tong_tien_theo_gio', 'AjaxController@tongTienTheoGio')->name('tongTienTheoGio');
	    Route::post('/data/trung_binh_theo_gio', 'AjaxController@trungBinhTheoGio')->name('trungBinhTheoGio');
	    Route::post('/data/tong_tien_theo_ngay', 'AjaxController@tongTienTheoNgay')->name('tongTienTheoNgay');
	    Route::post('/data/trung_binh_theo_ngay', 'AjaxController@trungBinhTheoNgay')->name('trungBinhTheoNgay');
	    Route::post('/data/doanh_thu_theo_san_pham', 'AjaxController@tongTienSanPham')->name('tongTienSanPham');
	    Route::post('/data/so_luong_san_pham', 'AjaxController@soLuongSanPham')->name('soLuongSanPham');
	});

	Route::get('/setting', 'pageController@setting')->name('setting');
});

Route::group(['prefix' => 'ajax', 'middleware' => 'login'], function(){
	//Ajax for loại sản phẩm
	Route::post('/add_loai_sp', 'AjaxController@addLoaiSanPham')->name('ajax.add_loai_sp');
	Route::get('/remove_loai_sp', 'AjaxController@removeLoaiSanPham')->name('ajax.remove_loai_sp');
	Route::get('/get_loai_sp', 'AjaxController@getLoaiSanPham')->name('ajax.get_loai_sp');

	// Ajax for loại topping
	Route::post('/add_loai_topping', 'AjaxController@addLoaiTopping')->name('ajax.add_loai_topping');

	Route::get('/remove_loai_topping', 'AjaxController@removeLoaiTopping')->name('ajax.remove_loai_topping');

	Route::get('/get_loai_topping', 'AjaxController@getLoaiTopping')->name('ajax.get_loai_topping');

	// Ajax for sản phẩm
	Route::post('/add_san_pham', 'AjaxController@addSanPham')->name('ajax.add_sp');

	Route::get('/remove_sp', 'AjaxController@removeSanPham')->name('ajax.remove_sp');

	Route::get('/get_sp', 'AjaxController@getSanPham')->name('ajax.get_sp');

	// Ajax for topping
	Route::post('/add_topping', 'AjaxController@addTopping')->name('ajax.add_topping');

	Route::get('/remove_topping', 'AjaxController@removeTopping')->name('ajax.remove_topping');

	Route::get('/get_topping', 'AjaxController@getTopping')->name('ajax.get_topping');

	// Ajax for phân quyền
	Route::post('/add_pq', 'AjaxController@addPhanQuyen')->name('ajax.add_phan_quyen');

	Route::get('/remove_phan_quyen', 'AjaxController@removePhanQuyen')->name('ajax.remove_phan_quyen');

	Route::get('/get_phan_quyen', 'AjaxController@getPhanQuyen')->name('ajax.get_phan_quyen');

	// Ajax for Loại Promotion
	Route::post('/add_loai_promotion', 'AjaxController@addLoaiPromotion')->name('ajax.add_loai_promotion');

	Route::get('/remove_loai_promotion', 'AjaxController@removeLoaiPromotion')->name('ajax.remove_loai_promotion');

	Route::get('/get_loai_promotion', 'AjaxController@getLoaiPromotion')->name('ajax.get_loai_promotion');

	// Ajax for promotion
	Route::post('/add_promotion', 'AjaxController@addPromotion')->name('ajax.add_promotion');

	Route::get('/remove_promotion', 'AjaxController@removePromotion')->name('ajax.remove_promotion');

	Route::get('/get_promotion', 'AjaxController@getPromotion')->name('ajax.get_promotion');

	// Ajax for nhân viên
	Route::post('/add_user', 'AjaxController@addUser')->name('ajax.add_user');

	Route::get('/remove_user', 'AjaxController@removeUser')->name('ajax.remove_user');

	Route::get('/get_user', 'AjaxController@getUser')->name('ajax.get_user');

	// Ajax for promotion detail
	Route::post('/add_promotion_detail', 'AjaxController@addPromotionDetail')->name('ajax.add_promotion_detail');

	Route::get('/remove_promotion_detail', 'AjaxController@removePromotionDetail')->name('ajax.remove_promotion_detail');

	// Ajax for loại hóa đơn
	Route::post('/add_loai_hd', 'AjaxController@addLoaiHoaDon')->name('ajax.add_loai_hd');

	Route::get('/remove_loai_hd', 'AjaxController@removeLoaiHoaDon')->name('ajax.remove_loai_hd');

	Route::get('/get_loai_hd', 'AjaxController@getLoaiHoaDon')->name('ajax.get_loai_hd');

	// Ajax for client
	Route::post('/client/add_posale', 'ClientController@addPosale')->name('ajax.add_posale');
	Route::get('/client/remove_cart', 'ClientController@removeCart')->name('ajax.remove_cart');
	Route::post('/client/add_product_option', 'ClientController@addProductOption')->name('ajax.add_product_option');
	Route::get('/client/remove_production', 'ClientController@removeProduction')->name('ajax.remove_production');
	Route::get('/client/update_promotion', 'ClientController@updatePromotion')->name('ajax.update_promotion');
	Route::get('/client/minus_quantity', 'ClientController@minusQuantity')->name('ajax.minus_quantity');
	Route::get('/client/add_quantity', 'ClientController@addQuantity')->name('ajax.add_quantity');
	Route::post('/client/submit_cart', 'ClientController@submitCart')->name('ajax.submit_cart');

	// filter
	Route::post('/admin/filter_sp', 'AjaxController@filterSP')->name('ajax.filter_sp');
	Route::post('/upload', "AjaxController@upload")->name('ajax.upload');
	Route::post('/print_bill', "AjaxController@printBill")->name('ajax.print_bill');
});

Route::get('client', 'ClientController@getClient')->name('client')->middleware('login');

Route::get('/getInvoice/{ma_hoa_don}', 'ClientController@getInvoice')->name('getInvoice')->middleware('login');

Route::get('/getInvoice', function() {
    return view('client.invoice');
});



Route::get('/generate-pdf','pageController@generatePDF');

Route::get('/test', function() {
    // $List = new App\HoaDon;
    // $List = $List->with(['loaihoadon', 'user', 'promotion']);
   
    // $List = $List->with(['loaihoadon', 'user', 'promotion'])->where('loai_promotion', 'TAB');
    // $List =$List->get();
    // $List = App\ChiTietHoaDon::with(['sanpham'])->join('tuy_chon', 'chi_tiet_hoa_don.id', '=', 'tuy_chon.id_chi_tiet_hoa_don')->get()->toArray();
    // $List = App\ChiTietHoaDon::with(['sanpham'])->get()->toArray();
    // var_dump($List);
    // $List = App\ChiTietHoaDon::find(19)->topping()->get()->toArray();
    // $List = App\ChiTietHoaDon::all();
    // $List = App\TuyChon::with('topping')->where('id_chi_tiet_hoa_don', '19')->get()->toArray();
    // var_dump($List[0]->join('tuy_chon', 'chi_tiet_hoa_don.id', '=', 'tuy_chon.id_chi_tiet_hoa_don')->get()->toArray());
    // $List = DB::table('hoa_don')->get();
    // $List = DB::table('hoa_don')->select(Db::raw('user_id, count(ma_hoa_don) as so_hoa_don, sum(tong_tien) as tong_tien_hd, sum(tong_tien_discount) as tong_tien_discount_hd, avg(tong_tien) as trung_binh'))->whereRaw('DATE(created_at) = CURRENT_DATE()')->groupBy('user_id')->get();
    // $List = DB::table('hoa_don')->join('chi_tiet_hoa_don', 'hoa_don.ma_hoa_don', '=', 'chi_tiet_hoa_don.ma_hoa_don')->select(Db::raw('user_id, count(DISTINCT hoa_don.ma_hoa_don) as so_hoa_don, sum(hoa_don.tong_tien) as tong_tien_hd, sum(hoa_don.tong_tien_discount) as tong_tien_discount_hd, avg(hoa_don.tong_tien) as trung_binh'))->whereBetween('created_at', array('2019-04-30 00:00', '2019-05-09 23:59'))->groupBy('user_id')->get();
    // $List = DB::table('chi_tiet_hoa_don')->join('san_pham', 'chi_tiet_hoa_don.id_san_pham', '=', 'san_pham.id')->select(Db::raw('id_san_pham, ten_san_pham, count(id_san_pham) as so_luong_tong, sum(tong_tien) as tong_tien_sp, avg(tong_tien) as trung_binh, sum(tong_tien_discount) as tong_tien_discount_sp'))->whereBetween('created_at', array('2019-05-01 00:50', '2019-05-10 00:50'))->groupBy('id_san_pham')->get();
    //  $List = DB::table('hoa_don')->join('chi_tiet_hoa_don', 'hoa_don.ma_hoa_don', '=', 'chi_tiet_hoa_don.ma_hoa_don')->select(Db::raw('id_san_pham, count(DISTINCT chi_tiet_hoa_don.ma_hoa_don) as so_luong_hd, sum(DISTINCT chi_tiet_hoa_don.tong_tien) as tong_tien_sp, sum(DISTINCT chi_tiet_hoa_don.tong_tien_discount) as tong_tien_discount_sp, avg(DISTINCT chi_tiet_hoa_don.tong_tien) as trung_binh, sum(so_luong) as so_luong_sp'))->whereBetween('created_at', array('2019-05-01 00:50', '2019-05-10 00:50'))->groupBy('chi_tiet_hoa_don.id_san_pham')->get();
    // var_dump($List->toArray());
    // foreach ($List as $ele) {
    // 	// var_dump($ele);
    // 	var_dump($ele);
    // 	echo '<br><br>';
    // }
    // print_r( dateRange( '2010/07/26', '2010/08/05') );
    // $List = Db::table('hoa_don')->select('*')->
  	// $hoa_don = App\HoaDon::where('ma_hoa_don', '=', '190504205407')->first();
   //  $cart = array();
   //  $cart['created_at'] = $hoa_don->created_at;
   //  $cart['tong_cong'] = number_format($hoa_don->tong_tien);
   //  $cart['tong_tien'] = number_format($hoa_don->tong_tien - $hoa_don->tong_tien_discount);

   //  $user = App\User::where('id', '=', $hoa_don->user_id)->first();
   //  $cart['nhan_vien'] = $user->name;

   //  $cart['items'] = [];
   //  $cart['tong_sl'] = 0;
   //  $list_chi_tiet_hd = App\ChiTietHoaDon::where('ma_hoa_don', '=', '190504205407')->get();
   //  foreach ($list_chi_tiet_hd as $chi_tiet_hoa_don) {
   //      $item = array();
   //      $san_pham = App\SanPham::where('id', '=', $chi_tiet_hoa_don->id_san_pham)->first();

   //      $item['ten_san_pham'] = $san_pham->ten_san_pham;
   //      $item['price'] = $san_pham->price;
   //      $item['tong_cong'] = number_format($chi_tiet_hoa_don->tong_tien);
   //      $item['tong_tien'] = number_format($chi_tiet_hoa_don->tong_tien - $chi_tiet_hoa_don->tong_tien_discount);
   //      $cart['tong_sl'] = $cart['tong_sl'] + $chi_tiet_hoa_don->so_luong;

   //      $options = App\TuyChon::where('id_chi_tiet_hoa_don', '=', $chi_tiet_hoa_don->id)->get();
   //      if (!empty($options)) {
   //          $item['options'] = [];
   //          foreach ($options as $key => $option) {
   //              $product_option = array();
   //              $topping = App\Topping::where('id', '=', $option->id_topping)->first();
   //              $product_option['ten_topping'] = $topping->ten_topping;
   //              $product_option['topping_price'] = number_format($topping->price);
   //              if($option->percent != null){
   //                  $product_option['percent'] = $option->percent;
   //              }

   //              array_push($item['options'], $product_option);
   //          }
   //      }

   //      array_push($cart['items'], $item);
   //  }

    // $a = json_encode($cart);

    // $List =  DB::table('hoa_don')->join('chi_tiet_hoa_don', 'hoa_don.ma_hoa_don', '=', 'chi_tiet_hoa_don.ma_hoa_don')->select(Db::raw('user_id, count(DISTINCT hoa_don.ma_hoa_don) as so_hoa_don, sum(DISTINCT hoa_don.tong_tien) as tong_tien_hd, sum(DISTINCT hoa_don.tong_tien_discount) as tong_tien_discount_hd, avg(DISTINCT hoa_don.tong_tien) as trung_binh'))->whereBetween('created_at', array('2019-04-22', '2019-04-22'))->groupBy('user_id')->toSql();
    // $List = DB::table('hoa_don')->join('users', 'hoa_don.user_id', '=', 'users.id')->select(Db::raw('user_id, users.name as ten_nv, count(hoa_don.ma_hoa_don) as so_hoa_don, sum(hoa_don.tong_tien) as tong_tien_hd, sum(hoa_don.tong_tien_discount) as tong_tien_discount_hd, avg(hoa_don.tong_tien) as trung_binh'))->whereBetween('hoa_don.created_at', array('2019-04-22', '2019-04-22'))->groupBy('user_id')->toSql();
    // $List = DB::table('hoa_don')->join('chi_tiet_hoa_don', 'hoa_don.ma_hoa_don', '=', 'chi_tiet_hoa_don.ma_hoa_don')->select(Db::raw('id_san_pham, count(DISTINCT chi_tiet_hoa_don.ma_hoa_don) as so_luong_hd, sum(DISTINCT chi_tiet_hoa_don.tong_tien) as tong_tien_sp, sum(DISTINCT chi_tiet_hoa_don.tong_tien_discount) as tong_tien_discount_sp, avg(DISTINCT chi_tiet_hoa_don.tong_tien) as trung_binh, sum(so_luong) as so_luong_sp'))->whereBetween('created_at', array('2019-04-22', '2019-04-23'))->groupBy('chi_tiet_hoa_don.id_san_pham')->toSql();
    // var_dump($a);
	App\ThongTin::update(['name' => $request->shop_name, 'phone_number' => $request->shop_number, 'address' => $request->shop_address, 'pass_wifi' => $request->shop_password]);
    var_dump($List);
})->name('test');
// Route::get('/data/index', 'DatatablesController@getIndex');
// Route::get('/data/loaisp', 'DatatablesController@loaiSanPhamData')->name('loaiSpData');
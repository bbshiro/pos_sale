<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'promotion';
    public $timestamps = false;

   	public function loaipromotion(){
   		return $this->belongsTo('App\LoaiPromotion', 'loai_promotion', 'ma_loai_promotion');
   	}

   	public function chitietpromotion(){
   		return $this->hasMany('App\ChiTietPromotion', 'id', 'promotion_id');
   	}

   	public function hoadon(){
   		return $this->hasMany('App\HoaDon', 'id', 'promotion_id');
   	}
}

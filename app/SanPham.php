<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SanPham extends Model
{
    //
    protected $table = 'san_pham';
    public $timestamps = false;

    public $primary = "id";
    public function loaisanpham(){
    	return $this->belongsTo('App\LoaiSanPham', 'loai_san_pham', 'ma_loai_san_pham');
    }

    public function chitietpromotion(){
    	return $this->hasMany('App\ChiTietPromotion', 'id', 'san_pham_id');
    }

    public function chitiethoadon(){
        return $this->hasMany('App\ChiTietHoaDon', 'id', 'id_san_pham');
    }
}

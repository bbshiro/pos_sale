<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChiTietPromotion extends Model
{
    protected $table = 'chi_tiet_promotion';
    public $timestamps = false;

    public function promotion(){
    	return $this->belongsTo('App\Promotion', 'promotion_id', 'id');
    }

    public function sanpham(){
    	return $this->belongsTo('App\SanPham', 'san_pham_id', 'id');
    }
}

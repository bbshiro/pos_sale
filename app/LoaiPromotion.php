<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiPromotion extends Model
{
    protected $table = 'loai_promotion';
    public $timestamps = false;

    public function promotion(){
    	return $this->hasMany('App\Promotion', 'ma_loai_promotion', 'loai_promotion');
    }
}

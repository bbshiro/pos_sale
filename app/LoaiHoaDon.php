<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiHoaDon extends Model
{
    protected $table = 'loai_hoa_don';
    public $timestamps = false;

    public function hoadon(){
    	return $this->hasMany('App\HoaDon', 'ma_loai_hoa_don', 'loai_hoa_don');
    }
}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChiTietHoaDon extends Model
{
    protected $table = 'chi_tiet_hoa_don';
    public $timestamps = false;

    public function hoadon(){
   		return $this->belongsTo('App\HoaDon', 'ma_hoa_don', 'ma_hoa_don');
   	}

   	public function sanpham(){
   		return $this->belongsTo('App\SanPham', 'id_san_pham', 'id');
   	}

   	public function tuychon(){
    	return $this->hasMany('App\TuyChon', 'id', 'id_chi_tiet_hoa_don');
    }

    public function topping(){
    	return $this->belongsToMany("App\Topping", "tuy_chon", "id_chi_tiet_hoa_don", "id_topping");
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HoaDon extends Model
{
    protected $table = 'hoa_don';
    public $timestamps = false;

    public function loaihoadon(){
   		return $this->belongsTo('App\LoaiHoaDon', 'loai_hoa_don', 'ma_loai_hoa_don');
   	}

   	public function chitiethoadon(){
    	return $this->hasMany('App\ChiTietHoaDon', 'ma_hoa_don', 'ma_hoa_don');
    }

    public function promotion(){
   		return $this->belongsTo('App\Promotion', 'promotion_id', 'id');
   	}

   	public function user(){
   		return $this->belongsTo('App\User', 'user_id', 'id');
   	}
}

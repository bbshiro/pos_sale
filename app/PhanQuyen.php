<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhanQuyen extends Model
{
    protected $table = 'phanquyen';
    public $timestamps = false;

    public function users(){
		return $this->hasMany('App\User', 'ma_phan_quyen', 'ma_phan_quyen');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topping extends Model
{
    protected $table = 'topping';
    public $timestamps = false;

    public $primary = "id";
    public function loaitopping(){
    	return $this->belongsTo('App\LoaiTopping', 'loai_topping', 'ma_loai_topping');
    }

    public function tuychon(){
    	return $this->hasMany('App\TuyChon', 'id', 'id_topping');
    }
}

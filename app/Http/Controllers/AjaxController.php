<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\LoaiSanPham;
use App\LoaiTopping;
use App\SanPham;
use App\Topping;
use App\PhanQuyen;
use App\loaiPromotion;
use App\Promotion;
use App\User;
use App\ChiTietPromotion;
use App\HoaDon;
use App\LoaiHoaDon;
use App\ChiTietHoaDon;
use App\TuyChon;
use Hash;
use Yajra\Datatables\Datatables;
use DB;
use App\ThongTin;
class AjaxController extends Controller
{

    public function addLoaiSanPham(Request $request){

        $validation = Validator::make($request->all(), [
            'ma_loaisp' => 'required|min:3|max:3',
            'name_loaisp' => 'required',
        ],
        ['ma_loaisp.required' => 'Bạn chưa nhập Mã loại sản phẩm',
         'ma_loaisp.min' => 'Mã loại sản phẩm gồm 3 ký tự' ,
         'ma_loaisp.max' => 'Mã loại sản phẩm gồm 3 ký tự',
         'name_loaisp.required' => 'Bạn chưa nhập tên loại sản phẩm' 
        ]);

        $error_array = array();
        $success_output = '';
        if($validation->fails()){
            foreach($validation->messages()->getMessages() as $field_name => $messages){
                $error_array[] = $messages; 
            }
        }
        else
        {
            if($request->button_action == "insert"){
                $loai_sp = new LoaiSanPham();
                $loai_sp->ma_loai_san_pham = strtoupper($request->ma_loaisp);
                $loai_sp->ten_loai_san_pham = $request->name_loaisp;
                $loai_sp->save();
                $success_output = '<div class="alert alert-success"> Data Inserted </div>';
            }

            if($request->button_action == "update"){
                LoaiSanPham::where('ma_loai_san_pham', $request->ma_loaisp)->update(['ten_loai_san_pham' => $request->name_loaisp]);
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removeLoaiSanPham(Request $request){
        $loai_sp = LoaiSanPham::where('ma_loai_san_pham', $request->input('id'));
        if($loai_sp->delete()){
            echo "Đã xóa";
        }
    }

    public function getLoaiSanPham(Request $request){
        $ma_loai_sp = $request->input('id');
        $loai_sp = LoaiSanPham::where('ma_loai_san_pham', $ma_loai_sp)->get();
        $output = array('ma_loai_sp' => $loai_sp[0]->ma_loai_san_pham, 
                        'name_loai_sp' => $loai_sp[0]->ten_loai_san_pham);
        echo json_encode($output);
    }


    public function addLoaiTopping(Request $request){

        $validation = Validator::make($request->all(), [
            'ma_loai_topping' => 'required|min:3|max:3',
            'name_loai_topping' => 'required',
        ],
        ['ma_loai_topping.required' => 'Bạn chưa nhập Mã loại topping',
         'ma_loai_topping.min' => 'Mã loại topping gồm 3 ký tự' ,
         'ma_loai_topping.max' => 'Mã loại topping gồm 3 ký tự',
         'name_loai_topping.required' => 'Bạn chưa nhập tên loại topping' 
        ]);

        $error_array = array();
        $success_output = '';
        if($validation->fails()){
            foreach($validation->messages()->getMessages() as $field_name => $messages){
                $error_array[] = $messages; 
            }
        }
        else
        {
            if($request->button_action == "insert"){
                $loai_topping = new LoaiTopping();
                $loai_topping->ma_loai_topping = strtoupper($request->ma_loai_topping);
                $loai_topping->ten_loai_topping = $request->name_loai_topping;
                $loai_topping->save();
                $success_output = '<div class="alert alert-success"> Data Inserted </div>';
            }

            if($request->button_action == "update"){
                LoaiTopping::where('ma_loai_topping', $request->ma_loai_topping)->update(['ten_loai_topping' => $request->name_loai_topping]);
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removeLoaiTopping(Request $request){
        $loai_topping = LoaiTopping::where('ma_loai_topping', $request->input('id'));
        if($loai_topping->delete()){
            echo "Đã xóa";
        }
    }

    public function getLoaiTopping(Request $request){
        $ma_loai_topping = $request->input('id');
        $loai_topping = LoaiTopping::where('ma_loai_topping', $ma_loai_topping)->get();
        $output = array('ma_loai_topping' => $loai_topping[0]->ma_loai_topping, 
                        'name_loai_topping' => $loai_topping[0]->ten_loai_topping);
        echo json_encode($output);
    }

    // Sản phẩm
    public function addSanPham(Request $request){

        $validation = Validator::make($request->all(), [
            'name_sp' => 'required',
            'price_sp' => 'required',
        ],
        ['name_sp.required' => 'Bạn chưa nhập tên sản phẩm',
         'price_sp.required' => 'Bạn chưa nhập giá sản phẩm']);

        $error_array = array();
        $success_output = '';
        if($validation->fails()){
            foreach($validation->messages()->getMessages() as $field_name => $messages){
                $error_array[] = $messages; 
            }
        }
        else
        {
            if($request->button_action == "insert"){
                $san_pham = new SanPham();
                $san_pham->ten_san_pham = $request->name_sp;
                $san_pham->loai_san_pham = $request->loai_sp;
                $san_pham->price = $request->price_sp;
                $san_pham->ghi_chu = $request->ghi_chu;
                $san_pham->save();
                $success_output = '<div class="alert alert-success"> Data Inserted </div>';
            }

            if($request->button_action == "update"){
                SanPham::where('id', $request->sp_id)->update(['ten_san_pham' => $request->name_sp, 'loai_san_pham' => $request->loai_sp, 
                    'price' => $request->price_sp, 'ghi_chu' => $request->ghi_chu]);
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removeSanPham(Request $request){
        $sp = SanPham::where('id', $request->input('id'));
        if($sp->delete()){
            echo "Đã xóa";
        }
    }

    public function getSanPham(Request $request){
        $id = $request->input('id');
        $sp = SanPham::where('id', $id)->get();
        $output = array('ten_san_pham' => $sp[0]->ten_san_pham, 
                        'loai_san_pham' => $sp[0]->loai_san_pham,
                        'price' => $sp[0]->price,
                        'ghi_chu' => $sp[0]->ghi_chu);
        echo json_encode($output);
    }

    // Topping
    public function addTopping(Request $request){

        $validation = Validator::make($request->all(), [
            'name_topping' => 'required',
            'price_topping' => 'required',
        ]);

        $error_array = array();
        $success_output = '';
        if($validation->fails()){
            foreach($validation->messages()->getMessages() as $field_name => $messages){
                $error_array[] = $messages; 
            }
        }
        else
        {
            if($request->button_action == "insert"){
                $topping = new Topping();
                $topping->ten_topping = $request->name_topping;
                $topping->loai_topping = $request->loai_topping;
                $topping->price = $request->price_topping;
                $topping->ghi_chu = $request->ghi_chu;
                $topping->save();
                $success_output = '<div class="alert alert-success"> Data Inserted </div>';
            }

            if($request->button_action == "update"){
                Topping::where('id', $request->topping_id)->update(['ten_topping' => $request->name_topping, 'loai_topping' => $request->loai_topping, 
                    'price' => $request->price_topping, 'ghi_chu' => $request->ghi_chu]);
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removeTopping(Request $request){
        $topping = Topping::where('id', $request->input('id'));
        if($topping->delete()){
            echo "Đã xóa";
        }
    }

    public function getTopping(Request $request){
        $id = $request->input('id');
        $topping = Topping::where('id', $id)->get();
        $output = array('ten_topping' => $topping[0]->ten_topping, 
                        'loai_topping' => $topping[0]->loai_topping,
                        'price' => $topping[0]->price,
                        'ghi_chu' => $topping[0]->ghi_chu);
        echo json_encode($output);
    }

    // Phân Quyền
    public function addPhanQuyen(Request $request){

        $validation = Validator::make($request->all(), [
            'ma_pq' => 'required|min:3|max:3',
            'name_pq' => 'required',
        ],
        ['ma_pq.required' => 'Bạn chưa nhập Mã phân quyền',
         'ma_pq.min' => 'Mã phân quyền gồm 3 ký tự' ,
         'ma_pq.max' => 'Mã phân quyền gồm 3 ký tự',
         'name_pq.required' => 'Bạn chưa nhập tên phân quyền' 
        ]);

        $error_array = array();
        $success_output = '';
        if($validation->fails()){
            foreach($validation->messages()->getMessages() as $field_name => $messages){
                $error_array[] = $messages; 
            }
        }
        else
        {
            if($request->button_action == "insert"){
                $phan_quyen = new PhanQuyen();
                $phan_quyen->ma_phan_quyen = strtoupper($request->ma_pq);
                $phan_quyen->ten_quyen = $request->name_pq;
                $phan_quyen->save();
                $success_output = '<div class="alert alert-success"> Data Inserted </div>';
            }

            if($request->button_action == "update"){
                PhanQuyen::where('ma_phan_quyen', $request->ma_pq)->update(['ten_quyen' => $request->name_pq]);
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removePhanQuyen(Request $request){
        $pq = PhanQuyen::where('ma_phan_quyen', $request->input('id'));
        if($pq->delete()){
            echo "Đã xóa";
        }
    }

    public function getPhanQuyen(Request $request){
        $id = $request->input('id');
        $pq = PhanQuyen::where('ma_phan_quyen', $id)->get();
        $output = array('ma_phan_quyen' => $pq[0]->ma_phan_quyen, 
                        'ten_quyen' => $pq[0]->ten_quyen);
        echo json_encode($output);
    }

    // Loại Promotion
    public function addLoaiPromotion(Request $request){

        $validation = Validator::make($request->all(), [
            'ma_loai_promotion' => 'required|min:3|max:3',
            'name_loai_promotion' => 'required',
        ],
        ['ma_loai_promotion.required' => 'Bạn chưa nhập Mã loại promotion',
         'ma_loai_promotion.min' => 'Mã loại promotion gồm 3 ký tự' ,
         'ma_loai_promotion.max' => 'Mã loại promotion gồm 3 ký tự',
         'name_loai_promotion.required' => 'Bạn chưa nhập tên loại promotion' 
        ]);

        $error_array = array();
        $success_output = '';
        if($validation->fails()){
            foreach($validation->messages()->getMessages() as $field_name => $messages){
                $error_array[] = $messages; 
            }
        }
        else
        {
            if($request->button_action == "insert"){
                $loai_pr = new loaiPromotion();
                $loai_pr->ma_loai_promotion = strtoupper($request->ma_loai_promotion);
                $loai_pr->ten_loai_promotion = $request->name_loai_promotion;
                $loai_pr->save();
                $success_output = '<div class="alert alert-success"> Data Inserted </div>';
            }

            if($request->button_action == "update"){
                LoaiPromotion::where('ma_loai_promotion', $request->ma_loai_promotion)->update(['ten_loai_promotion' => $request->name_loai_promotion]);
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removeLoaiPromotion(Request $request){
        $pq = LoaiPromotion::where('ma_loai_promotion', $request->input('id'));
        if($pq->delete()){
            echo "Đã xóa";
        }
    }

    public function getLoaiPromotion(Request $request){
        $id = $request->input('id');
        $pq = LoaiPromotion::where('ma_loai_promotion', $id)->get();
        $output = array('ma_loai_promotion' => $pq[0]->ma_loai_promotion, 
                        'ten_loai_promotion' => $pq[0]->ten_loai_promotion);
        echo json_encode($output);
    }

    // Nhân viên
    public function addUser(Request $request){

        $validation = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required|min:6|max:32',
            'email'    => 'required',
            'sdt'      => 'required',
            'cmnd'     => 'required',
            'phan_quyen' => 'required',
            'dia_chi'   => 'required'
        ],[
            'username.required' =>  'Vui lòng điền username',
            'password.required' => 'Vui lòng điền password',
            'password.min' => 'Mật khẩu phải có ít nhất 6 ký tự',
            'password.max' => 'Mật khẩu nhiều nhất có 32 ký tự',
            'email.required' => 'Vui lòng điền email',
            'sdt.required' => 'Vui lòng nhập số điện thoại',
            'cmnd.required' => 'Vui lòng nhập số Chứng minh nhân dân',
            'phan_quyen.required' => 'Vui lòng nhập mã phân quyền',
            'dia_chi.required' => 'Vui lòng nhập địa chỉ'
        ]);

        $error_array = array();
        $success_output = '';
        if($validation->fails()){
            foreach($validation->messages()->getMessages() as $field_name => $messages){
                $error_array[] = $messages; 
            }
        }
        else
        {
            if($request->button_action == "insert"){
                $user = new User();
                $user->name = $request->username;
                $user->password = Hash::make($request->password);
                $user->email = $request->email;
                $user->ma_phan_quyen = $request->phan_quyen;
                $user->sdt = $request->sdt;
                $user->cmnd = $request->cmnd;
                $user->dia_chi = $request->dia_chi;
                $user->save();
                $success_output = '<div class="alert alert-success"> Data Inserted </div>';
            }

            if($request->button_action == "update"){
                User::where('id', $request->user_id)->update(['name' => $request->username, 'password' =>  Hash::make($request->password), 'email' => $request->email, 'ma_phan_quyen' => $request->phan_quyen, 'sdt' => $request->sdt,
                    'cmnd' => $request->cmnd, 'dia_chi' => $request->dia_chi]);
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removeUser(Request $request){
        $user = User::where('id', $request->input('id'));
        if($user->delete()){
            echo "Đã xóa";
        }
    }

    public function getUser(Request $request){
        $id = $request->input('id');
        $user = User::where('id', $id)->get();
        $output = array('username' => $user[0]->name, 
                        'password' => $user[0]->password,
                        'email' => $user[0]->email,
                        'sdt' => $user[0]->sdt,
                        'cmnd' => $user[0]->cmnd,
                        'phan_quyen' => $user[0]->ma_phan_quyen,
                        'dia_chi' => $user[0]->dia_chi);
        echo json_encode($output);
    }

    // Promotion
    public function addPromotion(Request $request){

        $validation = Validator::make($request->all(), [
            'ten_promotion' => 'required',
            'percent_discount' => 'required|integer|max:80|min:0',
            'time_start'    => 'required|date',
            'time_end'      => 'required|date',
            'loai_promotion'     => 'required'
        ],
        ['ten_promotion.required' => 'Vui lòng nhập tên promotion',
         'percent_discount.required' => 'Vui lòng nhập phần trăm discount',
         'percent_discount.integer' => 'Phần trăm discount là số nguyên',
         'percent_discount.max' => 'Phần trăm discount không vượt quá 80%',
         'percent_discount.min' => 'Phần trăm discount không thấp hơn 0',
         'time_start.required' => 'Vui lòng nhập ngày bắt đầu',
         'time_start.date' => 'Sai định dạng ngày bắt đầu',
         'time_end.required' => 'Vui lòng nhập ngày kết thúc',
         'time_end.date' => 'Sai định dạng ngày kết thúc',
         'loai_promotion.required' => 'Vui lòng nhập loại promotion']);

        $error_array = array();
        $success_output = '';
        if($validation->fails()){
            foreach($validation->messages()->getMessages() as $field_name => $messages){
                $error_array[] = $messages; 
            }
        }
        else
        {
            if($request->button_action == "insert"){
                $promotion = new Promotion();
                $promotion->ten_promotion = $request->ten_promotion;
                $promotion->time_start = $request->time_start;
                $promotion->time_end = $request->time_end;
                $promotion->percent_discount = $request->percent_discount;
                $promotion->loai_promotion = $request->loai_promotion;
                $promotion->save();
                $success_output = '<div class="alert alert-success"> Data Inserted </div>';
            }

            if($request->button_action == "update"){
                Promotion::where('id', $request->promotion_id)->update(['ten_promotion' => $request->ten_promotion, 'time_start' => $request->time_start, 'time_end' => $request->time_end, 'percent_discount' => $request->percent_discount, 'loai_promotion' => $request->loai_promotion]);
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removePromotion(Request $request){
        $promotion = Promotion::where('id', $request->input('id'));
        if($promotion->delete()){
            echo "Đã xóa";
        }
    }

    public function getPromotion(Request $request){
        $id = $request->input('id');
        $promotion = Promotion::where('id', $id)->get();
        $output = array('ten_promotion' => $promotion[0]->ten_promotion, 
                        'percent_discount' => $promotion[0]->percent_discount,
                        'time_start' => $promotion[0]->time_start,
                        'time_end' => $promotion[0]->time_end,
                        'loai_promotion' => $promotion[0]->loai_promotion
                        );
        echo json_encode($output);
    }


    // Promotion Detail
    public function addPromotionDetail(Request $request){
        $error_array = array();
        $success_output = '';

        if(isset($request->san_pham_id)){
            foreach ($request->san_pham_id as $id) {
                $promotion_detail = new ChiTietPromotion();
                $promotion_detail->promotion_id = $request->promotion_id;
                $promotion_detail->san_pham_id = $id;
                $promotion_detail->save();
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        } else{
            $error_array[] = 'Chọn ít nhất một sản phẩm giảm giá!';
        }

        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removePromotionDetail(Request $request){
        $promotion_id = $request->promotion_id;
        $san_pham_id = $request->san_pham_id;
        $promotion_detail = ChiTietPromotion::where('promotion_id', $promotion_id)->where('san_pham_id', $san_pham_id);
        if($promotion_detail->delete()){
            echo "Đã xóa";
        }
    }

    // Loại Hóa Đơn
    public function addLoaiHoaDon(Request $request){

        $validation = Validator::make($request->all(), [
            'ma_loai_hoa_don' => 'required|min:3|max:3',
            'name_loai_hoa_don' => 'required',
        ],
        ['ma_loai_hoa_don.required' => 'Bạn chưa nhập Mã loại hóa đơn',
         'ma_loai_hoa_don.min' => 'Mã loại hóa đơn gồm 3 ký tự' ,
         'ma_loai_hoa_don.max' => 'Mã loại hóa đơn gồm 3 ký tự',
         'name_loai_hoa_don.required' => 'Bạn chưa nhập tên loại hóa đơn' 
        ]);

        $error_array = array();
        $success_output = '';
        if($validation->fails()){
            foreach($validation->messages()->getMessages() as $field_name => $messages){
                $error_array[] = $messages; 
            }
        }
        else
        {
            if($request->button_action == "insert"){
                $loai_hd = new LoaiHoaDon();
                $loai_hd->ma_loai_hoa_don = strtoupper($request->ma_loai_hoa_don);
                $loai_hd->ten_loai_hoa_don = $request->name_loai_hoa_don;
                $loai_hd->save();
                $success_output = '<div class="alert alert-success"> Data Inserted </div>';
            }

            if($request->button_action == "update"){
                LoaiHoaDon::where('ma_loai_hoa_don', $request->ma_loai_hoa_don)->update(['ten_loai_hoa_don' => $request->name_loai_hoa_don]);
                $success_output = '<div class="alert alert-success"> Data Updated </div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function removeLoaiHoaDon(Request $request){
        $pq = LoaiHoaDon::where('ma_loai_hoa_don', $request->input('id'));
        if($pq->delete()){
            echo "Đã xóa";
        }
    }

    public function getLoaiHoaDon(Request $request){
        $id = $request->input('id');
        $lhd = LoaiHoaDon::where('ma_loai_hoa_don', $id)->get();
        $output = array('ma_loai_hoa_don' => $lhd[0]->ma_loai_hoa_don, 
                        'ten_loai_hoa_don' => $lhd[0]->ten_loai_hoa_don);
        echo json_encode($output);
    }

    public function tongTienTheoGio(Request $request){
        $List = DB::table('hoa_don')->select(Db::raw('YEAR(created_at) as Year, MONTH(created_at) as Month, DAY(created_at) as Day, HOUR(created_at) as Hour, sum(tong_tien) as tong_tien_hd'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('Year', 'Month', 'Day', 'Hour')->orderByRaw(DB::raw('Year, Month, Day, Hour'))->get();
        return json_encode($List);
    }

    public function trungBinhTheoGio(Request $request){
        $List = DB::table('hoa_don')->select(Db::raw('YEAR(created_at) as Year, MONTH(created_at) as Month, DAY(created_at) as Day, HOUR(created_at) as Hour, avg(tong_tien) as trung_binh'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('Year', 'Month', 'Day', 'Hour')->orderByRaw(DB::raw('Year, Month, Day, Hour'))->get();
        return json_encode($List);
    }

    public function tongTienTheoNgay(Request $request){
        $List = DB::table('hoa_don')->select(Db::raw('YEAR(created_at) as Year, MONTH(created_at) as Month, DAY(created_at) as Day, sum(tong_tien) as tong_tien_hd'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('Year', 'Month', 'Day')->orderByRaw(DB::raw('Year, Month, Day'))->get();
        return json_encode($List);
    }

    public function trungBinhTheoNgay(Request $request){
        $List = DB::table('hoa_don')->select(Db::raw('YEAR(created_at) as Year, MONTH(created_at) as Month, DAY(created_at) as Day, avg(tong_tien) as trung_binh'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('Year', 'Month', 'Day')->orderByRaw(DB::raw('Year, Month, Day'))->get();
        return json_encode($List);
    }

    public function tongTienSanPham(Request $request){
        $List = DB::table('hoa_don')->join('chi_tiet_hoa_don', 'hoa_don.ma_hoa_don', '=', 'chi_tiet_hoa_don.ma_hoa_don')->select(Db::raw('id_san_pham, sum( chi_tiet_hoa_don.tong_tien) as tong_tien_sp'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('chi_tiet_hoa_don.id_san_pham')->get();
        
        $Result = array();
        foreach ($List as $value) {
            $element = ['tong_tien_sp' => $value->tong_tien_sp, 'ten_san_pham' => $this->getTenSp($value->id_san_pham)];
            array_push($Result, $element);
        }
        return json_encode($Result);
    }

    public function soLuongSanPham(Request $request){
        $List = DB::table('hoa_don')->join('chi_tiet_hoa_don', 'hoa_don.ma_hoa_don', '=', 'chi_tiet_hoa_don.ma_hoa_don')->select(Db::raw('id_san_pham, sum(so_luong) as so_luong_sp'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('chi_tiet_hoa_don.id_san_pham')->get();
        $Result = array();
        foreach ($List as $value) {
            $element = ['so_luong_sp' => $value->so_luong_sp, 'ten_san_pham' => $this->getTenSp($value->id_san_pham)]; 
            array_push($Result, $element);
        }
        return json_encode($Result);
    }

     public function getTenSp($id){
        $TenSp = SanPham::where('id', $id)->get();
        return $TenSp[0]->ten_san_pham;
    }


    public function upload(Request $request){
        $validation = Validator::make($request->all(), [
            'shop_logo' => 'image|mimes:jpeg,png,jpg,gif',
            'shop_name' => 'required|max:150',
            'shop_address' => 'required|max:500',
            'shop_number' => 'required|max:10',
            'shop_password' => 'required|min:8|max:30'
        ],
        [   'shop_logo.image' => 'Vui lòng chọn file ảnh làm logo',
            'shop_logo.mimes' => 'Logo của shop phải là file loại: jpeg, png, jpg, gif',
            'shop_name.required' => 'Vui lòng nhập tên cửa hàng',
            'shop_name.max' => 'Tên cửa hàng không quá 150 ký tự',
            'shop_address.required' => 'Vui lòng nhập địa chỉ cửa hàng',
            'shop_address.max' => "Địa chỉ cửa hàng không quá 500 ký tự",
            'shop_number.required' => "Vui lòng nhập số điện thoại cửa hàng",
            'shop_number.max' => "Số điện thoại không được quá 10 ký tự",
            'shop_password.required' => "Vui lòng nhập mật khẩu wifi",
            'shop_password.min' => "Mật khẩu wifi cần lớn hơn 8 ký tự",
            'shop_password.max' => "Mật khẩu wifi cần nhỏ hơn 30 ký tự", 
        ]);

        if($validation->passes()){
            if ($request->hasFile('shop_logo')){
                $image = $request->file('shop_logo');
                $new_name = rand().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath, $new_name);
                $image->imagePath = $destinationPath . $new_name;
                ThongTin::find(1)->update(['name' => $request->shop_name, 'phone_number' => $request->shop_number, 'address' => $request->shop_address, 'pass_wifi' => $request->shop_password, 'logo' => $new_name]);
            }
            else 
            {
                ThongTin::find(1)->update(['name' => $request->shop_name, 'phone_number' => $request->shop_number, 'address' => $request->shop_address, 'pass_wifi' => $request->shop_password]);
            }
            
            return response()->json([
                'message' => 'Upload thành công!!',
                'uploaded_image' => $new_name,
                'class_name' => 'alert-success',
                'errors' => [],
                'hasFile' => $request->hasFile('shop_logo')
            ]);
        }
        else
        {
            return response()->json([
                'errors' => $validation->errors()->all(),
                'message' => "",
                'uploaded_image' => '',
                'class_name' => 'alert-danger'
            ]);
        }
    }

    public function printBill(Request $request){
        $id = $request->id;
        $url = route('getInvoice', ['ma_hoa_don' => $id]);
        return json_encode(['url' => $url]);
    }
}

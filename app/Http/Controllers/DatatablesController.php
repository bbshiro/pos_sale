<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoaiSanPham;
use App\LoaiTopping;
use App\Topping;
use App\SanPham;
use App\PhanQuyen;
use App\LoaiPromotion;
use App\User;
use App\Promotion;
use App\ChiTietPromotion;
use App\LoaiHoaDon;
use App\HoaDon;
use App\ChiTietHoaDon;
use App\TuyChon;
use Yajra\Datatables\Datatables;
use DB;

class DatatablesController extends Controller
{
    public function getIndexLoaiSanPham()
    {
        return view('pages.thuc_don.loai_san_pham');
    }

    public function loaiSanPhamData()
    {   
        
        $List = LoaiSanPham::all();
        return Datatables::of($List)
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->ma_loai_san_pham.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" id="'.$List->ma_loai_san_pham.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i> Delete</a>';
        })->rawColumns(['action'])->make(true);
    }

    // Topping
    public function getIndexLoaiTopping(){
        return view('pages.thuc_don.loai_topping');
    }

     public function loaiToppingData()
    {
        $List = LoaiTopping::all();
        return Datatables::of($List)
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->ma_loai_topping.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" id="'.$List->ma_loai_topping.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i> Delete</a>';
        })->rawColumns(['action'])->make(true);
    }

    // Sản phẩm
    public function getIndexSanPham()
    {   
        $ListLoaiSanPham = LoaiSanPham::all();
        return view('pages.thuc_don.san_pham', ['ListLoaiSanPham' => $ListLoaiSanPham]);
    }

    public function sanPhamData(Request $request)
    {   
        $List = new SanPham;
        $List = $List->with('loaisanpham');
        if(isset($request->filter_loai_sp)){
            $List = $List->where('loai_san_pham', $request->filter_loai_sp);
        }
        if(isset($request->filter_price)){
            $List = $List->where('price', '>=', $request->filter_price);
        }
        $List = $List->get();
        
        return Datatables::of($List)
            ->editColumn('loai_san_pham', function($List){
                return $List->loaisanpham->ten_loai_san_pham;
            })
            ->editColumn('price', function($List){
                return number_format($List->price)." VND";
            })
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->id.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" id="'.$List->id.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i> Delete</a>';
        })->rawColumns(['loai_san_pham', 'action'])->make(true);
    }


    // Topping
    public function getIndexTopping()
    {   
        $ListLoaiTopping = LoaiTopping::all();
        return view('pages.thuc_don.topping', ['ListLoaiTopping' => $ListLoaiTopping]);
    }

    public function toppingData(Request $request)
    {   
        $List = new Topping;
        $List = $List->with('loaitopping');
        if(isset($request->filter_loai_tp)){
            $List = $List->where('loai_topping', $request->filter_loai_tp);
        }
        if(isset($request->filter_price)){
            $List = $List->where('price', '>=', $request->filter_price);
        }
        $List = $List->get();
        return Datatables::of($List)
            ->editColumn('loai_topping', function($List){
                return $List->loaitopping->ten_loai_topping;
            })
            ->editColumn('price', function($List){
                return number_format($List->price)." VND";
            })
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->id.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" id="'.$List->id.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i> Delete</a>';
        })->rawColumns(['loai_san_pham', 'action'])->make(true);
    }

    // Phân quyền
    public function getIndexPhanQuyen(){
        return view('pages.nhan_vien.phan_quyen');
    }

    public function phanQuyenData()
    {
        $List = PhanQuyen::all();
        return Datatables::of($List)
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->ma_phan_quyen.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" id="'.$List->ma_phan_quyen.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i> Delete</a>';
        })->rawColumns(['loai_san_pham', 'action'])->make(true);
    }

    // User
    public function getIndexUser(){
        $List = PhanQuyen::all();
        return view('pages.nhan_vien.user', ['phanquyen' => $List]);
    }

    public function userData(Request $request)
    {   
        $List = new User;
        $List = $List->with('phanquyen');
        if(isset($request->filter_phan_quyen)){
            $List = $List->where('ma_phan_quyen', $request->filter_phan_quyen);
        }
        $List = $List->get();
        // $List = User::with('phanquyen')->get();
        return Datatables::of($List)
            ->editColumn('phan_quyen', function($List){
                    return $List->phanquyen->ten_quyen;
                })
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->id.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" id="'.$List->id.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i> Delete</a>';
        })->rawColumns(['phan_quyen', 'action'])->make(true);
    }

    // Loại Promotion
    public function getIndexLoaiPromotion(){
        return view('pages.marketing.loai_promotion');
    }

    public function loaiPromotionData()
    {
        $List = LoaiPromotion::all();
        return Datatables::of($List)
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->ma_loai_promotion.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" id="'.$List->ma_loai_promotion.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i> Delete</a>';
        })->rawColumns(['action'])->make(true);
    }

    // Promotion
    public function getIndexPromotion(){
        $ListLoaiPromotion = LoaiPromotion::all();
        $ListAvailablePromotion = Promotion::where('time_end', '>', date("Y-m-d"))->get();
        $ListSanPham = SanPham::all();
        return view('pages.marketing.promotion', ['ListLoaiPromotion' => $ListLoaiPromotion, 'ListAvailablePromotion' => $ListAvailablePromotion,
        'ListSanPham' => $ListSanPham]);
    }

    public function promotionData(Request $request)
    {   
        $List = new Promotion;
        $List = $List->with('loaipromotion');
        if(isset($request->filter_loai_pr)){
            $List = $List->where('loai_promotion', $request->filter_loai_pr);
        }
        if(isset($request->filter_start_date)){
            $List = $List->where('time_start', '>=', $request->filter_start_date);
        }
        if(isset($request->filter_end_date)){
            $List = $List->where('time_end', '<=', $request->filter_end_date);
        }
        $List = $List->get();
        return Datatables::of($List)
            ->editColumn('percent_discount', function($List){
                return $List->percent_discount." %";
            })
            ->editColumn('loai_promotion', function($List){
                    return $List->loaipromotion->ten_loai_promotion;
                })
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->id.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" id="'.$List->id.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i> Delete</a>';
        })->rawColumns(['loai_promotion', 'action'])->make(true);
    }

    // Promotion Detail
    public function getIndexPromotionDetail(){
      
        return view('pages.marketing.promotion_detail');
    }

    public function promotionDetailData()
    {
        $List = ChiTietPromotion::with(['sanpham', 'promotion'])->get();
        return Datatables::of($List)
            ->editColumn('ten_san_pham', function($List){
                    return $List->sanpham->ten_san_pham;
                })
            ->editColumn('ten_promotion', function($List){
                    return $List->promotion->ten_promotion;
                })
            ->addColumn('action', function ($List) {
        return 
            // '<a href="javascript:void(0)" id="'.$List->id.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            '<a href="javascript:void(0)" id="'.$List->promotion_id.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i><input type="hidden" value="'.$List->sanpham->id.'"> Delete</a>';
        })->rawColumns(['ten_san_pham', 'ten_promotion', 'action'])->make(true);
    }

    // Loại Hóa Đơn
    public function getIndexLoaiHoaDon(){
        return view('pages.hoa_don.loai_hoa_don');
    }

    public function loaiHoaDonData()
    {
        $List = LoaiHoaDon::all();
        return Datatables::of($List)
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->ma_loai_hoa_don.'" class="btn btn-xs btn-warning edit" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Edit</a>

            <a href="javascript:void(0)" id="'.$List->ma_loai_hoa_don.'" class="btn btn-xs btn-danger btn-delete delete"><i class="fas fa-times"></i> Delete</a>';
        })->rawColumns(['action'])->make(true);
    }

    // Hóa Đơn
    public function getIndexHoaDon(){
        $ListLoaiHoaDon = LoaiHoaDon::all();
        $ListUser = User::all();
        return view('pages.hoa_don.hoa_don', 
                    ['ListLoaiHoaDon' => $ListLoaiHoaDon,
                     'ListUser' => $ListUser]);
    }

    public function hoaDonData(Request $request)
    {   
        $List = new HoaDon;
        $List = $List->with(['loaihoadon', 'user', 'promotion']);
        if(isset($request->filter_loai_hd)){
            $List = $List->where('loai_hoa_don', $request->filter_loai_hd);
        }
        if(isset($request->filter_user)){
            $List = $List->where('user_id', $request->filter_user);
        }
        if(isset($request->filter_start_date)){
            $List = $List->where('created_at', '>=',$request->filter_start_date);
        }
        if(isset($request->filter_end_date)){
            $List = $List->where('created_at', '<=',$request->filter_end_date);
        }
        $List =$List->get();

        return Datatables::of($List)
            ->editColumn('loai_hoa_don', function($List){
                    return $List->loaihoadon->ten_loai_hoa_don;
                })
            ->editColumn('user_id', function($List){
                    return $List->user->name;
                })
            ->editColumn('promotion_id', function($List){
                    if(isset($List->promotion->ten_promotion)){
                        return $List->promotion->ten_promotion;
                    }
                    return "Không có khuyến mại";
                })
            ->editColumn('tong_tien', function($List){
                return number_format($List->tong_tien, 2)." VND";
            })
            ->editColumn('tong_tien_discount', function($List){
                return number_format($List->tong_tien_discount, 2)." VND";
            })
            ->editColumn('khach_tra', function($List){
                return number_format($List->khach_tra, 2)." VND";
            })
            ->editColumn('tien_thua', function($List){
                return number_format($List->tien_thua, 2)." VND";
            })
            ->addColumn('action', function ($List) {
        return 
            '<a href="javascript:void(0)" id="'.$List->ma_hoa_don.'" class="btn btn-xs btn-warning detail" data-toggle="modal" data-target="#add_modal"><i class="fas fa-edit"></i> Detail</a>';
        })->rawColumns(['loai_hoa_don', 'user_id', 'promotion_id','action'])->make(true);
    }

    public function hoaDonDataDetail(Request $request){
        $List = ChiTietHoaDon::with('sanpham')->where('ma_hoa_don', $request->hoa_don_id)->get();
        return Datatables::of($List)
            ->editColumn('id_san_pham', function($List){
                    return $this->make_san_pham_col($List->sanpham->ten_san_pham, $List->id);
                })
            ->editColumn('tong_tien', function($List){
                return number_format($List->tong_tien, 2)." VND";
            })
            ->editColumn('tong_tien_discount', function($List){
                return number_format($List->tong_tien_discount, 2)." VND";
            })
            ->rawColumns(['id_san_pham'])->make(true);
    }

    public function make_san_pham_col($ten_san_pham, $id_chi_tiet_hoa_don){
        $div = '<span>'.$ten_san_pham.'</span><br>';
        $List = TuyChon::with('topping')->where('id_chi_tiet_hoa_don', $id_chi_tiet_hoa_don)->get();
        foreach ($List as $ele) {
            if(isset($ele->percent)){
                $div = $div.'<span class="topping-text">- '.$ele->topping->ten_topping.' ('.$ele->percent.')</span><br>';
            }
            else
                $div = $div.'<span class="topping-text">- '.$ele->topping->ten_topping.'</span><br>';
        }
        return $div;
    }

    public function getIndexDoanhThu(){
        return view('pages.bao_cao.doanh_thu');
    }

    public function doanhThuData()
    {
        $List = DB::table('hoa_don')->select(Db::raw('user_id, count(ma_hoa_don) as so_hoa_don, sum(tong_tien) as tong_tien_hd, sum(tong_tien_discount) as tong_tien_discount_hd, avg(tong_tien) as trung_binh'))->whereRaw('DATE(created_at) = CURRENT_DATE()')->groupBy('user_id')->get();
        return Datatables::of($List)
        ->editColumn('user_id', function($List){
            return $this->getUserName($List->user_id);
        })
        ->editColumn('tong_tien_hd', function($List){
            return number_format($List->tong_tien_hd, 2)." VND";
        })
        ->editColumn('tong_tien_discount_hd', function($List){
            return number_format($List->tong_tien_discount_hd, 2)." VND";
        })
        ->editColumn('trung_binh', function($List){
            return number_format($List->trung_binh, 2)." VND";
        })
        ->rawColumns(['user_id'])->make(true);
    }
    
    public function getUserName($Id){
        $Name = User::find($Id);
        return $Name->name;
    }

    public function doanhThuNhanVien(Request $request){
        $List = DB::table('hoa_don')->join('users', 'hoa_don.user_id', '=', 'users.id')->select(Db::raw('user_id, count(hoa_don.ma_hoa_don) as so_hoa_don, sum(hoa_don.tong_tien) as tong_tien_hd, sum(hoa_don.tong_tien_discount) as tong_tien_discount_hd, avg(hoa_don.tong_tien) as trung_binh, users.name as ten_nv'))->whereBetween('hoa_don.created_at', array($request->start_time, $request->end_time))->groupBy('user_id', 'ten_nv')->get();
        return Datatables::of($List)
        ->editColumn('ten_nv', function($List){
            // return $this->getUserName($List->user_id);
            return $List->ten_nv;
        })
        ->editColumn('tong_tien_hd', function($List){
            return number_format($List->tong_tien_hd, 2)." VND";
        })
        ->editColumn('tong_tien_discount_hd', function($List){
            return number_format($List->tong_tien_discount_hd, 2)." VND";
        })
        ->editColumn('trung_binh', function($List){
            return number_format($List->trung_binh, 2)." VND";
        })
        ->rawColumns(['ten_nv'])->make(true);
    }

    public function doanhThuLoaiHd(Request $request){
        $List = DB::table('hoa_don')->select(Db::raw('loai_hoa_don, count(ma_hoa_don) as so_hoa_don, sum(tong_tien) as tong_tien_hd, sum(tong_tien_discount) as tong_tien_discount_hd, avg(tong_tien) as trung_binh'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('loai_hoa_don')->get();
        return Datatables::of($List)
        ->editColumn('loai_hoa_don', function($List){
            return $this->getTenLoaiHd($List->loai_hoa_don);
        })
        ->editColumn('tong_tien_hd', function($List){
            return number_format($List->tong_tien_hd, 2)." VND";
        })
        ->editColumn('tong_tien_discount_hd', function($List){
            return number_format($List->tong_tien_discount_hd, 2)." VND";
        })
        ->editColumn('trung_binh', function($List){
            return number_format($List->trung_binh, 2)." VND";
        })
        ->rawColumns(['loai_hoa_don'])->make(true);
    }

    public function getTenLoaiHd($loai_hoa_don){
        $TenHoaDon = LoaiHoaDon::where('ma_loai_hoa_don', $loai_hoa_don)->get();
        return $TenHoaDon[0]->ten_loai_hoa_don;
    }

    public function doanhThuSanPham(Request $request){
        $List = DB::table('hoa_don')->join('chi_tiet_hoa_don', 'hoa_don.ma_hoa_don', '=', 'chi_tiet_hoa_don.ma_hoa_don')->select(Db::raw('id_san_pham, count(DISTINCT chi_tiet_hoa_don.ma_hoa_don) as so_luong_hd, sum(chi_tiet_hoa_don.tong_tien) as tong_tien_sp, sum(chi_tiet_hoa_don.tong_tien_discount) as tong_tien_discount_sp, avg( chi_tiet_hoa_don.tong_tien - ifnull(chi_tiet_hoa_don.tong_tien_discount, 0)) as trung_binh, sum(so_luong) as so_luong_sp'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('chi_tiet_hoa_don.id_san_pham')->get();
        return Datatables::of($List)
        ->editColumn('id_san_pham', function($List){
            return $this->getTenSp($List->id_san_pham);
        })
        ->editColumn('tong_tien_sp', function($List){
            return number_format($List->tong_tien_sp, 2)." VND";
        })
        ->editColumn('tong_tien_discount_sp', function($List){
            return number_format($List->tong_tien_discount_sp, 2)." VND";
        })
        ->editColumn('trung_binh', function($List){
            return number_format($List->trung_binh, 2)." VND";
        })
        ->rawColumns(['id_san_pham'])->make(true);
    }

    public function getTenSp($id){
        $TenSp = SanPham::where('id', $id)->get();
        return $TenSp[0]->ten_san_pham;
    }

    public function doanhThuTheoNgay(Request $request){
        $List = DB::table('hoa_don')->select(Db::raw('YEAR(created_at) as Year, MONTH(created_at) as Month, DAY(created_at) as Day, count(ma_hoa_don) as so_hoa_don, sum(tong_tien) as tong_tien_hd, sum(tong_tien_discount) as tong_tien_discount_hd, avg(tong_tien) as trung_binh'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('Year', 'Month', 'Day')->orderByRaw(DB::raw('Year, Month, Day'))->get();
        return Datatables::of($List)
        ->addColumn('ngay', function($List){
            return $List->Day."-".$List->Month."-".$List->Year;
        })
        ->editColumn('tong_tien_hd', function($List){
            return number_format($List->tong_tien_hd, 2)." VND";
        })
        ->editColumn('tong_tien_discount_hd', function($List){
            return number_format($List->tong_tien_discount_hd, 2)." VND";
        })
        ->editColumn('trung_binh', function($List){
            return number_format($List->trung_binh, 2)." VND";
        })
        ->rawColumns(['ngay'])->make(true);
    }

    public function doanhThuTheoGio(Request $request){
        $List = DB::table('hoa_don')->select(Db::raw('YEAR(created_at) as Year, MONTH(created_at) as Month, DAY(created_at) as Day, HOUR(created_at) as Hour, count(ma_hoa_don) as so_hoa_don, sum(tong_tien) as tong_tien_hd, sum(tong_tien_discount) as tong_tien_discount_hd, avg(tong_tien) as trung_binh'))->whereBetween('created_at', array($request->start_time, $request->end_time))->groupBy('Year', 'Month', 'Day', 'Hour')->orderByRaw(DB::raw('Year, Month, Day, Hour'))->get();
        return Datatables::of($List)
        ->addColumn('gio', function($List){
            return $List->Day."-".$List->Month."-".$List->Year." ".$List->Hour."h";
        })
        ->editColumn('tong_tien_hd', function($List){
            return number_format($List->tong_tien_hd, 2)." VND";
        })
        ->editColumn('tong_tien_discount_hd', function($List){
            return number_format($List->tong_tien_discount_hd, 2)." VND";
        })
        ->editColumn('trung_binh', function($List){
            return number_format($List->trung_binh, 2)." VND";
        })
        ->rawColumns(['gio'])->make(true);
    }

    // public function doanhThuPromoion(Request $request){
    //     $List = DB::table('hoa_don')->join('promotion', 'hoa_don.promotion_id', '=', 'promotion.id')->select(Db::raw('promotion_id, count(DISTINCT hoa_don.ma_hoa_don) as so_luong_hd, sum(DISTINCT hoa_don.tong_tien) as tong_tien_hd, sum(DISTINCT hoa_don.tong_tien_discount) as tong_tien_discount_hd, avg(DISTINCT hoa_don.tong_tien) as trung_binh, sum(so_luong) as so_luong_sp'))
    // }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoaiSanPham;
use Illuminate\Support\Facades\Auth;
use Hash;
use DB;
use PDF;
class pageController extends Controller
{
    //
    public function login(){
    	return view('pages.login');
    }

    public function setting(){
      return view('pages.setting');
    }

    public function checkLogin(Request $request){
       $this->validate($request, [
        'email' => 'required',
        'password' => 'required|min:3|max:32'
       ],
       [
        'email.required' => "Bạn chưa nhập email",
        'password.required' => "Bạn chưa nhập password",
        'password.min' => "Password không được ít hơn 3 ký tự",
        'password.max' => "Password không được vượt quá 32 ký tự"
       ]);

       if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
        return redirect()->route('client');
       }
       else{
        $thongbao = "Đăng nhập không thành công";
        return view('pages.login', ['thongbao' => $thongbao]);
       }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }

    public function getDashboard(){
      $List = DB::table('hoa_don')->select(DB::raw('sum(tong_tien - ifnull(tong_tien_discount, 0)) as tong_doanh_thu, count(ma_hoa_don) as so_hoa_don, avg(tong_tien - ifnull(tong_tien_discount, 0)) as trung_binh'))->whereRaw('DATE(created_at) = CURRENT_DATE()')->get();

      $tong_doanh_thu = 0;
      if($List[0]->tong_doanh_thu != null){
        $tong_doanh_thu = $List[0]->tong_doanh_thu;
      }

      $trung_binh = 0;
      if($List[0]->trung_binh != null){
        $trung_binh = $List[0]->trung_binh;
      }

    	return view('pages.bao_cao.chung', ['tong_doanh_thu' => number_format($tong_doanh_thu, 2), 'so_hoa_don' => $List[0]->so_hoa_don, 'trung_binh' => number_format($trung_binh, 2)]);
    }

    public function getTable(){
    	return view('pages.table');
    }

    public function getLoaiSanPham(){
        $data = LoaiSanPham::all();
        return view('pages.thuc_don.loai_san_pham', ['table_data' => $data]);
    }

}

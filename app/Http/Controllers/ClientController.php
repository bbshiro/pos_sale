<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\LoaiSanPham;
use App\LoaiTopping;
use App\SanPham;
use App\Topping;
use App\PhanQuyen;
use App\loaiPromotion;
use App\Promotion;
use App\User;
use App\ChiTietPromotion;
use Yajra\Datatables\Datatables;
use App\HoaDon;
use App\LoaiHoaDon;
use App\ChiTietHoaDon;
use App\TuyChon;
use Session;
use PDF;
use Illuminate\Support\Facades\Auth;
use App\ThongTin;

class ClientController extends Controller
{
    public function getClient(){
        $ListLoaiSanPham = LoaiSanPham::all();
        $ListSanPham = SanPham::all();
        $cart = Session::get('cart', []);
        $Topping = Topping::all();
        $ListAvailablePromotion = Promotion::where('time_end', '>', date("Y-m-d"))->get();
        $LoaiHoaDon = LoaiHoaDon::all();
        return view('client.client', ['ListLoaiSanPham' => $ListLoaiSanPham, 'ListSanPham' => $ListSanPham, 'cart' => $cart, 'Topping' => $Topping, 'ListPromotion' => $ListAvailablePromotion, 'LoaiHoaDons' => $LoaiHoaDon]);
    }

    public function addPosale(Request $request){
        
        $new_item = ['product_id' => $request->product_id, 'name' => $request->name, 'price' => $request->price, 'quantity' => 1, 'cart_id' => 1, 'total_price' => $request->price];
        
        if(Session::has('cart')){
            $cart = Session::get('cart', []);
            if(array_key_exists('items', $cart)){
                $items = $cart['items'];
                $i = 0;
                for ($i; $i < sizeof($items) ; $i++) { 
                    if($request->product_id == $items[$i]['product_id'] && !array_key_exists('option', $items[$i])){
                        break;
                    }
                }

                if($i >= sizeof($items)){
                    $new_item['cart_id'] = end($items)['cart_id'] + 1;
                    array_push($items, $new_item);
                    $cart['items'] = $items;
                    
                } else{
                    $items[$i]['quantity'] = $items[$i]['quantity'] + 1;
                    $items[$i] = $this->updateItemTotalPrice($items[$i]);
                    $cart['items'] = $items;
                    // Session::put('cart', $cart);
                }

            }else{
                $items = [];
                array_push($items, $new_item);
                $cart['items'] = $items;
                // Session::put('cart', $cart);
            }
        }else{
            $cart = array();
            $items = [];
            array_push($items, $new_item);
            $cart['items'] = $items;
            // Session::put('cart', $cart);
        }
        
        if($request->promotion_id != -1){
            $Promotion = Promotion::where('id', $request->promotion_id)->get();
            $cart['promotion'] = ['id' => $Promotion[0]['id'], 'name' => $Promotion[0]['ten_promotion'],'discount_percent' => $Promotion[0]['percent_discount'], 'type' => $Promotion[0]['loai_promotion']];
            Session::put('cart', $cart);
        }

        $cart['total_cart_price'] = $this->updateTotalPriceCart($items);
        $cart['items'] = $this->caculateDiscountPrice($items);
        if($this->caculateFinalTotalPrice($cart['items']) != 0){
            $cart['final_total_price'] = $this->caculateFinalTotalPrice($cart['items']);
        } 
        else 
        {
            $cart['final_total_price'] = $cart['total_cart_price'];
        }
        Session::put('cart', $cart);
        $saved = Session::get('cart', []);
        return json_encode($saved);
    }

    public function removeCart(){
        Session::forget('cart');
        return json_encode([]);
    }

    public function addProductOption(Request $request){
        $cart = Session::get('cart', []);
        $items = $cart['items'];
        $option = $request->option;
        $cart_id = $request->cart_id;
        $topping_price = 0;
        for($i = 0; $i < sizeof($option) ; $i++){
            $topping = Topping::where('id', $option[$i]['id'])->get();
            $option[$i]['price'] = $topping[0]->price;
            $option[$i]['name']  = $topping[0]->ten_topping;
            $topping_price = $topping_price + $topping[0]->price;
        }

        for ($i = 0; $i < sizeof($items) ; $i++) { 
            if($cart_id == $items[$i]['cart_id']){
                $items[$i]['option'] = $option;
                $items[$i]['total_price'] = ($items[$i]['price'] + $topping_price)*$items[$i]['quantity'];
            }
        }

        $cart['total_cart_price'] = $this->updateTotalPriceCart($items);
        $cart['items'] = $this->caculateDiscountPrice($items);
        if($this->caculateFinalTotalPrice($cart['items']) != 0){
            $cart['final_total_price'] = $this->caculateFinalTotalPrice($cart['items']);
        } 
        else 
        {
            $cart['final_total_price'] = $cart['total_cart_price'];
        }
        Session::put('cart', $cart);
        
        return json_encode(Session::get('cart', []));
    }

    public function removeProduction(Request $request){
        $cart = Session::get('cart', []);
        $items = $cart['items'];
        $cart_id = $request->cart_id;
        $temp_items = [];
        foreach ($items as $item) {
            if($item['cart_id'] != $cart_id){
                array_push($temp_items, $item);
            }
        }

        $cart['total_cart_price'] = $this->updateTotalPriceCart($temp_items);
        $cart['items'] = $temp_items;
        if($this->caculateFinalTotalPrice($cart['items']) != 0){
            $cart['final_total_price'] = $this->caculateFinalTotalPrice($cart['items']);
        } 
        else 
        {
            $cart['final_total_price'] = $cart['total_cart_price'];
        }
        Session::put('cart', $cart);
        
        return json_encode(Session::get('cart', []));
    }

    public function updateItemTotalPrice($item){
        if(array_key_exists('option', $item)){
            $option = $item['option'];
            $topping_price = 0;
            for($i = 0; $i < sizeof($option) ; $i++){
                $topping = Topping::where('id', $option[$i]['id'])->get();
                $topping_price = $topping_price + $topping[0]->price;
            }

            $item['total_price'] = ($item['price'] + $topping_price)*$item['quantity'];
        }
        else {
            $item['total_price'] = $item['price']*$item['quantity'];
        }

        return $item;
    }

    public function updateTotalPriceCart($Items){
        $total_cart_price = 0;
        foreach ($Items as $Item) {
            $total_cart_price = $total_cart_price + $Item['total_price'];
        }
        return $total_cart_price;
    }

    public function caculateFinalTotalPrice($Items){
        $final_total_price = 0;
        foreach ($Items as $Item) {
            if(array_key_exists('discount_price', $Item)){
                $final_total_price = $final_total_price + $Item['discount_price'];
            }
        }
        return $final_total_price;
    }

    public function caculateDiscountPrice($items){
        $cart = Session::get('cart', []);
        if(array_key_exists('promotion', $cart)){
            $Promotion = $cart['promotion'];
            $DiscountPercent = $Promotion['discount_percent'];
            $DiscountType = $Promotion['type'];
            $promotion_id = $Promotion['id'];

            if($DiscountType == 'BIL'){
                for($i = 0; $i < sizeof($items); $i++){
                    $items[$i]['discount_price'] = $items[$i]['total_price'] - $items[$i]['total_price']*$DiscountPercent/100;
                }
            } 
            else 
            {
                for($i = 0; $i < sizeof($items); $i++){
                    if($this->checkPromotion($items[$i]['product_id'], $promotion_id)){
                        $items[$i]['discount_price'] = $items[$i]['total_price'] - ($items[$i]['price']*$DiscountPercent/100)*$items[$i]['quantity'];
                    }
                }
            }

            return $items;
        }
        else
        {
            return $items;
        }
        
    }

    public function checkPromotion($item_id, $promotion_id){
        $Detail = ChiTietPromotion::where('promotion_id', $promotion_id)->where('san_pham_id', $item_id)->get();

        if($Detail->isEmpty()){
            return false;
        } else{
            return true;
        }
        return false;
    }

    public function updatePromotion(Request $request){
        $cart = Session::get('cart', []);
        $Promotion = Promotion::where('id', $request->promotion_id)->get();
        $cart['promotion'] = ['id' => $Promotion[0]['id'], 'name' => $Promotion[0]['ten_promotion'],'discount_percent' => $Promotion[0]['percent_discount'], 'type' => $Promotion[0]['loai_promotion']];
        Session::put('cart', $cart);

        if(array_key_exists('items', $cart)){
            $items = $cart['items'];
            $cart['total_cart_price'] = $this->updateTotalPriceCart($items);
            $cart['items'] = $this->caculateDiscountPrice($items);
            if($this->caculateFinalTotalPrice($cart['items']) != 0){
            $cart['final_total_price'] = $this->caculateFinalTotalPrice($cart['items']);
            } 
            else 
            {
                $cart['final_total_price'] = $cart['total_cart_price'];
            }
            Session::put('cart', $cart);
            $saved = Session::get('cart', []);
            return json_encode($saved);
        } 
        else 
        {
            $saved = Session::get('cart', []);
            return json_encode($saved);
        }
    }

    public function minusQuantity(Request $request){
        $cart_id = $request->cart_id;
        $cart = Session::get('cart', []);
        $items = $cart['items'];
        for($i = 0; $i < sizeof($items); $i++){
            if($items[$i]['cart_id'] == $cart_id && $items[$i]['quantity'] > 1){
                $items[$i]['quantity'] = $items[$i]['quantity'] - 1;
                $items[$i] = $this->updateItemTotalPrice($items[$i]);   
            }
        }

        $cart['items'] = $items;
        $cart['total_cart_price'] = $this->updateTotalPriceCart($items);
        $cart['items'] = $this->caculateDiscountPrice($items);
        if($this->caculateFinalTotalPrice($cart['items']) != 0){
        $cart['final_total_price'] = $this->caculateFinalTotalPrice($cart['items']);
        } 
        else 
        {
            $cart['final_total_price'] = $cart['total_cart_price'];
        }
        Session::put('cart', $cart);
        $saved = Session::get('cart', []);
        return json_encode($saved);
    }

    public function addQuantity(Request $request){
        $cart_id = $request->cart_id;
        $cart = Session::get('cart', []);
        $items = $cart['items'];
        for($i = 0; $i < sizeof($items); $i++){
            if($items[$i]['cart_id'] == $cart_id){
                $items[$i]['quantity'] = $items[$i]['quantity'] + 1;
                $items[$i] = $this->updateItemTotalPrice($items[$i]);
            }
        }

        $cart['items'] = $items;
        $cart['total_cart_price'] = $this->updateTotalPriceCart($items);
        $cart['items'] = $this->caculateDiscountPrice($items);
        if($this->caculateFinalTotalPrice($cart['items']) != 0){
        $cart['final_total_price'] = $this->caculateFinalTotalPrice($cart['items']);
        } 
        else 
        {
            $cart['final_total_price'] = $cart['total_cart_price'];
        }
        Session::put('cart', $cart);
        $saved = Session::get('cart', []);
        return json_encode($saved);
    }

    public function submitCart(Request $request){
        $payment_type = $request->payment_type;
        
        if(Session::has('cart')){
            $cart = Session::get('cart', []);
            $invoice = new HoaDon();
            $invoice->ma_hoa_don = $this->billId();
            $invoice->created_at = date("Y-m-d H:i:s");
            $invoice->tong_tien = $cart['total_cart_price'];
            $invoice->user_id = Auth::user()->id;
            $invoice->loai_hoa_don = $payment_type;
            $invoice->khach_tra = $request->client_money;
            $invoice->tien_thua = $request->excess_cash;

            if(array_key_exists('promotion', $cart)){
                $invoice->promotion_id = $cart['promotion']['id'];
                $invoice->tong_tien_discount = $cart['total_cart_price'] - $cart['final_total_price'];
            }

            $invoice->save();

            $latest = HoaDon::where('ma_hoa_don', '=', $invoice->ma_hoa_don)->first();
            $this->addChiTietHoaDon($latest->ma_hoa_don);  

        } else{
            $data = array('items' => [], 'ma_hoa_don' => 'undefined');
            return json_encode($data);
        }
        
        $latest = HoaDon::latest()->first(); 
        Session::forget('cart');
        $url = route('getInvoice', ['ma_hoa_don' => $invoice->ma_hoa_don]);
        $data = array('items' => Session::get('cart', []), 'ma_hoa_don' => $invoice->ma_hoa_don, 'url' => $url);
        Session::put('ma_hoa_don', $invoice->ma_hoa_don);
        return json_encode($data);
    }

    public function billId(){
        $year = date('y');
        $month = date('m');
        $day = date('d');
        $hour = date('H');
        $minute = date('i');
        $second = date('s');
        return $year.$month.$day.$hour.$minute.$second;
    }

    public function addChiTietHoaDon($Id){
        $cart = Session::get('cart', []);
        if(array_key_exists('items', $cart)){
            $items = $cart['items'];
            for($i = 0; $i < sizeof($items); $i++){
                $item = $items[$i];
                $detail = new ChiTietHoaDon();
                $detail->ma_hoa_don = $Id;
                $detail->id_san_pham = $item['product_id'];
                $detail->so_luong = $item['quantity'];
                $detail->tong_tien = $item['total_price'];
                if(array_key_exists('discount_price', $item)){
                    $detail->tong_tien_discount = $item['total_price'] - $item['discount_price'];
                }
                $detail->save();
                $latest = ChiTietHoaDon::orderBy('id', 'desc')->first();
                if(array_key_exists('option', $item)){
                    $option = $item['option'];
                    $this->addTuyChon($option, $latest->id);
                }
            }
        }
    }

    public function addTuyChon($option, $Id){
        for($i = 0; $i < sizeof($option); $i++){
            $value = $option[$i];
            $tuy_chon = new TuyChon();
            $tuy_chon->id_chi_tiet_hoa_don = $Id;
            $tuy_chon->id_topping = $value['id'];
            if(array_key_exists('ghi_chu', $value)){
                $tuy_chon->percent = str_replace('%', '', $value['ghi_chu']);
            }
            $tuy_chon->save();
        }
    }

    public function getInvoice($ma_hoa_don){
        $hoa_don = HoaDon::where('ma_hoa_don', '=', $ma_hoa_don)->first();
        echo $ma_hoa_don;
        $cart = array();
        $cart['created_at'] = $hoa_don->created_at;
        $cart['tong_cong'] = number_format($hoa_don->tong_tien);
        $cart['tong_tien'] = number_format($hoa_don->tong_tien - $hoa_don->tong_tien_discount);
        $cart['khach_tra'] = number_format($hoa_don->khach_tra);
        $cart['tien_thua'] = number_format($hoa_don->tien_thua);
        $user = User::where('id', '=', $hoa_don->user_id)->first();
        $cart['nhan_vien'] = $user->name;

        $cart['items'] = [];
        $cart['tong_sl'] = 0;
        $list_chi_tiet_hd = ChiTietHoaDon::where('ma_hoa_don', '=', $ma_hoa_don)->get();
        foreach ($list_chi_tiet_hd as $chi_tiet_hoa_don) {
            $item = array();
            $san_pham = SanPham::where('id', '=', $chi_tiet_hoa_don->id_san_pham)->first();

            $item['ten_san_pham'] = $san_pham->ten_san_pham;
            $item['price'] = $san_pham->price;
            $item['tong_cong'] = number_format($chi_tiet_hoa_don->tong_tien);
            $item['tong_tien'] = number_format($chi_tiet_hoa_don->tong_tien - $chi_tiet_hoa_don->tong_tien_discount);
            $item['so_luong'] = $chi_tiet_hoa_don->so_luong;
            $cart['tong_sl'] = $cart['tong_sl'] + $chi_tiet_hoa_don->so_luong;

            $options = TuyChon::where('id_chi_tiet_hoa_don', '=', $chi_tiet_hoa_don->id)->get();
            if (!empty($options)) {
                $item['options'] = [];
                foreach ($options as $key => $option) {
                    $product_option = array();
                    $topping = Topping::where('id', '=', $option->id_topping)->first();
                    $product_option['ten_topping'] = $topping->ten_topping;
                    $product_option['topping_price'] = number_format($topping->price);
                    if($option->percent != NULL){
                        $product_option['percent'] = $option->percent;
                    }

                    array_push($item['options'], $product_option);
                }
            }

            array_push($cart['items'], $item);
        }

        $thong_tin = ThongTin::find(1)->first();
        return PDF::loadView('client.invoice', ['bill_data' => $cart, 'ma_hoa_don' => $ma_hoa_don, 'thong_tin' => $thong_tin])->setPaper('a5')->stream();

    }
}

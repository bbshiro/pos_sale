<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiSanPham extends Model
{
    protected $table = 'loai_san_pham';

    public $timestamps = false;

    public function sanpham(){
		return $this->hasMany('App\SanPham', 'ma_loai_san_pham', 'loai_san_pham');
    }
}

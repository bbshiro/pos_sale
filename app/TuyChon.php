<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TuyChon extends Model
{
    protected $table = 'tuy_chon';
    public $timestamps = false;

    public function chitiethoadon(){
   		return $this->belongsTo('App\ChiTietHoaDon', 'id_chi_tiet_hoa_don', 'id');
   	}

   	public function topping(){
   		return $this->belongsTo('App\Topping', 'id_topping', 'id');
   	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiTopping extends Model
{
    protected $table = 'loai_topping';

    public $timestamps = false;

    public function topping(){
		return $this->hasMany('App\Topping', 'loai_topping', 'ma_loai_topping');
    }
}

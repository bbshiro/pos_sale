<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThongTin extends Model
{
    protected $table = 'thong_tin';
    public $timestamps = false;

    public $primary = "id";
    protected $fillable = ['name', 'phone_number', 'address', 'pass_wifi', 'logo'];
}

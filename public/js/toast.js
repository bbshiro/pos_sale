function show_toast(Text, Type){
  if(Type == "warning"){
    $('#toast_content').removeClass("alert-success");
    $('#toast_content').addClass("alert-danger");
    $('.toast').toast({delay: 2000});
    $('#toast_content').text(Text);
    $('.toast').toast('show');
  } else if(Type == "success"){
    $('#toast_content').removeClass("alert-danger");
    $('#toast_content').addClass("alert-success");
    $('.toast').toast({delay: 2000});
    $('#toast_content').text(Text);
    $('.toast').toast('show');
  }
}

-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 24, 2019 lúc 07:45 PM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `sale`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chi_tiet_hoa_don`
--

CREATE TABLE `chi_tiet_hoa_don` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ma_hoa_don` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_san_pham` bigint(20) UNSIGNED NOT NULL,
  `so_luong` int(11) NOT NULL,
  `tong_tien` double(12,2) NOT NULL,
  `tong_tien_discount` double(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chi_tiet_hoa_don`
--

INSERT INTO `chi_tiet_hoa_don` (`id`, `ma_hoa_don`, `id_san_pham`, `so_luong`, `tong_tien`, `tong_tien_discount`) VALUES
(18, '190422232638', 14, 2, 114000.00, 11400.00),
(19, '190422233027', 14, 2, 114000.00, 11400.00),
(20, '190422233027', 15, 1, 55000.00, 5500.00),
(21, '190422233027', 14, 1, 47000.00, 4700.00),
(22, '190424235150', 14, 1, 63000.00, 6300.00),
(23, '190504205407', 13, 1, 76000.00, NULL),
(24, '190504205407', 14, 1, 47000.00, NULL),
(25, '190504205717', 15, 1, 55000.00, 11000.00),
(26, '190504205717', 22, 1, 42000.00, 8400.00),
(27, '190506125441', 14, 1, 63000.00, NULL),
(28, '190506125441', 20, 2, 94000.00, NULL),
(29, '190506125832', 15, 1, 55000.00, NULL),
(30, '190506125832', 23, 1, 49000.00, NULL),
(31, '190506125950', 17, 1, 59000.00, NULL),
(32, '190506125950', 92, 1, 37000.00, NULL),
(33, '190508212015', 15, 1, 70000.00, NULL),
(34, '190508212015', 29, 1, 81000.00, NULL),
(35, '190508212143', 101, 2, 90000.00, NULL),
(36, '190508212143', 99, 1, 57000.00, NULL),
(37, '190508212143', 108, 1, 58000.00, NULL),
(38, '190509155631', 16, 1, 47000.00, NULL),
(39, '190509155631', 35, 1, 55000.00, NULL),
(40, '190513212334', 14, 1, 47000.00, NULL),
(41, '190513230459', 13, 1, 71000.00, NULL),
(42, '190513230459', 35, 1, 55000.00, NULL),
(43, '190513230908', 16, 1, 67000.00, NULL),
(44, '190513230908', 23, 1, 49000.00, NULL),
(45, '190513231907', 22, 1, 52000.00, NULL),
(46, '190513231907', 29, 1, 55000.00, NULL),
(47, '190513232356', 17, 1, 59000.00, NULL),
(48, '190513232356', 28, 1, 63000.00, NULL),
(49, '190513232356', 33, 1, 55000.00, NULL),
(50, '190515233133', 16, 1, 53000.00, NULL),
(51, '190515233133', 35, 1, 55000.00, NULL),
(52, '190515233348', 21, 1, 54000.00, NULL),
(53, '190515233348', 29, 1, 55000.00, NULL),
(54, '190516003849', 14, 2, 94000.00, NULL),
(55, '190516003849', 27, 1, 55000.00, NULL),
(56, '190516140622', 22, 1, 42000.00, NULL),
(57, '190516140622', 19, 1, 55000.00, NULL),
(58, '190516140852', 15, 1, 55000.00, NULL),
(59, '190516140852', 13, 1, 55000.00, NULL),
(60, '190516141050', 19, 1, 55000.00, NULL),
(61, '190516141050', 20, 1, 47000.00, NULL),
(62, '190516141209', 22, 1, 52000.00, NULL),
(63, '190516141209', 27, 1, 55000.00, NULL),
(64, '190516141445', 15, 1, 55000.00, NULL),
(65, '190516141445', 28, 1, 47000.00, NULL),
(66, '190516141610', 21, 1, 49000.00, NULL),
(67, '190516141829', 77, 1, 30000.00, NULL),
(68, '190516142238', 14, 1, 47000.00, NULL),
(69, '190516142238', 20, 1, 47000.00, NULL),
(70, '190516143608', 21, 1, 49000.00, NULL),
(71, '190516143608', 16, 1, 47000.00, NULL),
(72, '190516143713', 13, 1, 55000.00, NULL),
(73, '190516143713', 19, 1, 55000.00, NULL),
(74, '190516143907', 21, 1, 49000.00, NULL),
(75, '190516143907', 22, 1, 42000.00, NULL),
(76, '190516144805', 14, 1, 47000.00, NULL),
(77, '190516144805', 21, 1, 49000.00, NULL),
(78, '190516150007', 21, 1, 49000.00, NULL),
(79, '190516150007', 33, 1, 55000.00, NULL),
(80, '190516150039', 22, 1, 42000.00, NULL),
(81, '190516150039', 30, 1, 47000.00, NULL),
(82, '190516164853', 14, 1, 47000.00, NULL),
(83, '190516164853', 29, 2, 130000.00, NULL),
(84, '190516164853', 26, 1, 42000.00, NULL),
(85, '190520233448', 15, 1, 71000.00, NULL),
(86, '190520233448', 22, 1, 42000.00, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chi_tiet_promotion`
--

CREATE TABLE `chi_tiet_promotion` (
  `promotion_id` bigint(20) UNSIGNED NOT NULL,
  `san_pham_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chi_tiet_promotion`
--

INSERT INTO `chi_tiet_promotion` (`promotion_id`, `san_pham_id`) VALUES
(3, 2),
(3, 3),
(3, 6),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(3, 18),
(3, 19);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don`
--

CREATE TABLE `hoa_don` (
  `ma_hoa_don` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `tong_tien` double(12,2) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `loai_hoa_don` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tong_tien_discount` double(8,2) DEFAULT NULL,
  `promotion_id` bigint(20) UNSIGNED DEFAULT NULL,
  `khach_tra` float(12,2) DEFAULT NULL,
  `tien_thua` float(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `hoa_don`
--

INSERT INTO `hoa_don` (`ma_hoa_don`, `created_at`, `tong_tien`, `user_id`, `loai_hoa_don`, `tong_tien_discount`, `promotion_id`, `khach_tra`, `tien_thua`) VALUES
('190422232638', '2019-04-22 23:26:38', 216000.00, 1, 'TAB', 21600.00, 1, 216000.00, 0.00),
('190422233027', '2019-04-22 23:30:27', 216000.00, 1, 'TAB', 21600.00, 1, 216000.00, 0.00),
('190424235150', '2019-04-24 23:51:50', 63000.00, 1, 'TAK', 6300.00, 1, 63000.00, 0.00),
('190504205407', '2019-05-04 20:54:07', 123000.00, 1, 'TAK', NULL, NULL, 123000.00, 0.00),
('190504205717', '2019-05-04 20:57:17', 97000.00, 1, 'TAK', 19400.00, 4, 97000.00, 0.00),
('190506125441', '2019-05-06 12:54:41', 157000.00, 1, 'TAB', NULL, NULL, 157000.00, 0.00),
('190506125832', '2019-05-06 12:58:32', 104000.00, 1, 'TAK', NULL, NULL, 104000.00, 0.00),
('190506125950', '2019-05-06 12:59:50', 96000.00, 1, 'TAK', NULL, NULL, 96000.00, 0.00),
('190508212015', '2019-05-08 21:20:15', 151000.00, 2, 'TAK', NULL, NULL, 151000.00, 0.00),
('190508212143', '2019-05-08 21:21:43', 205000.00, 2, 'TAB', NULL, NULL, 205000.00, 0.00),
('190509155631', '2019-05-09 15:56:31', 102000.00, 1, 'TAK', NULL, NULL, 102000.00, 0.00),
('190513212334', '2019-05-13 21:23:34', 47000.00, 1, 'TAK', NULL, NULL, 47000.00, 0.00),
('190513230459', '2019-05-13 23:04:59', 126000.00, 1, 'TAK', NULL, NULL, 126000.00, 0.00),
('190513230908', '2019-05-13 23:09:08', 116000.00, 1, 'TAK', NULL, NULL, 116000.00, 0.00),
('190513231907', '2019-05-13 23:19:07', 107000.00, 1, 'TAB', NULL, NULL, 107000.00, 0.00),
('190513232356', '2019-05-13 23:23:56', 177000.00, 1, 'TAB', NULL, NULL, 177000.00, 0.00),
('190515233133', '2019-05-15 23:31:33', 108000.00, 1, 'TAK', NULL, NULL, 108000.00, 0.00),
('190515233348', '2019-05-15 23:33:48', 109000.00, 1, 'TAB', NULL, NULL, 109000.00, 0.00),
('190516003849', '2019-05-16 00:38:49', 149000.00, 1, 'TAK', NULL, NULL, 149000.00, 0.00),
('190516140622', '2019-05-16 14:06:22', 97000.00, 2, 'TAK', NULL, NULL, 100000.00, 3000.00),
('190516140852', '2019-05-16 14:08:52', 110000.00, 2, 'TAK', NULL, NULL, 120000.00, 10000.00),
('190516141050', '2019-05-16 14:10:50', 102000.00, 2, 'TAB', NULL, NULL, 105000.00, 3000.00),
('190516141209', '2019-05-16 14:12:09', 107000.00, 2, 'TAB', NULL, NULL, 107000.00, 0.00),
('190516141445', '2019-05-16 14:14:45', 102000.00, 2, 'TAB', NULL, NULL, 200000.00, 98000.00),
('190516141610', '2019-05-16 14:16:10', 49000.00, 2, 'TAK', NULL, NULL, 50000.00, 1000.00),
('190516141829', '2019-05-16 14:18:29', 30000.00, 2, 'TAB', NULL, NULL, 40000.00, 10000.00),
('190516142238', '2019-05-16 14:22:38', 94000.00, 2, 'TAB', NULL, NULL, 100000.00, 6000.00),
('190516143608', '2019-05-16 14:36:08', 96000.00, 2, 'TAK', NULL, NULL, 100000.00, 4000.00),
('190516143713', '2019-05-16 14:37:13', 110000.00, 2, 'TAK', NULL, NULL, 110000.00, 0.00),
('190516143907', '2019-05-16 14:39:07', 91000.00, 2, 'TAB', NULL, NULL, 100000.00, 9000.00),
('190516144805', '2019-05-16 14:48:05', 96000.00, 2, 'TAK', NULL, NULL, 100000.00, 4000.00),
('190516150007', '2019-05-16 15:00:07', 104000.00, 2, 'TAK', NULL, NULL, 200000.00, 96000.00),
('190516150039', '2019-05-16 15:00:39', 89000.00, 2, 'TAB', NULL, NULL, 100000.00, 11000.00),
('190516164853', '2019-05-16 16:48:53', 219000.00, 1, 'TAK', NULL, NULL, 300000.00, 81000.00),
('190520233448', '2019-05-20 23:34:48', 113000.00, 1, 'TAB', NULL, NULL, 120000.00, 7000.00);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_hoa_don`
--

CREATE TABLE `loai_hoa_don` (
  `ma_loai_hoa_don` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten_loai_hoa_don` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `loai_hoa_don`
--

INSERT INTO `loai_hoa_don` (`ma_loai_hoa_don`, `ten_loai_hoa_don`) VALUES
('TAB', 'Tại Bàn'),
('TAK', 'Mang Về');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_promotion`
--

CREATE TABLE `loai_promotion` (
  `ma_loai_promotion` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten_loai_promotion` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `loai_promotion`
--

INSERT INTO `loai_promotion` (`ma_loai_promotion`, `ten_loai_promotion`) VALUES
('BIL', 'Tổng Bill'),
('PAR', 'Sale một số sản phẩm');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_san_pham`
--

CREATE TABLE `loai_san_pham` (
  `ma_loai_san_pham` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten_loai_san_pham` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `loai_san_pham`
--

INSERT INTO `loai_san_pham` (`ma_loai_san_pham`, `ten_loai_san_pham`) VALUES
('ANV', 'Ăn vặt'),
('COF', 'Coffee'),
('CPT', 'Classic Pure Tea'),
('FRM', 'Fresh Milk'),
('FRT', 'Fruit Tea'),
('IBL', 'Ice Blend'),
('MIT', 'Milk Tea'),
('SMT', 'Smoothie'),
('SPT', 'Special Tea'),
('WDR', 'Winter Drinks'),
('YOG', 'Sữa chua');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loai_topping`
--

CREATE TABLE `loai_topping` (
  `ma_loai_topping` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten_loai_topping` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `loai_topping`
--

INSERT INTO `loai_topping` (`ma_loai_topping`, `ten_loai_topping`) VALUES
('OTH', 'Khác'),
('TFT', 'Topping for Tea');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(7, '2014_10_12_000000_create_users_table', 1),
(8, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2019_03_10_161531_phanquyen', 1),
(10, '2019_03_10_163813_add_users_foreign_key', 2),
(13, '2019_03_10_165012_loai_san_pham', 3),
(14, '2019_03_10_170129_san_pham', 3),
(15, '2019_03_10_171528_add_foreign_key_san_pham', 4),
(20, '2019_03_10_172709_loai_topping', 5),
(21, '2019_03_10_172918_topping', 5),
(22, '2019_03_10_173804_add_foreign_key_topping', 5),
(23, '2019_03_19_162215_update_san_pham', 6),
(31, '2019_03_19_164145_make_hoa_don', 7),
(32, '2019_03_20_164634_make_loai_hoa_don', 7),
(33, '2019_03_20_165853_add_foreign_key_hoa_don', 8),
(35, '2019_03_20_170145_make_chi_tiet_hoa_don', 9),
(36, '2019_03_20_171825_add_foreign_key_chi_tiet_hoa_don', 10),
(37, '2019_03_20_172509_make_tuy_chon', 11),
(38, '2019_03_20_173514_add_foreign_key_tuy_chon', 12),
(41, '2019_03_22_034935_make_loai_promotion', 13),
(42, '2019_03_22_035302_make_promotion', 13),
(43, '2019_03_22_040914_add_foreign_key_promotion', 14),
(44, '2019_03_22_180511_make_chi_tiet_promotion', 15),
(45, '2019_03_22_183615_add_foreign_key_chi_tiet_promotion', 16),
(47, '2019_03_22_184646_update_hoa_don', 17),
(48, '2019_03_22_191253_foreign_key_for_hoa_don', 18),
(49, '2019_03_22_191724_update_chi_tiet_hoa_don', 19),
(50, '2019_03_23_024237_make_loai_hoa_don', 20),
(51, '2019_04_22_044947_create_hoa_don', 21),
(52, '2019_04_22_065710_loai_hoa_don', 22),
(53, '2019_04_22_070009_add_foreign_key', 23),
(54, '2019_04_22_070147_create_chi_tiet_hoa_don', 24),
(55, '2019_04_22_070331_add_foreign_key_chi_tiet_hoa_don', 25),
(56, '2019_04_22_070614_create_tuy_chon', 26),
(57, '2019_04_22_070714_add_foreign_key_tuy_chon', 27),
(58, '2019_04_22_223028_update_chi_tiet_hoa_don', 28),
(59, '2019_04_22_223316_update_chi_tiet_hoa_don', 29);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `phanquyen`
--

CREATE TABLE `phanquyen` (
  `ma_phan_quyen` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten_quyen` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `phanquyen`
--

INSERT INTO `phanquyen` (`ma_phan_quyen`, `ten_quyen`) VALUES
('NVI', 'Nhân Viên'),
('QLY', 'Quản Lý');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `promotion`
--

CREATE TABLE `promotion` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten_promotion` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_start` date NOT NULL,
  `time_end` date NOT NULL,
  `percent_discount` int(11) NOT NULL,
  `loai_promotion` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `promotion`
--

INSERT INTO `promotion` (`id`, `ten_promotion`, `time_start`, `time_end`, `percent_discount`, `loai_promotion`) VALUES
(1, 'Giám giá ngày 30/4', '2019-04-02', '2019-04-30', 10, 'BIL'),
(2, 'Giảm giá một số sản phẩm', '2019-03-28', '2019-03-29', 30, 'PAR'),
(3, 'giảm giá lần 1', '2019-04-15', '2019-04-18', 25, 'PAR'),
(4, 'giảm giá tháng 5', '2019-05-04', '2019-05-05', 20, 'BIL');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `san_pham`
--

CREATE TABLE `san_pham` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `loai_san_pham` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten_san_pham` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `ghi_chu` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `san_pham`
--

INSERT INTO `san_pham` (`id`, `loai_san_pham`, `ten_san_pham`, `price`, `ghi_chu`) VALUES
(2, 'ANV', 'Hướng Dương', '20000.00', 'Ăn'),
(3, 'FRT', 'Trà Đào Cam Sả - L', '64000.00', NULL),
(4, 'CPT', 'Trà Ahamd bạc hà', '28000.00', NULL),
(5, 'CPT', 'Trà Ahamad dâu', '28000.00', NULL),
(6, 'CPT', 'Trà Ahamad táo', '28000.00', NULL),
(7, 'CPT', 'Trà Ahamad Đào', '28000.00', NULL),
(8, 'CPT', 'Hồng Trà- L', '34000.00', NULL),
(9, 'CPT', 'Hồng Trà - M', '29000.00', NULL),
(10, 'CPT', 'Trà Ô Long - L', '34000.00', NULL),
(11, 'CPT', 'Trà Ô Long - M', '29000.00', NULL),
(12, 'CPT', 'Trà Xanh Hoa Nhài - L', '34000.00', NULL),
(13, 'MIT', 'Trà Sữa Matcha - L', '55000.00', NULL),
(14, 'MIT', 'Trà Sữa Matcha - M', '47000.00', NULL),
(15, 'MIT', 'Trà Sữa Bạc Hà - L', '55000.00', NULL),
(16, 'MIT', 'Trà Sữa Bạc Hà - M', '47000.00', NULL),
(17, 'MIT', 'Trà Sữa Socola Bạc Hà - L', '59000.00', NULL),
(18, 'MIT', 'Trà Sữa Socola Bạc Hà - M', '49000.00', NULL),
(19, 'MIT', 'Trà Sữa Socola - L', '55000.00', NULL),
(20, 'MIT', 'Trà Sữa Socola - M', '47000.00', NULL),
(21, 'MIT', 'Trà Ô Long Sữa - L', '49000.00', NULL),
(22, 'MIT', 'Trà Ô Long Sữa - M', '42000.00', NULL),
(23, 'MIT', 'Trà Xanh Sữa - L', '49000.00', NULL),
(24, 'MIT', 'Trà Xanh Sữa - M', '42000.00', NULL),
(25, 'MIT', 'Hồng Trà Sữa - L', '49000.00', NULL),
(26, 'MIT', 'Hồng Trà Sữa - M', '42000.00', NULL),
(27, 'MIT', 'Trà Sữa Việt Quất - L', '55000.00', NULL),
(28, 'MIT', 'Trà Sữa Việt Quất - M', '47000.00', NULL),
(29, 'MIT', 'Trà Sữa Dâu Tây - L', '55000.00', NULL),
(30, 'MIT', 'Trà Sữa Dâu Tây', '47000.00', NULL),
(31, 'MIT', 'Trà Sữa Đào - L', '55000.00', NULL),
(32, 'MIT', 'Trà Sữa Đào - M', '47000.00', NULL),
(33, 'MIT', 'Trà Sữa Kiwi - L', '55000.00', NULL),
(34, 'MIT', 'Trà Sữa Kiwi - M', '47000.00', NULL),
(35, 'MIT', 'Trà Sữa Xoài - L', '55000.00', NULL),
(36, 'MIT', 'Trà Sữa Xoài - M', '47000.00', NULL),
(37, 'FRT', 'Trà Xanh Kiwi - L', '53000.00', NULL),
(38, 'FRT', 'Trà Xanh Kiwi - M', '44000.00', NULL),
(39, 'FRT', 'Trà Xoài - L', '53000.00', NULL),
(40, 'FRT', 'Trà Chanh Leo - L', '53000.00', NULL),
(41, 'FRT', 'Trà Chanh Leo - M', '44000.00', NULL),
(42, 'FRT', 'Trà Đao Cam Sả - L', '64000.00', NULL),
(43, 'FRT', 'Trà Đào Cam Sả - M', '54000.00', NULL),
(44, 'FRT', 'Trà Táo - L', '53000.00', NULL),
(45, 'FRT', 'Trà Táo - M', '44000.00', NULL),
(46, 'FRT', 'Trà Bạc Hà - L', '53000.00', NULL),
(47, 'FRT', 'Trà Bạc Hà - M', '44000.00', NULL),
(48, 'FRT', 'Trà Việt Quất - L', '53000.00', NULL),
(49, 'FRT', 'Trà Việt Quất - M', '44000.00', NULL),
(50, 'FRT', 'Trà Táo Bạc Hà - L', '57000.00', NULL),
(51, 'FRT', 'Trà Táo Bạc Hà - M', '47000.00', NULL),
(52, 'IBL', 'Caramel dừa đá xay - L', '72000.00', NULL),
(53, 'IBL', 'Caramel dừa đá xay - M', '59000.00', NULL),
(54, 'IBL', 'Cam xoài đá xay - L', '72000.00', NULL),
(55, 'IBL', 'Cam xoài đá xay - M', '59000.00', NULL),
(56, 'SMT', 'Xoài Dừa Đá Xay - L', '69000.00', NULL),
(57, 'SMT', 'Xoài Dừa Đá Xay - M', '57000.00', NULL),
(58, 'SMT', 'Matcha Đá Xay - L', '56000.00', NULL),
(59, 'SMT', 'Matcha Đá Xay - M', '49000.00', NULL),
(60, 'SMT', 'Socola Đá Xay - L', '56000.00', NULL),
(61, 'ANV', 'Socola Đá Xay - M', '49000.00', NULL),
(62, 'SMT', 'Việt Quất Đá Xay - L', '56000.00', NULL),
(63, 'SMT', 'Việt Quất Đá Xay - M', '49000.00', NULL),
(64, 'SMT', 'Dâu Tây Đá Xay - L', '56000.00', NULL),
(65, 'SMT', 'Dâu Tay Đá Xay - M', '49000.00', NULL),
(66, 'SMT', 'Xoài Đá Xay - L', '56000.00', NULL),
(67, 'SMT', 'Xoài Đá Xay - M', '49000.00', NULL),
(68, 'SMT', 'Kiwi Đá Xay - L', '56000.00', NULL),
(69, 'SMT', 'Kiwi Đá Xay - M', '49000.00', NULL),
(70, 'SMT', 'Chanh Leo Đá Xay - L', '56000.00', NULL),
(71, 'SMT', 'Chanh Leo Đá Xay - M', '49000.00', NULL),
(72, 'SMT', 'Chanh Tuyết - L', '55000.00', NULL),
(73, 'SMT', 'Chanh Tuyết - M', '49000.00', NULL),
(74, 'SMT', 'Sinh Tố Dưa Hấu', '46000.00', NULL),
(75, 'SPT', 'Nước Chanh Sả Mật Ong', '42000.00', NULL),
(76, 'SPT', 'Nước Cam Vắt', '48000.00', NULL),
(77, 'SPT', 'Nước Chanh', '30000.00', NULL),
(78, 'COF', 'Mocha Coffee', '62000.00', NULL),
(79, 'COF', 'Caramel Macchiatio', '49000.00', NULL),
(80, 'COF', 'Cafe Đá Xay', '46000.00', NULL),
(81, 'COF', 'Cafe Cốt Dừa', '48000.00', NULL),
(82, 'COF', 'Bac Xỉu', '42000.00', NULL),
(83, 'COF', 'Americano', '35000.00', NULL),
(84, 'COF', 'Espresso Double', '35000.00', NULL),
(85, 'COF', 'Espresso Single', '34000.00', NULL),
(86, 'COF', 'Cafe Giấy Lọc', '40000.00', NULL),
(87, 'COF', 'Cappuccino/Latte Coffee', '47000.00', NULL),
(88, 'COF', 'Cafe Nâu', '34000.00', NULL),
(89, 'COF', 'Cafe Đen', '29000.00', NULL),
(90, 'YOG', 'Sữa Chua Việt Quất', '47000.00', NULL),
(91, 'YOG', 'Sữa Chua Xoài', '47000.00', NULL),
(92, 'YOG', 'Sữa Chua Đánh Đá Nha Đam', '37000.00', NULL),
(93, 'ANV', 'Sữa Chua Đánh Đá Thạch Tổ', '37000.00', NULL),
(94, 'YOG', 'Sữa Chua Đánh Đá Cacao', '37000.00', NULL),
(95, 'YOG', 'Sữa Chua Đánh Đá Cafe', '37000.00', NULL),
(96, 'YOG', 'Sữa Chua Đánh Đá', '29000.00', NULL),
(97, 'WDR', 'Trà sương mù London', '47000.00', NULL),
(98, 'WDR', 'Trà Bưởi Mật Ong', '47000.00', NULL),
(99, 'WDR', 'Trà Sữa Gừng', '45000.00', NULL),
(100, 'SPT', 'Trà Hoa Cúc Mật Ong', '45000.00', NULL),
(101, 'WDR', 'Trà Cam Quế', '45000.00', NULL),
(102, 'WDR', 'Trà Cam Gừng', '45000.00', NULL),
(103, 'SPT', 'Trà Gừng', '32000.00', NULL),
(104, 'WDR', 'Matcha Latte Nóng', '42000.00', NULL),
(105, 'WDR', 'Cacao Nóng', '42000.00', NULL),
(106, 'WDR', 'Socola Nóng', '42000.00', NULL),
(107, 'FRM', 'Sữa Matcha Macchiato - L', '74000.00', NULL),
(108, 'FRM', 'Sữa Matcha Macchiato - M', '58000.00', NULL),
(109, 'FRM', 'Sữa Socola Macchiato - L', '74000.00', NULL),
(110, 'FRM', 'Sữa Socola Macchiato - M', '58000.00', NULL),
(111, 'FRM', 'Sữa Caramel Macchiato - L', '78000.00', NULL),
(112, 'FRM', 'Sữa Caramel Macchiato - M', '59000.00', NULL),
(113, 'FRM', 'Sữa Tươi Trân Châu Đường Đen - L', '78000.00', NULL),
(114, 'FRM', 'Sữa Tươi Trân Châu Đường Đen - M', '59000.00', NULL),
(115, 'ANV', 'Bim Bim', '14000.00', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `topping`
--

CREATE TABLE `topping` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `loai_topping` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ten_topping` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `ghi_chu` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `topping`
--

INSERT INTO `topping` (`id`, `loai_topping`, `ten_topping`, `price`, `ghi_chu`) VALUES
(1, 'OTH', 'Đá', '0.00', NULL),
(2, 'OTH', 'Đường', '0.00', NULL),
(3, 'TFT', 'Trân châu đen', '5000.00', NULL),
(4, 'TFT', 'Trân châu trắng', '10000.00', NULL),
(5, 'TFT', 'Trân châu đường đen', '10000.00', NULL),
(6, 'TFT', 'Kem sữa', '10000.00', NULL),
(7, 'TFT', 'Nha Đam/ Lô Hội', '6000.00', NULL),
(8, 'TFT', 'Thạch Hoa Quả', '6000.00', NULL),
(9, 'TFT', 'Thạc Pudding', '6000.00', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tuy_chon`
--

CREATE TABLE `tuy_chon` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_chi_tiet_hoa_don` bigint(20) UNSIGNED NOT NULL,
  `id_topping` bigint(20) UNSIGNED NOT NULL,
  `percent` enum('0','30','50','70','100') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tuy_chon`
--

INSERT INTO `tuy_chon` (`id`, `id_chi_tiet_hoa_don`, `id_topping`, `percent`) VALUES
(1, 19, 1, '50'),
(2, 19, 2, '0'),
(3, 19, 5, NULL),
(4, 22, 1, '50'),
(5, 22, 2, '30'),
(6, 22, 5, NULL),
(7, 22, 7, NULL),
(8, 23, 1, '70'),
(9, 23, 2, '50'),
(10, 23, 3, NULL),
(11, 23, 4, NULL),
(12, 23, 9, NULL),
(13, 27, 1, '100'),
(14, 27, 2, '50'),
(15, 27, 4, NULL),
(16, 27, 7, NULL),
(17, 33, 1, '70'),
(18, 33, 2, '50'),
(19, 33, 3, NULL),
(20, 33, 6, NULL),
(21, 34, 1, '70'),
(22, 34, 2, '100'),
(23, 34, 5, NULL),
(24, 34, 6, NULL),
(25, 34, 7, NULL),
(26, 35, 1, '70'),
(27, 35, 2, '50'),
(28, 36, 1, '70'),
(29, 36, 2, '50'),
(30, 36, 8, NULL),
(31, 36, 9, NULL),
(32, 41, 1, '0'),
(33, 41, 4, NULL),
(34, 41, 7, NULL),
(35, 43, 1, '70'),
(36, 43, 5, NULL),
(37, 43, 6, NULL),
(38, 45, 4, NULL),
(39, 48, 1, '70'),
(40, 48, 2, '30'),
(41, 48, 5, NULL),
(42, 48, 7, NULL),
(43, 50, 1, '50'),
(44, 50, 2, '100'),
(45, 50, 7, NULL),
(46, 52, 1, '50'),
(47, 52, 2, '30'),
(48, 52, 3, NULL),
(49, 62, 1, '70'),
(50, 62, 2, '30'),
(51, 62, 4, NULL),
(52, 83, 1, '70'),
(53, 83, 2, '30'),
(54, 83, 4, NULL),
(55, 85, 1, '30'),
(56, 85, 2, '50'),
(57, 85, 4, NULL),
(58, 85, 7, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmnd` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dia_chi` varchar(350) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdt` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ma_phan_quyen` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `cmnd`, `dia_chi`, `sdt`, `email`, `ma_phan_quyen`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Đắc Tuấn Anh', '013554675', 'Cổ Nhuế Hà Nội', '0961558396', 'shichino1202@gmail.com', 'QLY', NULL, '$2y$10$q3NEx9bHIaOTEO3DJPy0D.wIaA9SzEknrHcgP7ZAJAX81FLqwFUq6', NULL, '2019-03-24 22:53:35', '2019-04-28 09:21:18'),
(2, 'Nguyễn Hữu Thịnh', '013645976', 'Hà Nội', '0965395467', 'thinh@gmail.com', 'NVI', NULL, '$2y$10$UiNQCAjgJNyIfcqPuXJXVuSIP5r7N4k5CZDd7i2qTEKQNdMNyy7yu', NULL, '2019-04-28 08:25:04', '2019-04-28 08:25:04');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `chi_tiet_hoa_don`
--
ALTER TABLE `chi_tiet_hoa_don`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chi_tiet_hoa_don_ma_hoa_don_foreign` (`ma_hoa_don`),
  ADD KEY `chi_tiet_hoa_don_id_san_pham_foreign` (`id_san_pham`);

--
-- Chỉ mục cho bảng `chi_tiet_promotion`
--
ALTER TABLE `chi_tiet_promotion`
  ADD PRIMARY KEY (`promotion_id`,`san_pham_id`),
  ADD KEY `chi_tiet_promotion_san_pham_id_foreign` (`san_pham_id`);

--
-- Chỉ mục cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD PRIMARY KEY (`ma_hoa_don`),
  ADD KEY `hoa_don_user_id_foreign` (`user_id`),
  ADD KEY `hoa_don_loai_hoa_don_foreign` (`loai_hoa_don`),
  ADD KEY `hoa_don_promotion_id_foreign` (`promotion_id`);

--
-- Chỉ mục cho bảng `loai_hoa_don`
--
ALTER TABLE `loai_hoa_don`
  ADD PRIMARY KEY (`ma_loai_hoa_don`);

--
-- Chỉ mục cho bảng `loai_promotion`
--
ALTER TABLE `loai_promotion`
  ADD PRIMARY KEY (`ma_loai_promotion`);

--
-- Chỉ mục cho bảng `loai_san_pham`
--
ALTER TABLE `loai_san_pham`
  ADD PRIMARY KEY (`ma_loai_san_pham`);

--
-- Chỉ mục cho bảng `loai_topping`
--
ALTER TABLE `loai_topping`
  ADD PRIMARY KEY (`ma_loai_topping`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `phanquyen`
--
ALTER TABLE `phanquyen`
  ADD PRIMARY KEY (`ma_phan_quyen`);

--
-- Chỉ mục cho bảng `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `promotion_loai_promotion_foreign` (`loai_promotion`);

--
-- Chỉ mục cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  ADD PRIMARY KEY (`id`),
  ADD KEY `san_pham_loai_san_pham_foreign` (`loai_san_pham`);

--
-- Chỉ mục cho bảng `topping`
--
ALTER TABLE `topping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `topping_loai_topping_foreign` (`loai_topping`);

--
-- Chỉ mục cho bảng `tuy_chon`
--
ALTER TABLE `tuy_chon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tuy_chon_id_chi_tiet_hoa_don_foreign` (`id_chi_tiet_hoa_don`),
  ADD KEY `tuy_chon_id_topping_foreign` (`id_topping`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_ma_phan_quyen_foreign` (`ma_phan_quyen`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `chi_tiet_hoa_don`
--
ALTER TABLE `chi_tiet_hoa_don`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT cho bảng `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT cho bảng `topping`
--
ALTER TABLE `topping`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `tuy_chon`
--
ALTER TABLE `tuy_chon`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `chi_tiet_hoa_don`
--
ALTER TABLE `chi_tiet_hoa_don`
  ADD CONSTRAINT `chi_tiet_hoa_don_id_san_pham_foreign` FOREIGN KEY (`id_san_pham`) REFERENCES `san_pham` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chi_tiet_hoa_don_ma_hoa_don_foreign` FOREIGN KEY (`ma_hoa_don`) REFERENCES `hoa_don` (`ma_hoa_don`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `chi_tiet_promotion`
--
ALTER TABLE `chi_tiet_promotion`
  ADD CONSTRAINT `chi_tiet_promotion_promotion_id_foreign` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chi_tiet_promotion_san_pham_id_foreign` FOREIGN KEY (`san_pham_id`) REFERENCES `san_pham` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD CONSTRAINT `hoa_don_loai_hoa_don_foreign` FOREIGN KEY (`loai_hoa_don`) REFERENCES `loai_hoa_don` (`ma_loai_hoa_don`) ON DELETE CASCADE,
  ADD CONSTRAINT `hoa_don_promotion_id_foreign` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `hoa_don_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `promotion`
--
ALTER TABLE `promotion`
  ADD CONSTRAINT `promotion_loai_promotion_foreign` FOREIGN KEY (`loai_promotion`) REFERENCES `loai_promotion` (`ma_loai_promotion`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  ADD CONSTRAINT `san_pham_loai_san_pham_foreign` FOREIGN KEY (`loai_san_pham`) REFERENCES `loai_san_pham` (`ma_loai_san_pham`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `topping`
--
ALTER TABLE `topping`
  ADD CONSTRAINT `topping_loai_topping_foreign` FOREIGN KEY (`loai_topping`) REFERENCES `loai_topping` (`ma_loai_topping`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `tuy_chon`
--
ALTER TABLE `tuy_chon`
  ADD CONSTRAINT `tuy_chon_id_chi_tiet_hoa_don_foreign` FOREIGN KEY (`id_chi_tiet_hoa_don`) REFERENCES `chi_tiet_hoa_don` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tuy_chon_id_topping_foreign` FOREIGN KEY (`id_topping`) REFERENCES `topping` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ma_phan_quyen_foreign` FOREIGN KEY (`ma_phan_quyen`) REFERENCES `phanquyen` (`ma_phan_quyen`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
